<?php
$request_uri = $_SERVER["REQUEST_URI"];
$uri = strrpos($request_uri, "/");
$dir = $_SERVER["SERVER_NAME"] . substr($request_uri, 0, $uri + 1);
if (!isset($_SERVER["HTTPS"]) || strtolower($_SERVER["HTTPS"]) != "on") {
    $adresa = 'https://' . $dir . 'rezervacija.php';
    header("Location: $adresa");
    exit();
}

include ("sesija.class.php");
include ("baza.class.php");
require 'vanjske_biblioteke/Smarty/libs/Smarty.class.php';

require_once('recaptchalib.php');
$privatekey = "6Lcdsh8UAAAAAE3XfhSfQ4hXE6NMS1Sec2oqi7Jm";
$publickey = "6Lcdsh8UAAAAAOi6USw-bUQkleOaY6LkY81T-PV6";
$greske = "";
$nedopusteniZnak = '(){}\'!#“\\/';
$nedopusteno = false;
$popunjenaPolja = false;
$krivalozinka = false;
$krivimail = false;
$praznoPolje = "";
$imeGreska = false;
$prezimeGreska = false;
$uspjesno = false;

$greska = "";
$ispravno = true;

if (isset($_POST["submit"])) {
    $greska = "";


    $ispravno = true;
    $nedopusteno = false;
    $nijePopunjeno = false;
    foreach ($_POST as $key => $value) {
        if (empty($_POST[$key])) {
            $greska .= "Nisu popunjena sva polja! <br>";
            $popunjenaPolja = true;
            $ispravno = false;
            $nijePopunjeno = true;
        }
    }

    $ime = $_POST["ime"];




    if (strlen($ime) < 5) {
        $greska .= "Ime mora sadrzavati minimalno 5 znakova.<br>";
        $imeGreska = true;
        $ispravno = false;
    }
    if ($ime != ucfirst($ime)) {
        $greska .= "Ime mora zapoceti velikim slovom.<br>";
        $imeGreska = true;
        $ispravno = false;
    }

    $prezime = $_POST["prezime"];

    if ($prezime != ucfirst($prezime)) {
        $greska .= "Prezime mora zapoceti velikim slovom.<br>";
        $prezimeGreska = true;
        $ispravno = false;
    }



    foreach ($_POST as $key => $value) {
        //echo $value."<br>";

        for ($index = 0; $index < strlen($value); $index++) {
            for ($index1 = 0; $index1 < strlen($nedopusteniZnak); $index1++) {
                if ($value[$index] == $nedopusteniZnak[$index1])
                    $nedopusteno = true;
                if ($nedopusteno == true)
                    break;
            }
            if ($nedopusteno == true)
                break;
        }
        if ($nedopusteno == true)
            break;
    }

    if ($nedopusteno == true) {
        $greska .= "Ne smijete koristiti ove znakove! <br>";
        $ispravno = false;
    }



    $format = $_POST["lozinka"];
    $pattern = '/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{5,15}$/';
    if (!preg_match($pattern, $format, $matches, PREG_OFFSET_CAPTURE)) {
        $greska .= "Lozinka nije u ispravnom formatu! <br> Sadrži od 5-15 znakova, dva velika slova, dva mala i barem jedan broj. <br>";
        $krivalozinka = true;
        $ispravno = false;
    }

    $format = $_POST["email"];
    $pattern = '/^\w{2,}@(\w{2,}\.){1,2}\w{2,}$/';
    if (!preg_match($pattern, $format, $matches, PREG_OFFSET_CAPTURE)) {
        $greska .= "Email nije u ispravnom formatu!<br>";
        $krivimail = true;
        $ispravno = false;
    }


    $baza = new Baza();
    $baza->spojiDB();

    if ($_POST["lozinka"] != $_POST["lozinka2"]) {
        $greska .= "Lozinke se ne poklapaju!<br>";
        $ispravno = false;
    }

    $ime = mysqli_real_escape_string($baza->spojiDB(), $_POST["ime"]);
    $prezime = mysqli_real_escape_string($baza->spojiDB(), $_POST["prezime"]);
    $korime = mysqli_real_escape_string($baza->spojiDB(), $_POST["korime"]);
    $email = mysqli_real_escape_string($baza->spojiDB(), $_POST["email"]);
    $lozinka = mysqli_real_escape_string($baza->spojiDB(), $_POST["lozinka"]);
    $salt = 'jkwgvurfzt47rz3648r437trf3478tr6f34 tf734trf3496';
    $kriptlozinka = sha1($lozinka) . $salt;



    $upit = "SELECT korisnickoIme FROM korisnik WHERE korisnickoIme = '$korime'";
    $res = $baza->selectDB($upit);
    if ($baza->pogreskaDB()) {
        $greska .= "Problem kod upita na bazu podataka!<br>";
        exit;
    }
    $polje_kor_ime = mysqli_fetch_array($res);
    if ($korime == $polje_kor_ime["korisnickoIme"]) {
        $greska .= "Postoji korisnik s unesenim korisnickim imenom!<br>";
        $ispravno = false;
    }


    $upit = "SELECT email FROM korisnik WHERE email = '$email'";
    $res = $baza->selectDB($upit);
    if ($baza->pogreskaDB()) {
        $greska .= "Problem kod upita na bazu podataka!<br>";
        exit;
    }
    $polje_email = mysqli_fetch_array($res);
    if ($email == $polje_email["email"]) {
        $greska .= "Postoji korisnik s unesenom email adresom!<br>";
        $ispravno = false;
    }

    if ($_POST['zelimUDvaKoraka'] == "da") {
        $prijava_u_dva_koraka = 1;
    }
    if ($_POST['zelimUDvaKoraka'] == "ne") {
        $prijava_u_dva_koraka = 0;
    }

    $vrijeme = time();
    $salt = 'WebDiPjezakon';
    $aktivacijskiKod = sha1($vrijeme . $salt);

    $mail_to = $polje_email['email'];
    $mail_subject = "Kod za aktivaciju računa";
    $mail_body = "Kod za aktivaciju računa je: http://barka.foi.hr/WebDiP/2016_projekti/WebDiP2016x004/aktivacija.php?kod=" . $aktivacijskiKod;
    mail($mail_to, $mail_subject, $mail_body);
    setcookie('aktivacijskiKod', $aktivacijskiKod, time() + (60 * 60 * 24));

    $baza->zatvoriDB();
} else {
    $ispravno = false;
}


/* if (isset($_SESSION["tip"])) {
  $tipP = $_SESSION["tip"];
  } else {
  $tipP = '4';
  } */
$smarty = new Smarty;
$smarty->assign("naslov", "Registracija");
//$smarty->assign("tip", $tipP);
$smarty->display('predlosci/_header.tpl');

include 'navigacija.php';
?>


<span id="status"></span>

<form id="registracija"  name="registracija" method="POST" action="registracija.php">

    <div id="gornji_dio">
        <label for="ime">Ime: <?php
if ($imeGreska) {
    echo "<span class='greska' > ! </span>";
}
?> </label> 
        <input id="ime" type="text" name="ime" ><br>


        <label for="prezime">Prezime:<?php
            if ($prezimeGreska) {
                echo "<span class='greska' > ! </span>";
            }
?> </label>
        <input id="prezime" type="text" name="prezime" ><br>

        <label for="korime">Korisničko ime:<?php
            if ($praznoPolje == 'korime') {
                echo "<span class='greska' > ! </span>";
            }
?> </label>
        <input id="korime" type="text" name="korime" onkeyup="dajPodatke();"><br>

        <label for="email">E-mail: <?php
            if ($krivimail || ($praznoPolje == 'email')) {
                echo "<span class='greska' > ! </span>";
            }
?> </label>
        <input id="email" type="email" name="email"  ><br>

        <label for="lozinka">Lozinka: <?php
            if ($krivalozinka || ($praznoPolje == 'lozinka')) {
                echo "<span class='greska' > ! </span>";
            }
?></label>
        <input id="lozinka" type="password" name="lozinka" ><br>

        <label for="lozinka2">Ponovi lozinku: <?php
            if ($praznoPolje == 'lozinka2') {
                echo "<span class='greska' > ! </span>";
            }
?></label> 
        <input id="lozinka2" type="password" name="lozinka2" ><br>
    </div>

    <div id="gumbi" >
        <label id="dva_koraka" >Želiš li prijavu u dva koraka? </label>

        <input id="zelimUDvaKoraka" type="radio" name="zelimUDvaKoraka" value="da" >Da <br>

        <input id="neZelimUDvaKoraka" type="radio" name="zelimUDvaKoraka" value="ne">Ne<br>
    </div>

    <div class="g-recaptcha" data-sitekey="6Lcdsh8UAAAAAOi6USw-bUQkleOaY6LkY81T-PV6"></div>

    <input type="submit" name="submit" value="Registriraj se" style="margin: 10px;">

</form>

<br><?php echo "<span class='greska' >" . $greska . "</span>" ?>
<div id='greska'></div>
<br><?php
            if ($uspjesno) {
                echo "<span  >Uspjesno ste se registrirali!<br>Aktivacijski kod će vam doći na Vaš email.</span>";
            }
?>

<?php
$smarty2 = new Smarty;
$smarty2->display('predlosci/_footer.tpl');

$baza = new Baza();
$baza->spojiDB();

if ($ispravno == true) {
    $sql = "INSERT INTO korisnik VALUES(DEFAULT, '$ime', '$prezime', '$korime', '$email', '$lozinka', '$kriptlozinka', '1', 3, '41257/14-R', '$prijava_u_dva_koraka', 0, '$aktivacijskiKod' )";


    $uspjesno = $baza->selectDB($sql);

    if ($baza->pogreskaDB()) {
        echo "Problem kod upita na bazu podataka!";
        exit;
    }
    $baza->zatvoriDB();

    $korisnik = $_SESSION["id_korisnika"];
    $baza = new Baza();
    $baza->spojiDB();

    $datum = date("Y-m-d H:i:s");

    $upit = "INSERT INTO dnevnik_rada VALUES(DEFAULT, 0, '$datum', 'Registracija korisnika.' )";
    $baza->selectDB($upit);

    $baza->zatvoriDB();
}
?>