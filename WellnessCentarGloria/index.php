
<?php
include ("sesija.class.php");
include("baza.class.php");
require 'vanjske_biblioteke/Smarty/libs/Smarty.class.php';
Sesija::kreirajSesiju();

$smarty = new Smarty;
$smarty->assign("naslov", "Početna stranica");
$smarty->display('predlosci/_header.tpl');

if (isset($_SESSION["id_korisnika"])) {
    $korisnik = $_SESSION["id_korisnika"];
} else {
    $korisnik = 0;
}
$baza = new Baza();
$baza->spojiDB();

$datum = date("Y-m-d H:i:s");

$upit = "INSERT INTO dnevnik_rada VALUES(DEFAULT, '$korisnik', '$datum', 'Ulazak u index.php' )";
$baza->selectDB($upit);

$baza->zatvoriDB();



include 'navigacija.php';
if (!isset($_SESSION["korisnik"])) {
    include 'neregistrirani.php';
}


if (isset($_SESSION["korisnik"])) {
    if ($_SESSION["tip"] == 3 || $_SESSION["tip"] == 1 || $_SESSION["tip"] == 2) {
        include 'registrirani.php';
    }
    if ($_SESSION["tip"] == 1) {
        $baza = new Baza();
        $baza->spojiDB();
        $sql = "SELECT idkategorija FROM kategorija_usluga";
        $rezultat = $baza->selectDB($sql);

        if ($baza->pogreskaDB()) {
            echo "Problem kod upita na bazu podataka!";
            exit;
        }
        $kategorije = array();
        while ($polje = mysqli_fetch_array($rezultat)) {
            array_push($kategorije, $polje["0"]);
        }

        Sesija::kreirajModeratora($kategorije);
    }
    if ($_SESSION["tip"] == 2) {
        $baza = new Baza();
        $baza->spojiDB();
        $korisnik = $_SESSION["id_korisnika"];
        $sql = "SELECT * FROM moderatori WHERE korisnik='$korisnik'";
        $rezultat = $baza->selectDB($sql);

        if ($baza->pogreskaDB()) {
            echo "Problem kod upita na bazu podataka!";
            exit;
        }
        $kategorije = array();
        while ($polje = mysqli_fetch_array($rezultat)) {
            array_push($kategorije, $polje["kategorija"]);
        }

        Sesija::kreirajModeratora($kategorije);
    }
}
?>

</body>
</html>

<?php
$smarty2 = new Smarty;
$smarty2->display('predlosci/_footer.tpl');
?>
