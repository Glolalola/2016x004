<?php
include ("sesija.class.php");
include ("baza.class.php");
require 'vanjske_biblioteke/Smarty/libs/Smarty.class.php';
$obavijest = "";

Sesija::kreirajSesiju();
if ($_SESSION["tip"] != 1 && $_SESSION["tip"] != 2) {
    echo "Preusmjeravanje...";
    header("Location: index.php");
}

$korisnik = $_SESSION["id_korisnika"];
$baza = new Baza();
$baza->spojiDB();

$datum = date("Y-m-d H:i:s");

$upit = "INSERT INTO dnevnik_rada VALUES(DEFAULT, '$korisnik', '$datum', 'Ulazak u provjeriKupon.php.' )";
$baza->selectDB($upit);

$baza->zatvoriDB();


$smarty = new Smarty;
$smarty->assign("naslov", "Definiraj uslugu");
$smarty->display('predlosci/_header.tpl');

include 'navigacija.php';
?>
<form id="imaLiKoda" name="imaLiKoda"  method="post" class="def">

    <div style="display: inline-block">
        <label for="kod"  id="kodLabel" >Unesi kod: </label>
        <input id="kod" type="text" name="kod" ><br>

    </div>
    <div style="display: block">
        <button style="float: right; margin: 5px; display: block;"  type="submit" name="imaLiKoda" value="imaLiKoda">Provjeri</button>
    </div>
</form>

<?php
if (!empty($_POST["imaLiKoda"])) {
    $baza = new Baza;
    $baza->spojiDB();
    $nijePopunjeno = false;

    if (empty($_POST["kod"])) {
        $obavijest .= "Niste unijeli kod!";
        $nijePopunjeno = true;
    }
    $kod = $_POST["kod"];
    if (!$nijePopunjeno) {
        $sql = "SELECT kod_kupona FROM košarica WHERE kod_kupona= '$kod'";
        $uspjesno = $baza->selectDB($sql);

        if ($baza->pogreskaDB()) {
            echo "Problem kod upita na bazu podataka!";
            exit;
        }

        $kodIzBaze = mysqli_fetch_array($uspjesno);
        if ($kod == $kodIzBaze["kod_kupona"]) {
            $obavijest .= "Kod postoji.";
        } else {
            $obavijest .= "Kod ne postoji.";
        }
    }
}
echo'<br><span>' . $obavijest . '</span>" ';

$smarty2 = new Smarty;
$smarty2->display('predlosci/_footer.tpl');
?>



