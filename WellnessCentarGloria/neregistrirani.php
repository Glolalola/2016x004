<?php if (!isset($_COOKIE['uvjeti_koristenjaN'])) {
    
    echo '<div style="color:white; background-color:#003399; display:block;" >
        <p>Ova stranica koristi kolačiće. Prihvaćate li uvjete korištenja?
        <a style="color: white;" href="http://ec.europa.eu/ipg/basics/legal/cookies/index_en.htm">Saznaj više</a>
        </p>
        <a style="color: white;" href="index.php?prihvacanje=da">Prihvaćam</a></div>';
    if (isset($_GET['prihvacanje'])) {
        setcookie('uvjeti_koristenjaN', '$vrijednost', time() + (60 * 60 * 3 * 24));
        echo "<meta http-equiv=\"refresh\" content=\"0;URL=index.php\">";
    }   
     
}?>
<style>
<?php include 'css/globabic.css'; ?>
</style>


<div id="kategorije" class="kategorije">

    <h1>Kategorije</h1>

    <?php
    $baza = new Baza();
    $baza->spojiDB();

    $sql = "SELECT * FROM kategorija_usluga";
    $rezultat = $baza->selectDB($sql);

    if ($baza->pogreskaDB()) {
        echo "Problem kod upita na bazu podataka!";
        exit;
    }
    
    while ($polje = mysqli_fetch_array($rezultat)) {

        echo '<ul>
        <li>
           <a href="index.php?kategorija=' . $polje[0] . '">' . $polje[1] . '</a>  
        </li>
    </ul>';
    }

    if (isset($_GET['kategorija'])) {
        $kategorija = $_GET['kategorija'];


        $sql = "SELECT * FROM usluga WHERE kategorija_usluga_idkategorija='{$kategorija}' ORDER BY broj_rezervacija DESC LIMIT 3";
        $rezultat = $baza->selectDB($sql);

        if ($baza->pogreskaDB()) {
            echo "Problem kod upita na bazu podataka!";
            exit;
        }

        echo '<h2 class="usluge"> Tri najčešće rezervirane usluge</h2>';
        
        $baza2 = new Baza();
        $baza2->spojiDB();
        $sql2 = "SELECT boja, font, pozadina FROM kategorija_usluga WHERE idkategorija='{$kategorija}'";
        $rezultat2 = $baza2->selectDB($sql2);
        $polje2 = mysqli_fetch_assoc($rezultat2);

        if ($baza2->pogreskaDB()) {
            echo "Problem kod upita na bazu podataka!";
            exit;
        }

        echo '<div class=' . $polje2['boja'] . ' > <div class=' . $polje2['font'] . ' ><div id"uslugeKategorije">';


        while ($polje = mysqli_fetch_array($rezultat)) {
            echo "<ul><li style='display: block;
                text-transform: uppercase;
                text-decoration: none;
                float:left;
                border-width: 1px;
                padding: 5px 10px;'><p  class=" . $polje2['pozadina'] . " >" . $polje["naziv"] . "</p></li></ul>";
        }
         echo '</div></div></div>';
    }

    $baza->zatvoriDB();
    ?>

</div>
