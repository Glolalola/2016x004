<?php
include ("sesija.class.php");
include ("baza.class.php");
require 'vanjske_biblioteke/Smarty/libs/Smarty.class.php';
$kat = array();
$greska = "";

Sesija::kreirajSesiju();
if ($_SESSION["tip"] != 1) {
    echo "Preusmjeravanje...";
    header("Location: index.php");
}
$korisnik = $_SESSION["id_korisnika"];
$baza = new Baza();
$baza->spojiDB();

$datum = date("Y-m-d H:i:s");

$upit = "INSERT INTO dnevnik_rada VALUES(DEFAULT, '$korisnik', '$datum', 'Ulazak u kreiranjeKupona.php' )";
$baza->selectDB($upit);

$baza->zatvoriDB();

$smarty = new Smarty;
$smarty->assign("naslov", "Kreiraj kupon");
$smarty->display('predlosci/_header.tpl');

include 'navigacija.php';

$slike = array();
$video = array();
$pdf = array();

function popisSlika() {
    $baza = new Baza();
    $baza->spojiDB();

    $sql = "SELECT * FROM slika";
    $rezultat = $baza->selectDB($sql);

    if ($baza->pogreskaDB()) {
        echo "Problem kod upita na bazu podataka!";
        exit;
    }
    $idSlike = array();
    $nazivSlike = array();
    while ($polje = mysqli_fetch_array($rezultat)) {
        echo "<option>" . $polje["naziv"] . "</option>";
        array_push($idSlike, $polje[0]);
        array_push($nazivSlike, $polje["naziv"]);
    }
    global $slike;
    $slike = array_combine($nazivSlike, $idSlike);

    $baza->zatvoriDB();
}

function popisVidea() {
    $baza = new Baza();
    $baza->spojiDB();

    $sql = "SELECT * FROM video";
    $rezultat = $baza->selectDB($sql);

    if ($baza->pogreskaDB()) {
        echo "Problem kod upita na bazu podataka!";
        exit;
    }
    $idVideo = array();
    $nazivVideo = array();
    while ($polje = mysqli_fetch_array($rezultat)) {
        echo "<option>" . $polje["naziv"] . "</option>";
        array_push($idVideo, $polje[0]);
        array_push($nazivVideo, $polje["naziv"]);
    }
    echo "<option></option>";
    global $video;
    $video = array_combine($nazivVideo, $idVideo);

    $baza->zatvoriDB();
}

function popisPDF() {
    $baza = new Baza();
    $baza->spojiDB();

    $sql = "SELECT * FROM pdf";
    $rezultat = $baza->selectDB($sql);

    if ($baza->pogreskaDB()) {
        echo "Problem kod upita na bazu podataka!";
        exit;
    }
    $idPDF = array();
    $nazivPDF = array();
    while ($polje = mysqli_fetch_array($rezultat)) {
        echo "<option>" . $polje["naziv"] . "</option>";
        array_push($idPDF, $polje[0]);
        array_push($nazivPDF, $polje["naziv"]);
    }
    global $pdf;
    $pdf = array_combine($nazivPDF, $idPDF);

    $baza->zatvoriDB();
}
?>
<form id="kreiranjeKupona" name="kreiranjeKupona"  method="post" class="def">  

    <div style="display: inline-block">
        <div style="display: inline-block">

            <label style="float: left;" for="nazivKupona"  id="nazivKuponaLabel" >Naziv kupona: </label>
            <input style="float: right; margin: 7px;" id="nazivKupona" type="text" name="nazivKupona" ><br>

        </div>
        <div style="display: inline-block">

            <label style="float: left;" for="slika"  id="slikaLabel" >Slika: </label>
            <select style="float: right; margin: 7px;" id="slika" name="slika"><?php popisSlika() ?></select><br>

        </div>
        <div style="display: inline-block">

            <label  style="float: left; "for="video"  id="videoLabel" >Video: </label>
            <select style="float: right; margin: 7px;" id="video" name="video"><?php popisVidea() ?></select><br>

        </div>
        <div style="display: inline-block">

            <label  style="float: left; "for="pdf"  id="pdfLabel" >Pdf: </label>
            <select style="float: right; margin: 7px;" id="pdf" name="pdf"><?php popisPDF() ?></select><br>

        </div>


    </div>
    <div style="display: block">
        <button style="float: right; margin: 5px; display: block;"  type="submit" name="kreiranjeKupona" value="kreiranjeKupona">Dodaj kupon</button>
    </div>
</form>

<?php
if (!empty($_POST["kreiranjeKupona"])) {
    $baza = new Baza;
    $baza->spojiDB();
    $ispravno = true;
    
    foreach ($_POST as $key => $value) {
        if (empty($_POST[$key])) {
            global $greska;
            $greska .= "Nisu popunjena sva polja! <br>";
            
            $ispravno = false;
            
        }
    }
    if(empty($_POST["video"])){$ispravno = true;}
    $uneseniNaziv = $_POST["nazivKupona"];
    $unesenaSlika = $slike[$_POST["slika"]];
    $uneseniVideo = $video[$_POST["video"]];
    $uneseniPDF = $pdf[$_POST["pdf"]];

    if ($ispravno) {
        $sql = "INSERT INTO akcija_kupona VALUES(DEFAULT, '$uneseniNaziv', '$unesenaSlika', '$uneseniVideo', '$uneseniPDF')";
        //echo "upit za rezervaciju<br>".$sql;
        $uspjesno = $baza->selectDB($sql);
        if ($baza->pogreskaDB()) {
            echo "Problem kod upita na bazu podataka!";
            exit;
        }

        $baza->zatvoriDB();
}}

   /* $sql = "INSERT INTO kategorija_usluga VALUES(DEFAULT, '$uneseniNaziv', '$unesenaBoja', '$uneseniFont', '$unesenaPozadina')";
    //echo "upit za rezervaciju<br>".$sql;
    $uspjesno = $baza->selectDB($sql);
    if ($baza->pogreskaDB()) {
        echo "Problem kod upita na bazu podataka!";
        exit;
    }

    $baza->zatvoriDB();
    $baza2 = new Baza;
    $baza2->spojiDB();


    $sql = "SELECT idkategorija , naziv FROM kategorija_usluga WHERE naziv='$uneseniNaziv'";
    //echo 'SQL= '.$sql;

    $rezultat = $baza2->selectDB($sql);

    if ($baza2->pogreskaDB()) {
        echo "Problem kod upita na bazu podataka!";
        exit;
    }
    $idBaza = mysqli_fetch_assoc($rezultat);
    $id = $idBaza["idkategorija"];

    $baza2->zatvoriDB();
    $baza3 = new Baza;
    $baza3->spojiDB();
    $sql = "INSERT INTO moderatori VALUES('$uneseniModerator','$id')";
    //echo "upit za moderatore<br>".$sql;
    $uspjesno = $baza3->selectDB($sql);
    if ($baza3->pogreskaDB()) {
        echo "Problem kod upita na bazu podataka!";
        exit;
    }
}*/
echo'<br><span class="greska" >' . $greska . '</span>" ';

$smarty2 = new Smarty;
$smarty2->display('predlosci/_footer.tpl');
?>



