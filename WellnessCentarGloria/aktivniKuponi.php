<?php
include("baza.class.php");
include("sesija.class.php");

Sesija::kreirajSesiju();
if ($_SESSION["tip"] != 1 && $_SESSION["tip"] != 2 && $_SESSION["tip"] != 3) {
    echo "Preusmjeravanje...";
    header("Location: index.php");
}
$korisnik = $_SESSION["id_korisnika"];
$baza = new Baza();
$baza->spojiDB();

$datum = date("Y-m-d H:i:s");

$upit = "INSERT INTO dnevnik_rada VALUES(DEFAULT, '$korisnik', '$datum', 'Ulazak u aktivniKuponi.php' )";
$baza->selectDB($upit);

$baza->zatvoriDB();

require 'vanjske_biblioteke/Smarty/libs/Smarty.class.php';
$smarty = new Smarty;
$smarty->assign("naslov", "Moji bodovi");
$smarty->display('predlosci/_header.tpl');

include 'navigacija.php';

$baza = new Baza();
$baza->spojiDB();

$suma = 0;
$korisnik = $_SESSION["id_korisnika"];

$sql = "SELECT naziv, datum, korisnik, idakcije_korisnika, ostvareni_bodovi, akcija_korisnika FROM bodovi, akcije_korisnika WHERE korisnik='$korisnik' AND idakcije_korisnika=akcija_korisnika ";
$rezultat = $baza->selectDB($sql);

if ($baza->pogreskaDB()) {
    echo "Problem kod upita na bazu podataka!";
    exit;
}
while ($polje = mysqli_fetch_array($rezultat)) {

    $suma += $polje["ostvareni_bodovi"];
}
$datum = date("Y-m-d");
$sql = "SELECT s.path, s.idslika, ak.slika, ak.idakcija_kupona, ak.naziv, k.bodovi, k.akcija_kupona_idakcija_kupona, k.kategorija_usluga_idkategorija, k.idkupon, k.aktivan, ku.idkategorija, ku.naziv
FROM slika s, akcija_kupona ak, kupon k, kategorija_usluga ku
WHERE s.idslika = ak.slika
AND k.akcija_kupona_idakcija_kupona = ak.idakcija_kupona
AND ku.idkategorija = k.kategorija_usluga_idkategorija
AND k.kraj > '$datum'";
$rezultat2 = $baza->selectDB($sql);


if ($baza->pogreskaDB()) {
    echo "Problem kod upita na bazu podataka!";
    exit;
}

$id = array();
$kategorija = array();


while ($polje = mysqli_fetch_array($rezultat2)) {
    if ($suma >= $polje[5]) {
        if (!in_array($polje[7], $id)) {
            array_push($id, $polje[7]);
            $kategorija[] = array("id" => $polje[7], "ime" => $polje[11]);
        }
    }
}
/* echo 'id = <br>';
  print_r($id);
  echo 'kategorija = <br>';
  print_r($kategorija); */

for ($index = 0; $index < count($id); $index++) {
    $baza2 = new Baza();
    $baza2->spojiDB();
    $kat = $kategorija[$index]['id'];
    $sql2 = "SELECT boja, font, pozadina FROM kategorija_usluga WHERE idkategorija='$kat'";
    $rezultat3 = $baza2->selectDB($sql2);
    $polje2 = mysqli_fetch_assoc($rezultat3);

    if ($baza2->pogreskaDB()) {
        echo "Problem kod upita na bazu podataka!";
        exit;
    }

    echo "<div style='display:inline-block;' >
          <div class=" . $polje2['boja'] . " >
          <div class=" . $polje2['font'] . " >
          <div class=" . $polje2['pozadina'] . " >
          <h2>" . $kategorija[$index]['ime'] . "</h2>";
    kuponi($kategorija[$index]['id']);
}

function kuponi($kat) {
    $baza = new Baza();
    $baza->spojiDB();
    $datum = date("Y-m-d");

    $sql = "SELECT s.naziv, s.idslika, ak.slika, ak.idakcija_kupona, ak.naziv, k.bodovi, k.akcija_kupona_idakcija_kupona, k.kategorija_usluga_idkategorija, k.idkupon, k.aktivan, ku.idkategorija, ku.naziv
FROM slika s, akcija_kupona ak, kupon k, kategorija_usluga ku
WHERE s.idslika = ak.slika
AND k.akcija_kupona_idakcija_kupona = ak.idakcija_kupona
AND ku.idkategorija = k.kategorija_usluga_idkategorija
AND k.kraj > '$datum'";
    $rezultat2 = $baza->selectDB($sql);


    if ($baza->pogreskaDB()) {
        echo "Problem kod upita na bazu podataka!";
        exit;
    }

    //echo 'pokrenuta sam' . $kat;
    while ($polje = mysqli_fetch_array($rezultat2)) {
        //echo 'Printam polje: <br>';
        //print_r($polje);
        //echo 'Printam sliku: <br>'. $polje[0];

        if ($polje[7] == $kat) {

            /* echo '<figure>
              <img id="kupon" src="slike/'.$polje[0].'.jpg" width="100px" height="66px" alt="'.$polje[4].'" >

              </figure>'; */

            echo '<div class="responzivno">
  <div class="galerija">
    <a href="kupon.php?id=' . $polje[8] . '">
      <img src="slike/' . $polje[0] . '.jpg" alt="' . $polje[4] . '" width="300" height="200">
    </a>
    <div class="desc">' . $polje[4] . '</div>
  </div>
</div>
';
        }
    }
    echo '<div class="clearfix"></div></div></div>

<div style="padding:6px;">
</div></div>';
}

$baza->zatvoriDB();
?>

</div>

<?php
$smarty2 = new Smarty;
$smarty2->display('predlosci/_footer.tpl');
?>


