<form id="novi_proizvod" name="novi_proizvod" novalidate method="post">

                <label for="naziv" id="nazivLabel" >Naziv proizvoda: </label>
                <input id="naziv" value='{$naziv}' type="text" name="naziv"  ><br>

                <label for="opis" id="opisLabel"  >Opis proizvoda: </label>
                <textarea id="opis" name="opis" value='{$opis}' rows="50" cols="100"  style="margin: 10px; max-width: 60%;  " placeholder="Ovdje unesite opis proizvoda"></textarea><br>

                <label for="datumProizvodnje"  id="datumProizvodnjeLabel" >Datum proizvodnje </label>
                <input id="datumProizvodnje" value='{$datumProizvodnje}' type="text"  name="datumProizvodnje" ><br>

                <label for="vrijemeProizvodnje" id="vrijemeProizvodnjeLabel" >Vrijeme proizvodnje </label>
                <input id="vrijemeProizvodnje" value='{$vrijemeProizvodnje}' type="time"  name="vrijemeProizvodnje" ><br>

                <label for="kolicinaProizvoda" id="kolicinaProizvodaLabel" >Količina proizvoda </label>
                <input id="kolicinaProizvoda" value='{$kolicinaProizvoda}' type="number"  name="kolicinaProizvoda" min="1" ><br>

                <label for="tezinaProizvoda"  id="tezinaProizvodaLabel"  >Težina proizvoda </label>
                <input id="tezinaProizvoda" value='{$tezinaProizvoda}' type="range"  name="tezinaProizvoda" min="0" max="100"><br>


                <label id="kategorijaLabel"  > Kategorija </label><br>

                <label for="kategorija1"> Elegantno </label><br>
                <input id="kategorija1" checked="{$elBoolean}" type="checkbox" ><br>

                <label for="kategorija2"> Plaža </label><br>
                <input id="kategorija2" checked="{$plBoolean}" type="checkbox" ><br>

                <label for="kategorija3"> Svaki dan </label><br>
                <input id="kategorija3" checked="{$sdBoolean}" type="checkbox" >  <br>

                <label style="margin: 30px;">  </label>


                <input id="posalji" type="submit" style="margin: 10px;"  name="posalji" value="Pošalji"><br>
                <input id="reset" type="reset" style="margin: 10px;" value=" Inicijaliziraj ">



            </form>
