<?php
if ($_SESSION["tip"] != 1 && $_SESSION["tip"] != 2 && $_SESSION["tip"] != 3) {
    echo "Preusmjeravanje...";
    header("Location: index.php");
}
if (!isset($_COOKIE['uvjeti_koristenjaR'])) {
    
    echo '<div style="color:white; background-color:#003399; display:block;" >
        <p>Ova stranica koristi kolačiće. Prihvaćate li uvjete korištenja?
        <a style="color: white;" href="http://ec.europa.eu/ipg/basics/legal/cookies/index_en.htm">Saznaj više</a>
        </p>
        <a style="color: white;" href="index.php?prihvacanje=da">Prihvaćam</a></div>';
    if (isset($_GET['prihvacanje'])) {
        setcookie('uvjeti_koristenjaR', '$vrijednost', time() + (60 * 60 * 30 * 24));
        echo "<meta http-equiv=\"refresh\" content=\"0;URL=index.php\">";
    }   
     
}
?>
<style>
<?php include 'css/globabic.css'; ?>
</style>

<div id="kategorije" class="kategorije">
    <a class="tumblr-share-button" data-color="black" data-notes="none" href="https://embed.tumblr.com/share"></a>
    <script>
        !function (d, s, id) {
            var js, ajs = d.getElementsByTagName(s)[0];
            if (!d.getElementById(id))
            {
                js = d.createElement(s);
                js.id = id;
                js.src = "https://assets.tumblr.com/share-button.js";
                ajs.parentNode.insertBefore(js, ajs);

            }
        }
        (document, "script", "tumblr-js");

    </script>


    <h1>Kategorije</h1>

    <?php
    $baza = new Baza();
    $baza->spojiDB();

    $sql = "SELECT * FROM kategorija_usluga";
    $rezultat = $baza->selectDB($sql);

    if ($baza->pogreskaDB()) {
        echo "Problem kod upita na bazu podataka!";
        exit;
    }
    while ($polje = mysqli_fetch_array($rezultat)) {

        echo '<ul>
        <li>
           <a href="index.php?kategorija=' . $polje[0] . '">' . $polje[1] . '</a>  
        </li>
    </ul>';
    }


    if (isset($_GET['kategorija'])) {
        $kategorija = $_GET['kategorija'];


        $sql = "SELECT * FROM usluga WHERE kategorija_usluga_idkategorija='{$kategorija}' ORDER BY broj_rezervacija DESC";
        $rezultat = $baza->selectDB($sql);

        if ($baza->pogreskaDB()) {
            echo "Problem kod upita na bazu podataka!";
            exit;
        }
        $baza2 = new Baza();
        $baza2->spojiDB();
        $sql2 = "SELECT boja, font, pozadina FROM kategorija_usluga WHERE idkategorija='{$kategorija}'";
        $rezultat2 = $baza2->selectDB($sql2);
        $polje2 = mysqli_fetch_assoc($rezultat2);

        if ($baza2->pogreskaDB()) {
            echo "Problem kod upita na bazu podataka!";
            exit;
        }

        echo '<div class=' . $polje2['boja'] . ' > <div class=' . $polje2['font'] . ' ><h2 class="usluge">Usluge</h2><div id"uslugeKategorije">';

        while ($polje = mysqli_fetch_array($rezultat)) {
            echo '<ul style="list-style: none;
                            float:left;
                            margin: 20px;
                            display: inline-block;">
        <li>
           <a class=' . $polje2['pozadina'] . ' style="display: block;
                text-transform: uppercase;
                text-decoration: none;
                float:left;
                border-width: 1px;
                padding: 5px 10px;" 
                href="rezervacija.php?usluga=' . $polje[0] . '">' . $polje[1] . '</a>  
        </li>
    </ul>';
        }
        echo '</div></div></div>';
    }

    $baza->zatvoriDB();
    ?>

</div>
