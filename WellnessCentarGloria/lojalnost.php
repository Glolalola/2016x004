
<?php
include("baza.class.php");
include("sesija.class.php");

Sesija::kreirajSesiju();
if ($_SESSION["tip"] != 1) {
    echo "Preusmjeravanje...";
    header("Location: index.php");
}

$korisnik = $_SESSION["id_korisnika"];
$baza = new Baza();
$baza->spojiDB();

$datum = date("Y-m-d H:i:s");

$upit = "INSERT INTO dnevnik_rada VALUES(DEFAULT, '$korisnik', '$datum', 'Ulazak u lojalnost.php' )";
$baza->selectDB($upit);

$baza->zatvoriDB();

$korisnik = $_SESSION["id_korisnika"];
$baza = new Baza();
$baza->spojiDB();

$datum = date("Y-m-d H:i:s");

$upit = "INSERT INTO dnevnik_rada VALUES(DEFAULT, '$korisnik', '$datum', 'Ulazak u blokiranje_korisnika.php' )";
$baza->selectDB($upit);

$baza->zatvoriDB();

function blokiraj_korisnike_lista() {
    $baza = new Baza();
    $baza->spojiDB();

    $sql = "SELECT korisnickoIme, email FROM korisnik WHERE(broj_pokusaja < 3) AND (blokiran = 0 )";
    $rezultat = $baza->selectDB($sql);

    if ($baza->pogreskaDB()) {
        echo "Problem kod upita na bazu podataka!";
        exit;
    }

    while ($polje = mysqli_fetch_array($rezultat)) {
        echo "<tr><td>" . $polje["korisnickoIme"] . "</td><td>" . $polje["email"] . "</td></tr>";
    }

    $baza->zatvoriDB();
}

function selekcija_korisnika() {
    $baza = new Baza();
    $baza->spojiDB();

    $sql = "SELECT korisnickoIme FROM korisnik WHERE(broj_pokusaja <= 3) AND (blokiran = 0 )";
    $rezultat = $baza->selectDB($sql);

    if ($baza->pogreskaDB()) {
        echo "Problem kod upita na bazu podataka!";
        exit;
    }

    while ($polje = mysqli_fetch_array($rezultat)) {
        echo "<option>" . $polje["korisnickoIme"] . "</option>";
    }

    $baza->zatvoriDB();
}

if (isset($_POST["blokiraj"])) {
    $baza = new Baza();
    $baza->spojiDB();

    $korime = mysqli_real_escape_string($baza->spojiDB(), $_POST["korisnik"]);

    $sql = "UPDATE korisnik SET  blokiran = 1 WHERE korisnickoIme = '$korime'";

    $baza->selectDB($sql);

    if ($baza->pogreskaDB()) {
        echo "Problem kod upita na bazu podataka!";
        exit;
    }

    $baza->zatvoriDB();
}


require 'vanjske_biblioteke/Smarty/libs/Smarty.class.php';
//$tipP = $_SESSION["tip"];
$smarty = new Smarty;
$smarty->assign("naslov", "Otkljucavanje i blokiranje korisnika");
//$smarty->assign("tip", $tipP);
$smarty->display('predlosci/_header.tpl');
//$smarty->display('predlosci/_navigation.tpl');

include 'navigacija.php';
?>

<div class="tablica" style="margin: 10px; margin-top: 20px;">
    <table id="tablica" border="none" class="display"style="margin: 10px; margin-top: 20px;">
        <thead>
            <tr>
                <th>Korisničko ime</th>
                <th>Email</th>
        </thead>
        <tbody>
            <?php blokiraj_korisnike_lista() ?>
    </table>
    <form method="POST" action="otkljucavanje_korisnika.php">
       Blokiraj korisnika:<br>
        <select name="korisnik"><?php selekcija_korisnika() ?></select><br>
        <button type="submit" name="blokiraj" value="Blokiraj">Blokiraj korisnike</button></form>
</div>

</section>
<?php
$smarty2 = new Smarty;
$smarty2->display('predlosci/_footer.tpl');
?>