<?php
include ("sesija.class.php");
include ("baza.class.php");
require 'vanjske_biblioteke/Smarty/libs/Smarty.class.php';
$kat = array();
$greska = "";

Sesija::kreirajSesiju();
if ($_SESSION["tip"] != 1) {
    echo "Preusmjeravanje...";
    header("Location: index.php");
}

$korisnik = $_SESSION["id_korisnika"];
$baza = new Baza();
$baza->spojiDB();

$datum = date("Y-m-d H:i:s");

$upit = "INSERT INTO dnevnik_rada VALUES(DEFAULT, '$korisnik', '$datum', 'Ulazak u kreirajKategoriju.php' )";
$baza->selectDB($upit);

$baza->zatvoriDB();

$smarty = new Smarty;
$smarty->assign("naslov", "Kreiraj kategoriju");
//$smarty->assign("tip", $tipP);
$smarty->display('predlosci/_header.tpl');

include 'navigacija.php';
function popisModeratora() {
    $baza = new Baza();
    $baza->spojiDB();
    
    $sql = "SELECT idkorisnika, ime, tip_korisnika_idtip_korisnika FROM korisnik WHERE tip_korisnika_idtip_korisnika = 2 ";
    
    $rezultat = $baza->selectDB($sql);
    
    $idmoderatora = array();
    $nazivmoderatora = array();
    while ($polje = mysqli_fetch_array($rezultat)){
        
        echo "<option>" . $polje["ime"] . "</option>";
        array_push($idmoderatora, $polje["idkorisnika"]);
        array_push($nazivmoderatora, $polje["ime"]);
        
    }
   
        global $kat;
        $kat = array_combine($nazivmoderatora, $idmoderatora);

    $baza->zatvoriDB();
}

function popisBoja() {
    $i = 1;
    while ($i <= 3) {
        echo "<option>boja" . $i . "</option>";
        $i++;
    }
}

function popisFont() {
    $i = 1;
    while ($i <= 3) {
        echo "<option>font" . $i . "</option>";
        $i++;
    }
}

function popisPozadina() {
    $i = 1;
    while ($i <= 3) {
        echo "<option>pozadina" . $i . "</option>";
        $i++;
    }
}
?>
<form id="kreiranjeKategorije" name="kreiranjeKategorije"  method="post" class="def">

    <div style="display: inline-block">
        <label for="nazivKategorije"  id="nazivKategorijeLabel" >Naziv kategorije: </label>
        <input id="nazivKategorije" type="text" name="nazivKategorije" ><br>

        <label for="boja"  id="bojaKategorijeLabel" >Boja kategorije: </label>
        <select style="float: right; margin: 5px;" id="boja" name="boja"><?php popisBoja() ?></select><br>

        <label for="font"  id="fontKategorijeLabel" >Font kategorije: </label>
        <select style="float: right; margin: 5px;" id="font" name="font"><?php popisFont() ?></select><br>

        <label for="pozadina"  id="pozadinaKategorijeLabel" >Pozadina kategorije: </label>
        <select style="float: right; margin: 5px;" id="pozadina" name="pozadina"><?php popisPozadina() ?></select><br>

        <label for="moderator"  id="moderatorKategorijeLabel" >Moderator kategorije: </label>
        <select style="float: right; margin: 5px;" id="moderator" name="moderator"><?php popisModeratora() ?></select><br>




    </div>
    <div style="display: block">
        <button style="float: right; margin: 5px; display: block;"  type="submit" name="kreiranjeKategorije" value="kreiranjeKategorije">Dodaj kategoriju</button>
    </div>
</form>

<?php
if (!empty($_POST["kreiranjeKategorije"])) {
    $baza = new Baza;
    $baza->spojiDB();
    $ispravno = true;
    $nijePopunjeno = false;
    foreach ($_POST as $key => $value) {
        if (empty($_POST[$key])) {
            global $greska;
            $greska .= "Nisu popunjena sva polja! <br>";
            $popunjenaPolja = true;
            $ispravno = false;
            $nijePopunjeno = true;
        }
    }
    $uneseniNaziv = $_POST["nazivKategorije"];
    $uneseniModerator = $kat[$_POST["moderator"]];
    $imeMOderatora = $_POST["moderator"];
    $uneseniFont = $_POST["font"];
    $unesenaBoja = $_POST["boja"];
    $unesenaPozadina = $_POST["pozadina"];



    $sql = "INSERT INTO kategorija_usluga VALUES(DEFAULT, '$uneseniNaziv', '$unesenaBoja', '$uneseniFont', '$unesenaPozadina')";
    //echo "upit za rezervaciju<br>".$sql;
    $uspjesno = $baza->selectDB($sql);
    if ($baza->pogreskaDB()) {
        echo "Problem kod upita na bazu podataka!";
        exit;
    }
    
    $baza->zatvoriDB();
    $baza2 = new Baza;
    $baza2->spojiDB();
    
    
    $sql = "SELECT idkategorija , naziv FROM kategorija_usluga WHERE naziv='$uneseniNaziv'";
    //echo 'SQL= '.$sql;

        $rezultat = $baza2->selectDB($sql);

        if ($baza2->pogreskaDB()) {
            echo "Problem kod upita na bazu podataka!";
            exit;
        }
        $idBaza = mysqli_fetch_assoc($rezultat);
        $id = $idBaza["idkategorija"];
        
    $baza2->zatvoriDB();
    $baza3 = new Baza;
    $baza3->spojiDB();
    $sql = "INSERT INTO moderatori VALUES('$uneseniModerator','$id')";
    //echo "upit za moderatore<br>".$sql;
    $uspjesno = $baza3->selectDB($sql);
    if ($baza3->pogreskaDB()) {
        echo "Problem kod upita na bazu podataka!";
        exit;
    }
}
echo'<br><span class="greska" >' . $greska . '</span>" ';

$smarty2 = new Smarty;
$smarty2->display('predlosci/_footer.tpl');
?>



