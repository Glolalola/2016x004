<?php
include ("sesija.class.php");
include ("baza.class.php");
require 'vanjske_biblioteke/Smarty/libs/Smarty.class.php';
$kat = array();
$greska = "";
$kuponi = array();

Sesija::kreirajSesiju();
if ($_SESSION["tip"] != 1 && $_SESSION["tip"] != 2) {
    echo "Preusmjeravanje...";
    header("Location: index.php");
}
$korisnik = $_SESSION["id_korisnika"];
$baza = new Baza();
$baza->spojiDB();

$datum = date("Y-m-d H:i:s");

$upit = "INSERT INTO dnevnik_rada VALUES(DEFAULT, '$korisnik', '$datum', 'Ulazak u noviKupon.php' )";
$baza->selectDB($upit);

$baza->zatvoriDB();



$smarty = new Smarty;
$smarty->assign("naslov", "Definiraj kupon");
$smarty->display('predlosci/_header.tpl');

include 'navigacija.php';

function popisKategorija() {
    $baza = new Baza();
    $baza->spojiDB();
    if (isset($_SESSION["kategorije"])) {


        $kategorije = $_SESSION["kategorije"];

        $sql = "SELECT * FROM kategorija_usluga";

        $rezultat = $baza->selectDB($sql);

        if ($baza->pogreskaDB()) {
            echo "Problem kod upita na bazu podataka!";
            exit;
        }
        $idkategorije = array();
        $nazivkategorije = array();
        while ($polje = mysqli_fetch_array($rezultat)) {
            if (in_array($polje["idkategorija"], $kategorije)) {
                echo "<option>" . $polje["naziv"] . "</option>";
                array_push($idkategorije, $polje["idkategorija"]);
                array_push($nazivkategorije, $polje["naziv"]);
            }
        }
        global $kat;
        $kat = array_combine($nazivkategorije, $idkategorije);

        echo '</div>';
    }
    $baza->zatvoriDB();
}

function popisKupona() {
    $baza = new Baza();
    $baza->spojiDB();

    $sql = "SELECT * FROM akcija_kupona";

    $rezultat = $baza->selectDB($sql);

    if ($baza->pogreskaDB()) {
        echo "Problem kod upita na bazu podataka!";
        exit;
    }
    $idkupona = array();
    $nazivkupona = array();
    while ($polje = mysqli_fetch_array($rezultat)) {

        echo "<option>" . $polje["naziv"] . "</option>";
        array_push($idkupona, $polje["idakcija_kupona"]);
        array_push($nazivkupona, $polje["naziv"]);
    }
    global $kuponi;
    $kuponi = array_combine($nazivkupona, $idkupona);

    echo '</div>';

    $baza->zatvoriDB();
}
?>
<form id="definiranjeKupona" name="definiranjeKupona"  method="post" class="def">

    <div style="display: inline-block">
        <label for="kupon"  id="kuponLabel" >Kupon: </label>
        <select style="float: right; margin: 5px;" id="kupon" name="kupon"><?php popisKupona() ?></select><br>


        <label for="kategorijaKupona"  id="kategorijaKuponaLabel" >Kategorija kupona: </label>
        <select style="float: right; margin: 5px;" id="kategorijaKupona" name="kategorijaKupona"><?php popisKategorija() ?></select><br>

        <label for="datumPocetka" id="datumPocetkaLabel" >Datum pocetka kupona: </label>
        <input id="datumPocetka" type="date" name="datumPocetka"><br>

        <label for="datumKraja" id="datumKrajaLabel" >Datum kraja kupona: </label>
        <input id="datumKraja" type="date" name="datumKraja"><br>

        <label for="bodovi" id="bodoviLabel">Bodovi: </label>
        <input id="bodovi" name="bodovi" type="number" min="1" step="any" /><br>
    </div>
    <div style="display: block">
        <button style="float: right; margin: 5px; display: block;"  type="submit" name="dodaj" value="Dodaj">Dodaj kupon</button>
    </div>
</form>

<?php
if (!empty($_POST["dodaj"])) {
    $baza = new Baza;
    $baza->spojiDB();
    $ispravno = true;
    $nijePopunjeno = false;
    foreach ($_POST as $key => $value) {
        if (empty($_POST[$key])) {
            global $greska;
            $greska .= "Nisu popunjena sva polja! <br>";
            $popunjenaPolja = true;
            $ispravno = false;
            $nijePopunjeno = true;
        }
    }
    $uneseniKupon = $kuponi[$_POST["kupon"]];
    $unesenaKategorija = $kat[$_POST["kategorijaKupona"]];
    $uneseniPocetak = $_POST["datumPocetka"];
    $uneseniKraj = $_POST["datumKraja"];
    $uneseniBodovi = $_POST["bodovi"];



    $sql = "INSERT INTO kupon VALUES(DEFAULT, '$uneseniPocetak', '$uneseniKraj', '$uneseniBodovi', '$uneseniKupon',1 , '$unesenaKategorija')";
    //echo "upit za rezervaciju<br>".$sql;
    $uspjesno = $baza->selectDB($sql);
    if ($baza->pogreskaDB()) {
        echo "Problem kod upita na bazu podataka!";
        exit;
    }
}
echo'<br><span class="greska" >' . $greska . '</span>" ';

$smarty2 = new Smarty;
$smarty2->display('predlosci/_footer.tpl');
?>



