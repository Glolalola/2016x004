$(document).ready(function () {
    $('#tablica').dataTable();


    $('#ime').focusout(function (event) {
        var re = /^[A-ZŠČĆĐŽ].[a-zđšžćč]{1,}$/;
        if (re.test($('#ime').val())) {
            $('#korime').removeAttr('disabled');
            $('#ime').css('background', 'grey');
        } else {
            $('#ime').css('background', 'red');

        }
    });

    $('#prezime').focusout(function (event) {
        var re = /^[A-ZŠČĆĐŽ].[a-zđšžćč]{1,}$/;
        if (re.test($('#prezime').val())) {
            $('#korime').removeAttr('disabled');
            $('#prezime').css('background', 'grey');
        } else {
            $('#prezime').css('background', 'red');

        }
    });


    $("#korime").focus(function (event) {
        var ime = $("#ime").val();
        var prezime = $("#prezime").val();
        if (ime === "" || prezime === "") {
            $("#korime").attr("disabled", true);
            $("#korime").css('background', 'red');
        } else {
            $("#korime").removeAttr("disabled", true);
            $("#korime").css('background', 'grey');
        }
    });

    $("#korime").focusout(function (event) {
        var korime = $("#korime").val();
        var korime_provjera;
        $.ajax({
            url: '../ajax.php',
            data: {'korisnik': korime, 'id': "registracija"},
            type: 'GET',
            dataType: 'xml',
            success: function (xml) {
                $(xml).find('korisnicko_ime').each(function () {
                    korime_provjera = $(this).text();
                });
                if ($.isNumeric(korime_provjera)) {
                    $("#lozinka").removeAttr("disabled", true);
                    $("#status").text("Korisnik ne postoji!");
                    $("#status").css('color', 'green');

                } else {
                    $("#lozinka").attr("disabled", true);
                    $("#korime").css('color', 'red');
                    $("#status").css('color', 'red');
                    $("#status").text("Korisnik postoji. Promijenite korisničko ime.");
                }
                console.log(korime_provjera);
            },

            error: function (jqXHR, textStatus, errorThrown) {
                console.log(textStatus);
            }
        });
    });


    $('#lozinka').focusout(function (event) {
        var re = /^(?=(.*[a-z]){2})(?=(.*[A-Z]){2})(?=.*\d)[a-zA-Z\d]{5,15}$/;
        if (re.test($('#lozinka').val())) {
            $('#lozinka2').removeAttr('disabled');
            $('#lozinka').css('background', 'grey');
            $("#status").text("");
        } else {
            $('#lozinka2').attr('disabled');
            $('#lozinka').css('background', 'red');
            $("#status").css('color', 'red');
            $("#status").text("Lozinka mora sadržavati barem dva velika slova, dva mala slova, jedan broj i duljine je od 5 do 15 znakova.");

        }
    });
    $("#lozinka2").focus(function (event) {
        var lozinka = $("#lozinka").val();

        if (lozinka === "") {
            $("#lozinka2").attr("disabled", true);
            $("#lozinka2").css('background', 'red');
        } else {
            $("#lozinka2").removeAttr("disabled", true);
            $("#lozinka2").css('background', 'grey');
        }

    });
    $("#lozinka2").focusout(function (event) {
        var lozinka = $("#lozinka").val();
        var lozinka2 = $("#lozinka2").val();

        if (lozinka2 !== lozinka) {
            $("#lozinka2").css('background', 'red');
            $("#status").css('color', 'red');
            $("#status").text("Ponovljena lozinka se ne poklapa sa lozinkom.");
        } else {
            $("#lozinka2").css('background', 'grey');
            $("#status").text("");
        }
    });


}

);


$("#pomak").click(function (event) {

    $.ajax({
        type: 'GET',
        url: 'https://barka.foi.hr/WebDiP/pomak_vremena/pomak.php=json',
        data: {format: 'json'},
        dataType: 'json',
        success: function (e) {
            console.log(e);
            vrijednost = e.WebDiP.vrijeme.pomak.brojSati;
            $.ajax({
                type: 'POST',
                url: 'dohvaceniPomak.php',
                data: {action: 'pomak', pomak: vrijednost},
                dataType: 'json',
                success: function (json) {
                    alert("Uspješan pomak!");
                },
                error: function (e) {
                    alert("Neuspješan pomak.");
                }
            });
        },
        error: function (e) {
            alert("Greška!");
                                       
        }
    });
}
);
