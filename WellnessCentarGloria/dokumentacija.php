<?php
require 'vanjske_biblioteke/Smarty/libs/Smarty.class.php';
include ("sesija.class.php");

$smarty = new Smarty;
$smarty->assign("naslov", "Dokumenatcija");
$smarty->display('predlosci/_header.tpl');

include 'navigacija.php';
?>

<div id="opisProjektnogZadatka">
    <h2>
        Opis projektnog zadatka
    </h2>
    <div>
        Wellness centar je projektni zadatak na kolegiju Web Dizajn i Programiranje na Faklutetu organizacije i informatike.
        Cilj ovog projekta je olakšati rad Wellness centra u smislu prenošenja informacija korisnicima i omogućavanje rezervacija usluga i kupnje kupona za usluge.
        <br><br>
        Neprijavljeni korisnici mogu vidjeti kategorije usluga, te klikom na njih vide koje su tri najčešće rezervirane usluge unutar te kategorije.
        Osim toga naravno, mogu se prijaviti ako već posjeduju korisnički račun, ili kreirati korisnički račun tj. registrirati se.
        Registraciju je potrebno potvditi aktivacijskim kodom preko mail-a, a nakon toga korisnik se može prijaviti u sustav preko jednog koraka ili dva,
        ovisno kakvu su prijavu izabrali prilikom registracije. Prijava u jedan korak zahtjeva samo unos korisničkog imena i lozinke,
        dok prijava u dva koraka još zahtjeva i unos koda koji se dobije na mail.<br><br>

        Registrirani korisnici imaju mogućnost pregleda svih usluga te rezerviranja istih. Također, imaju pregled nad svojim rezerviranim terminima.
        Imaju i pregled nad svim kuponima za koje imaju dovoljno bodova te iste mogu staviti u košaricu i kupiti bodovima. 
        Bodovi se skupljaju koristeći aplikaciju.
        <br><br>

        Iduća vrsta korisnika je moderator. Ima sve ovlasti kao i obično korisnici ali i više. Moderator upravlja svojim kategorijama usluga za koje ga je zadužio administrator.
        Moderator može mijenjati izgled svojim kategorijama, potvrđivati ili odbijati termine rezervacije, kreirati nove usluge,
        određivati bodove za kupone za svoju kategoriju, proveravati postoje li kuponi prema njihovom kodu te pregledati sve svoje termine rezervacija.
        <br><br>
        Korisnik koji održava cijeli sustav je aministrator.
        Administrator kreira kategorije usluga i dodjeljuje moderatore kategorijama. 
        Također, stvara akcije kupona koje kasnije moderatori nadopunjuju za svoje sekcije. 
        Administrator može manipulirati virtualnim vremenom i ostalim konfiguracijama sustava. 
        Može vidjeti lojalnost korisnika u obliku statističkih podataka i grafova te na stranici te može otključavati i zaključavati korisnike.
        Općenito, može manipulirati svim podacima u bazi.

    </div>
</div>

<div id="opisProjektnogRješenja">
    <h2>
        Opis projektnog rješenja
    </h2>
    <div>
        Rješenje projektnog zadatka je realizirano pomoću nekoliko različitih jezika. 
        Sadržaj je kreiran u HTML-u, a izgled u CSS-u.
        Većina sadržaja na stranici se ispisuje preko PHP-a. 
        JQuery framework se koristio prilikom registracije za validaciju korisničkih podataka te za straničenje i pretraživanje tablica putem DataTables plug-ina.
        Svi podaci se dohvaćaju PHP-om iz MySQL baze. 
        <br><br>
        PHP-om je implementirano ograničenje sadržaja ovisno o ulogama korisnika.
        Sadržaj je ograničen putem sesija i kolačića u kojima su se nalazili podaci o korisniku kako bi se stranica njemu prilagodila.
        Korišten je i Smarty framework koji služi za odvajanje prezentacijskog i programskog djela web stranice.
        Međutim, samo su zaglavlje i podnožje realizirani na ovaj način.
        Kod prijave i registracije je korišten i HTTPS protokol.
    </div>
</div>

<div id="eraMOdel">
    <h2>
        ERA model
    </h2>
    <div>
        ERA model projektnog rješenja sastoji se od 18 tablica.
        Korištena je MySQL baza podataka.

        <br><br>
        <div style="text-align: center">
            <a href="slike/ERA.JPG"><img src="slike/ERA.JPG"><br>
                ERA model</a>
        </div>
    </div>
</div>

<div id="popis">
    <h2>
        Popis skripata, mapa mjesta i navigacijski dijagram
    </h2>
    <div>
        Navigacijski dijagram ćemo detaljnije opisati pomoću skripti.

        <br><br>
        <div style="text-align: center">
            <a href="slike/navigacijski.png"><img src="slike/navigacijski.png"><br>
                Navigacijski model</a>
        </div>

        <div>
            Popis direktorija:
            <ul>
                <li><i>css</i> - ovdje je smještena datoteka css koja određuje izgled stranice</li>
                <li><i>slike</i> - ovdje su smještene slike koje se pojavljuju na stranici</li>
                <li><i>js</i> - ovdje je smještena JavaScript JQuery datoteka zaslužna za izgled tablica i korisničku validaciju podataka</li>
                <li><i>predlosci</i> - ovdje su smješteni template-ovi (HTML) korišteni kod Smarty framework-a</li>
                <li><i>templates_c</i> - ovdje su smještene datoteke koje generira Smarty na temelju template-ova</li>
                <li><i>vanjske_biblioteke</i> - ovdje je smješten Smarty framework</li>
                <li><i>datoteke</i> - ovdje su smještenu PDF-ovi za kupone</li>
                <li><i>privatno</i> - ovdje je smještena skripta za ispis korisnika</li>
            </ul><br><br>

            Popis skripti:
            <ul>
                <li><i>ajax.php</i> - dohvaća korisnike kako bi se prilikom registracije sa ajaxom provjerilo postojanje korisničkog imena</li>
                <li><i>aktivacija.php</i> - provjerava da li je korisnik unio dobar aktivacijski kod</li>
                <li><i>aktivniKuponi.php</i> - prikazuje aktivne kupone koje za koje korisnik ima dovoljno bodova</li>
                <li><i>baza.class.php</i> - povezuje se na bazu podataka, izvršava upite, javlja greške i zatvara bazu</li>
                <li><i>blokiran.php></i> - pokreće se kada želimo korisnika obavijestiti da mu ej račun blokiran</li>
                <li><i>blokiranje_korisnika.php</i> - omogućava administratoru blokiranje korisnika</li>
                <li><i>definirajUsluge.php</i> - omogućava moderatoru kreiranje novih usluga unutar njegovih kategorija</li>
                <li><i>dohvacniPomak.php</i> - dohvaća pomak vremena</li>
                <li><i>dokumentacija.php</i> - prikazuje dokumenatciju sustava</li>
                <li><i>drugaPrijava.php</i> - zahtjeva unos koda prilikom drugog koraka prijave</li>
                <li><i>index.php</i> - početna stranica koja pokazuje popis kategorija te klikom na kategoriju popis usluga unutar te kategorije</li>
                <li><i>kalendar.php</i> - moderatoru pokazuje prema odabranom datumu sve potvrđene rezervacije u tom danu</li>
                <li><i>kosarica.php</i> - pokazuje sve kupone koje je korisnik odabrao da želi kupiti</li>
                <li><i>kreirajKategoriju.php</i> - omogućava administratoru kreiranje nove kategorije i dodjelu moderatora toj kategoriji</li>
                <li><i>kreiranjeKupona.php</i> - omogućava administratoru kreiranje nove akcije kupona sa slikom, videom i pdf-om</li>
                <li><i>krivaPrijava.php</i> - obavještava korisnika da se pokušao prijaviti sa krivim podacima i ažurira broj pokušaja krive prijave u bazu</li>
                <li><i>kupon.php</i> - prikazuje detalje o kuponu i omogućava dodavanje kupona u košaricu</li>
                <li><i>lojalnost.php</i> - nije implementirana do kraja, trebala je prikazivati statistiku korisnika</li>
                <li><i>mojeRezervacije.php</i> - prikazuje korisniku njegove rezervirane termine</li>
                <li><i>navigacija.php</i> - prikazuje navigaciju ovisno o tipu korisnika</li>
                <li><i>neregistrirani.php</i> - pokreće se kada stranici pristupa neregistrirani korisnik i prikazuje index.php samo sa kategorijama i tri najčešće rezervirane usluge</li>
                <li><i>noviKupon.php</i> - omogućava moderatoru biranje i doradu kupona za njegovu kategoriju</li>
                <li><i>oAutoru.php</i> - prikazuje podatke o autoru</li>
                <li><i>odjava.php</i> - poziva se prilikom odjave sa stranice, briše sesiju</li>
                <li><i>otkljucavanje_korisnika.php</i> - omogućava administratoru otključavanje blokiranih korisnika</li>
                <li><i>pomakVremena.php</i> - omogućava administratoru unos i dohvaćanje pomaka vremena</li>
                <li><i>prijava.php</i> - omogućava korisnicima prijavu u sustav</li>
                <li><i>printKodova.php</i> - poziva se kada korisnik u košarici želi isprintati kodove kupona, otvara pregled za ispis</li>
                <li><i>promijeniIzgled.php</i> - omogućava moderatoru mijenjanje izgleda njegovih kategorija</li>
                <li><i>provjeriKupon.php</i> - omogućava moderatoru provjeru da li je kupon stvaran ili lažan</li>
                <li><i>recaptchalib.php</i> - omogućava korištenje recaptcha provjere je li korisnik ili robot</li>
                <li><i>registracija.php</i> - omogućava registraciju novog korisnika u sustav</li>
                <li><i>registrirani.php</i> - prikazuje sadržaj početne stranice za registrirane korisnike</li>
                <li><i>rezervacija.php</i> - omogućava korisnicima rezervaciju odabrane usluge</li>
                <li><i>rezervacijeModerator.php</i> - omogućava moderatoru potvrđivanje ili odbijanje rezervacije te prijedlog novog termina</li>
                <li><i>sesija.class.php</i> - kreira i briše sesiju</li>
                <li><i>skupljeniBodovi.php</i> - prikazuje korisniku broj skupljenih bodova i na čemu ih je skupio</li>
                <li><i>uploadPDF.php</i> - omogućava upload pdf-a</li>
                <li><i>uploadSlike.php</i> - omogućava upload slike</li>
                <li><i>uploadVideo.php</i> - omogućava unos videa u bazu</li>
                <li><i>uploaderPDF.php</i> - postavlja PDF u bazu</li>
                <li><i>uploaderSlike</i> - postavlja Sliku u bazu</li>
                <li><i>zaboravljena.php</i> - šalje korisniku novu lozinku na mail</li>

                <li><i>css/globabic.css</i> - određuje dizajn stranice</li>
                <li><i>js/globabic_jquery.js</i> - provjerava podatke pri registraciji sa korisničke strane, prikazuje tablice, dohvaća pomak</li>
                <li><i>predlosci/_footer.tpl</i> - sadrži prikaz podnožja</li>
                <li><i>predlosci/_header.tpl</i> - sadrži prikaz zaglavlja</li>

                <li><i>privatno/korisnici.php</i> - ispisuje sve korisnike iz baze</li>
                <li><i>privatno/.htaccess</i> - omogućava pristup ispisu korisnika</li>
                <li><i>privatno/.htpasswd</i> - sprema lozinku za pristup ispisu korisnika</li>

            </ul><br>
        </div>
    </div>

    <div>
        <h2>
            Popis i opis korištenih tehnologija i alata
        </h2>
        <div>
            Popis tehnologija:
            <ul>
                <li><i>HTML</i> - prezentacijski jezik za izradu sadržaja web stranica</li
                <li><i>CSS</i> - stilski jezik koji se rabi za opis prezentacije dokumenta napisanog pomoću markup (HTML) jezika</li>
                <li><i>PHP</i> - skriptni jezik namjenjen za web development</li>
                <li><i>Javascript</i> - skriptni programski jezik koji se izvršava u web pregledniku na strani korisnika</li>
                
            </ul>

            Popis alata:
            <ul>
                <li><i>Navicat for MySQL</i> -  korišten za izradu ERA modela</li>
                <li><i>NetBeans IDE</i> - korišten za pisanje koda</li>
                <li><i>Chrome</i> - korišten za pregled stranice</li>
                <li><i>FileZilla</i> - korištena za prijenos na barku</li>
                <li><i>phpMyAdmin</i> - korišten za pohranu baze</li>
                
            </ul>
        </div>

    </div>
     <div>
        <h2>
            popis i opis vanjskih (tuđih) modula/biblioteka i njihovo korištenje u skriptama
        </h2>
        <div>
            <ul>
                <li><i>uploaderPDF.php</i> - postavlja PDF u bazu, preuzeto s Moodla (dorađeno)</li>
                <li><i>uploaderSlike</i> - postavlja Sliku u bazu, preuzeto s Moodla (dorađeno)</li>
                <li><i>sesija.class.php</i> - kreira i briše sesiju, preuzeto s Moodla (dorađeno)</li>
                <li><i>baza.class.php</i> - povezuje se na bazu podataka, izvršava upite, javlja greške i zatvara bazu, preuzeto s Moodla</li>
                <li><i>recaptchalib.php</i> - omogućava korištenje recaptcha provjere je li korisnik ili robot</li>
                
            </ul>
        </div>

    </div>
    

    <?php
    $smarty2 = new Smarty;
    $smarty2->display('predlosci/_footer.tpl');
    ?>
