<?php
include("baza.class.php");
include("sesija.class.php");

Sesija::kreirajSesiju();

if ($_SESSION["tip"] != 1 && $_SESSION["tip"] != 2) {
    echo "Preusmjeravanje...";
    header("Location: index.php");
}

$korisnik = $_SESSION["id_korisnika"];
$baza = new Baza();
$baza->spojiDB();

$datum = date("Y-m-d H:i:s");

$upit = "INSERT INTO dnevnik_rada VALUES(DEFAULT, '$korisnik', '$datum', 'Ulazak u rezervacijeModerator.php.' )";
$baza->selectDB($upit);

$baza->zatvoriDB();

require 'vanjske_biblioteke/Smarty/libs/Smarty.class.php';
$smarty = new Smarty;
$smarty->assign("naslov", "Rezervacije");
$smarty->display('predlosci/_header.tpl');

include 'navigacija.php';
$baza = new Baza();
$baza->spojiDB();

$greska = "";

$korisnik = $_SESSION["id_korisnika"];
$datum = date("Y-m-d H:i:s");
$sql = "INSERT INTO bodovi VALUES(8, '$korisnik', '$datum',5)";
//echo "<br>" . $sql;
$uspjesno = $baza->selectDB($sql);
if ($baza->pogreskaDB()) {
    echo "Problem kod upita na bazu podataka!";
    exit;
}

function rezervacijeModeratora() {

    $baza = new Baza();
    $baza->spojiDB();
    $korisnik = $_SESSION["id_korisnika"];

    $sql = "SELECT naziv, datum, vrijeme, potvrda, korisnik_idkorisnik, idusluga , idrezervacije, noviTermin, kategorija_usluga_idkategorija FROM usluga, rezervacije WHERE korisnik_idkorisnik='$korisnik' AND usluga_idusluga=idusluga ";
    $rezultat = $baza->selectDB($sql);

    if ($baza->pogreskaDB()) {
        echo "Problem kod upita na bazu podataka!";
        exit;
    }
    $kategorije = $_SESSION["kategorije"];
    while ($polje = mysqli_fetch_array($rezultat)) {
        if (in_array($polje["kategorija_usluga_idkategorija"], $kategorije)) {
            echo "<tr><td>" . $polje["naziv"] . "</td><td>" . $polje["datum"] . "</td><td>" . $polje["vrijeme"] . "</td>";
            if ($polje["potvrda"] == 0) {
                echo "<td><a href='rezervacijeModerator.php?rezervacijaPotvrdi=" . $polje["idrezervacije"] . "'>Potvrdi</a></td>";
            } else {
                echo "<td></td><td></td>";
            }

            if ($polje["potvrda"] == 1 && $polje["noviTermin"] == 1) {
                echo'<td><p>Potvrđen novi termin</p></td>';
            } else {
                if ($polje["potvrda"] == 0 && $polje["noviTermin"] == 0) {
                    echo'<td><a href="rezervacijeModerator.php?rezervacijaOdbij=' . $polje["idrezervacije"] . '">Odbij</a></td>
                    <td><a href="rezervacijeModerator.php?noviTermin=' . $polje["idrezervacije"] . '">Predloži</a></td>';
                } else {
                    echo "<td><p>Čeka se potvrda novog termina</p></td><td></td>";
                }
            }
        }
        echo"</tr>";
    }
    if (isset($_GET['rezervacijaPotvrdi'])) {
        $rezervacija = $_GET['rezervacijaPotvrdi'];
        $sql = "UPDATE rezervacije SET potvrda = 1 WHERE idrezervacije = '{$rezervacija}'";
        $baza->selectDB($sql);
        if ($baza->pogreskaDB()) {
            echo "Problem kod upita na bazu podataka!";
            exit;
        }
        $baza2 = new Baza();
        $baza2->spojiDB();
        $sql2 = "SELECT korisnik_idkorisnik FROM rezervacije WHERE idrezervacije = '{$rezervacija}'";

        if ($baza2->pogreskaDB()) {
            echo "Problem kod upita na bazu podataka!";
            exit;
        }
        $rezultat2 = $baza2->selectDB($sql2);
        $primatelj = mysqli_fetch_assoc($rezultat2);
        $idPrimatelja = $primatelj['korisnik_idkorisnik'];

        $baza3 = new Baza();
        $baza3->spojiDB();
        $sql3 = "SELECT email FROM korisnik WHERE idkorisnika = '$idPrimatelja'";

        if ($baza2->pogreskaDB()) {
            echo "Problem kod upita na bazu podataka!";
            exit;
        }
        $baza2->zatvoriDB();
        $rezultat3 = $baza3->selectDB($sql3);
        $email = mysqli_fetch_assoc($rezultat3);

        $baza3->zatvoriDB();

        $mail_to = $email['email'];
        $mail_subject = "Wellness rezervacija";
        $mail_body = "Vaša rezervacija za wellness centar je potvrđena.";
        mail($mail_to, $mail_subject, $mail_body);

        echo "<meta http-equiv=\"refresh\" content=\"0;URL=rezervacijeModerator.php\">";
    }

    if (isset($_GET['rezervacijaOdbij'])) {
        $rezervacija = $_GET['rezervacijaOdbij'];
        $sql = "UPDATE rezervacije SET potvrda = 0 WHERE idrezervacije = '{$rezervacija}'";
        $baza->selectDB($sql);
        if ($baza->pogreskaDB()) {
            echo "Problem kod upita na bazu podataka!";
            exit;
        }

        $baza2 = new Baza();
        $baza2->spojiDB();
        $sql2 = "SELECT korisnik_idkorisnik FROM rezervacije WHERE idrezervacije = '{$rezervacija}'";

        if ($baza2->pogreskaDB()) {
            echo "Problem kod upita na bazu podataka!";
            exit;
        }
        $rezultat2 = $baza2->selectDB($sql2);
        $primatelj = mysqli_fetch_assoc($rezultat2);
        $idPrimatelja = $primatelj['korisnik_idkorisnik'];

        $baza3 = new Baza();
        $baza3->spojiDB();
        $sql3 = "SELECT email FROM korisnik WHERE idkorisnika = '$idPrimatelja'";

        if ($baza2->pogreskaDB()) {
            echo "Problem kod upita na bazu podataka!";
            exit;
        }
        $baza2->zatvoriDB();
        $rezultat3 = $baza3->selectDB($sql3);
        $email = mysqli_fetch_assoc($rezultat3);
        $baza3->zatvoriDB();


        $mail_to = $email['email'];
        $mail_subject = "Wellness rezervacija";
        $mail_body = "Vaša rezervacija za wellness centar je odbijena.";
        mail($mail_to, $mail_subject, $mail_body);
        echo "<meta http-equiv=\"refresh\" content=\"0;URL=rezervacijeModerator.php\">";
    }
}
?>
<div class="tablica" style="margin: 10px; margin-top: 20px;">
    <table id="tablica" border="none" class="display"style="margin: 10px; margin-top: 20px;">
        <thead>
            <tr>
                <th>Usluga</th>
                <th>Datum</th>
                <th>Vrijeme</th>
                <th>Potvrdi</th>
                <th>Odbij</th>
                <th>Predloži novi termin</th>

        
        </thead>
        <tbody>
            <?php rezervacijeModeratora(); ?>
        </tbody>
    </table>

    <br><span class='greska' ><?php echo $greska; ?></span>
</div>

<?php
if (isset($_GET['noviTermin'])) {

    echo '<form id="predlozi" name="predlozi" novalidate method="post">

                        <label for="datumRezervacije"  id="datumRezervacijeLabel" >Datum rezervacije </label>
                        <input id="datumRezervacije" type="date" name="datumRezervacije" ><br>

                        <label for="vrijemeRezervacije" id="vrijemeRezervacijeLabel" >Vrijeme rezervacije: </label>
                        <input id="vrijemeRezervacije" type="time" name="vrijemeRezervacije" ><br>

                        <input id="predlozi" type="submit" style="margin: 10px;"name="predlozi" value="Predloži"><br>

                    </form>
                    
                                
                ';

    $rezervacija = $_GET['noviTermin'];

    $baza4 = new Baza;
    $baza4->spojiDB();
    $sql4 = "SELECT usluga_idusluga, idrezervacije FROM rezervacije WHERE idrezervacije = '{$rezervacija}'";
    if ($baza4->pogreskaDB()) {
        echo "Problem kod upita na bazu podataka!";
        exit;
    }
    $rezultat4 = $baza->selectDB($sql4);
    $res = mysqli_fetch_assoc($rezultat4);
    $usluga = $res['usluga_idusluga'];
    $zauzeto = false;
    $ispravno = true;
    $baza4->zatvoriDB();
    
    
    if (isset($_POST["predlozi"])) {
        
        $baza->spojiDB();
        $uneseniDatum = $_POST["datumRezervacije"];
        //echo 'datum' . $uneseniDatum;
        $unesenoVrijeme = $_POST["vrijemeRezervacije"];
        //echo 'vrijeme' . $unesenoVrijeme ;
        $danas = date("Y-m-d H:i:s");
        if ($uneseniDatum < $danas) {
            global $greska;
            $greska .= "Datum ne može biti u prošlosti!";
            $ispravno = false;
        }

        $upit = "SELECT datum, vrijeme, vrijeme_trajanja, potvrda FROM rezervacije, usluga WHERE usluga_idusluga = '$usluga' AND usluga_idusluga = idusluga AND potvrda = 1";

        if ($baza->pogreskaDB()) {
            global $greska;
            $greska .= "Problem kod upita na bazu podataka!<br>";
            exit;
        }
        $rezultat = $baza->selectDB($upit);
        while ($res = mysqli_fetch_array($rezultat)) {
            if ($res["datum"] == $uneseniDatum) {
                $prvaProvjera = $unesenoVrijeme > $res["vrijeme"];
                $drugaProvjera = $unesenoVrijeme < ($res["vrijeme"] + date('H', $res["vrijeme_trajanja"]));
                $trecaProvjera = ($unesenoVrijeme + $trajajeUsluge) > $res["vrijeme"];
                $cetvrtaProvjera = $unesenoVrijeme < $res["vrijeme"];

                if (($prvaProvjera && $drugaProvjera) || ($trecaProvjera && $cetvrtaProvjera)) {
                    global $greska;
                    $greska .= "Taj termin je već rezerviran!";
                    $zauzeto = true;
                }
            }
        }
        
        if (!$zauzeto && $ispravno) {

            $baza5 = new Baza();
            $baza5->spojiDB();

            $sql5 = "UPDATE rezervacije SET datum = '$uneseniDatum', vrijeme = '$unesenoVrijeme' , noviTermin = 1 WHERE idrezervacije = {$rezervacija} ";
            
            $baza5->selectDB($sql5);

            if ($baza5->pogreskaDB()) {
                echo "Problem kod upita na bazu podataka!";
                exit;
            }
            $baza5->zatvoriDB();
            $baza6 = new Baza();
            $baza6->spojiDB();
            $sql6 = "SELECT korisnik_idkorisnik FROM rezervacije WHERE idrezervacije = '{$rezervacija}'";

            if ($baza6->pogreskaDB()) {
                echo "Problem kod upita na bazu podataka!";
                exit;
            }
            $rezultat6 = $baza6->selectDB($sql6);
            $primatelj = mysqli_fetch_assoc($rezultat6);
            $idPrimatelja = $primatelj['korisnik_idkorisnik'];
            $baza6->zatvoriDB();

            $baza7 = new Baza();
            $baza7->spojiDB();
            $sql7 = "SELECT email FROM korisnik WHERE idkorisnika = '$idPrimatelja'";

            if ($baza7->pogreskaDB()) {
                echo "Problem kod upita na bazu podataka!";
                exit;
            }
            $rezultat7 = $baza7->selectDB($sql7);
            $email = mysqli_fetch_assoc($rezultat7);

            $baza7->zatvoriDB();

            $mail_to = $email['email'];
            $mail_subject = "Wellness rezervacija";
            $mail_body = "Predložen vam je novi termin za vašu rezervaciju. Ako vam termin odgovara, molimo Vas da ga potvrdite na: https://barka.foi.hr/WebDiP/2016_projekti/WebDiP2016x004/mojeRezervacije.php.";
            mail($mail_to, $mail_subject, $mail_body);
        }

        echo "<meta http-equiv=\"refresh\" content=\"0;URL=rezervacijeModerator.php\">";
    }
}
$baza->zatvoriDB();

$smarty2 = new Smarty;
$smarty2->display('predlosci/_footer.tpl');
?>

