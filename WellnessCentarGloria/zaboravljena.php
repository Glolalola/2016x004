<style>
<?php include 'css/globabic.css'; ?>
</style>
<?php
include("sesija.class.php");
include("baza.class.php");
$uspjesno = false;

require 'vanjske_biblioteke/Smarty/libs/Smarty.class.php';

$smarty = new Smarty;
$smarty->assign("naslov", "Zaboravljena lozinka");
$smarty->display('predlosci/_header.tpl');

$ispravno = true;
$greska = "";

if (isset($_POST["submit"])) {
    $greska = "";
    
    $baza = new Baza();
    $baza->spojiDB();

    
    $korime = mysqli_real_escape_string($baza->spojiDB(), $_POST["korime"]);
    $email = mysqli_real_escape_string($baza->spojiDB(), $_POST["email"]);
    

    $upit = "SELECT email FROM korisnik WHERE email = '$email'";
    $res = $baza->selectDB($upit);
    if ($baza->pogreskaDB()) {
        $greska .= "Problem kod upita na bazu podataka!<br>";
        exit;
    }
    $polje_email = mysqli_fetch_array($res);
    if ($email != $polje_email["email"]) {
        $greska .= "Ne postoji korisnik s unesenom email adresom!<br>";
        $ispravno = false;
    }
    
    $upit = "SELECT korisnickoIme FROM korisnik WHERE korisnickoIme = '$korime'";
    $res = $baza->selectDB($upit);
    if ($baza->pogreskaDB()) {
        $greska .= "Problem kod upita na bazu podataka!<br>";
        exit;
    }
    $polje_kor_ime = mysqli_fetch_array($res);
    if ($korime != $polje_kor_ime["korisnickoIme"]) {
        $greska .= "Ne postoji korisnik s unesenim korisnickim imenom!<br>";
        $ispravno = false;
    }
    
    if($ispravno){
        $novaLozinka = sha1(time());
        $salt = 'jkwgvurfzt47rz3648r437trf3478tr6f34 tf734trf3496';
        $kriptlozinka = sha1($novaLozinka) . $salt;
        $mail_to = $polje_email["email"];
        $mail_subject = "Nova lozinka";
        $mail_body = "Vaša nova lozinka je: " . $novaLozinka;
        mail($mail_to, $mail_subject, $mail_body);
        
        $sql = "UPDATE korisnik SET lozinka ='$novaLozinka' WHERE korisnickoIme ='$korime'AND email ='$email'";
        $uspjesno = $baza->selectDB($sql);
        
        $sql = "UPDATE korisnik SET hash_lozinka ='$kriptlozinka' WHERE korisnickoIme ='$korime'AND email ='$email'";
        $uspjesno = $baza->selectDB($sql);
        

        if ($baza->pogreskaDB()) {
            echo "Problem kod upita na bazu podataka!";
            exit;
        }
        
        
    }
}
?>

    <h2>Zaboravljena lozinka</h2>

    <form id="unos_podataka" method="post" novalidate>
        <label for="kod">Unesite svoju email adresu i korisničko ime</label><br>
        <input type="text" name="email" id="email" /><br/>
        <br>
        <input type="text" name="korime" id="korime" /><br/>
        <br>
        <input type="submit" name="submit" id="zaboravljena" value="Pošalji"/><br>
        
        <br><?php echo "<span class='greska' >" . $greska . "</span>" ;
        if($uspjesno){
            echo '<span>Nova lozinka Vam je poslana na email</span><br>'
            . '<a href="prijava.php">Vrati se na prijavu</a>';
        }
                ?>
        <div id='greska'></div>
        <br>

    </form>



