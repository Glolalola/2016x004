<?php
include ("sesija.class.php");
include ("baza.class.php");
require 'vanjske_biblioteke/Smarty/libs/Smarty.class.php';
$kat = array();
$greska = "";

Sesija::kreirajSesiju();
if ($_SESSION["tip"] != 1 && $_SESSION["tip"] != 2) {
    echo "Preusmjeravanje...";
    header("Location: index.php");
}
$korisnik = $_SESSION["id_korisnika"];
$baza = new Baza();
$baza->spojiDB();

$datum = date("Y-m-d H:i:s");

$upit = "INSERT INTO dnevnik_rada VALUES(DEFAULT, '$korisnik', '$datum', 'Ulazak u promijeniIzgled.php.' )";
$baza->selectDB($upit);

$baza->zatvoriDB();


$smarty = new Smarty;
$smarty->assign("naslov", "Promijeni izgled");
//$smarty->assign("tip", $tipP);
$smarty->display('predlosci/_header.tpl');

include 'navigacija.php';

function popisKategorija() {
    $baza = new Baza();
    $baza->spojiDB();
    if (isset($_SESSION["kategorije"])) {


        $kategorije = $_SESSION["kategorije"];

        $sql = "SELECT * FROM kategorija_usluga";

        $rezultat = $baza->selectDB($sql);

        if ($baza->pogreskaDB()) {
            echo "Problem kod upita na bazu podataka!";
            exit;
        }
        $idkategorije = array();
        $nazivkategorije = array();
        while ($polje = mysqli_fetch_array($rezultat)) {
            if (in_array($polje["idkategorija"], $kategorije)) {
                echo "<option>" . $polje["naziv"] . "</option>";
                array_push($idkategorije, $polje["idkategorija"]);
                array_push($nazivkategorije, $polje["naziv"]);
            }
        }
        global $kat;
        $kat = array_combine($nazivkategorije, $idkategorije);

        echo '</div>';
    }
    $baza->zatvoriDB();
}

function popisBoja() {
    $i = 1;
    while ($i <= 3) {
        echo "<option>boja" . $i . "</option>";
        $i++;
    }
}

function popisFont() {
    $i = 1;
    while ($i <= 3) {
        echo "<option>font" . $i . "</option>";
        $i++;
    }
}

function popisPozadina() {
    $i = 1;
    while ($i <= 3) {
        echo "<option>pozadina" . $i . "</option>";
        $i++;
    }
}
?>
<form id="promjenaIzgleda" name="promjenaIzgleda"  method="post" class="def">

    <div style="display: inline-block">

        <label for="kategorija"  id="kategorijaLabel" >Kategorija: </label>
        <select style="float: right; margin: 5px;" id="kategorija" name="kategorija"><?php popisKategorija() ?></select><br>

        <label for="boja"  id="bojaKategorijeLabel" >Boja kategorije: </label>
        <select style="float: right; margin: 5px;" id="boja" name="boja"><?php popisBoja() ?></select><br>

        <label for="font"  id="fontKategorijeLabel" >Font kategorije: </label>
        <select style="float: right; margin: 5px;" id="font" name="font"><?php popisFont() ?></select><br>

        <label for="pozadina"  id="pozadinaKategorijeLabel" >Pozadina kategorije: </label>
        <select style="float: right; margin: 5px;" id="pozadina" name="pozadina"><?php popisPozadina() ?></select><br>

    </div>
    <div style="display: block">
        <button style="float: right; margin: 5px; display: block;"  type="submit" name="promijeni" value="Promijeni">Promijeni</button>
    </div>



    <?php
    if (!empty($_POST["promijeni"])) {
        $baza = new Baza;
        $baza->spojiDB();
        
        $unesenaKategorija = $_POST["kategorija"];
        $unesenaBoja = $_POST["boja"];
        $uneseniFont = $_POST["font"];
        $unesenaPozadina = $_POST["pozadina"];

        $sql = "UPDATE kategorija_usluga SET boja = '$unesenaBoja', font = '$uneseniFont', pozadina = '$unesenaPozadina' WHERE naziv = '$unesenaKategorija' ";
        //echo "upit za hh<br>".$sql;
        $uspjesno = $baza->selectDB($sql);
        if ($baza->pogreskaDB()) {
            echo "Problem kod upita na bazu podataka!";
            exit;
        }
    }
    

    $smarty2 = new Smarty;
    $smarty2->display('predlosci/_footer.tpl');
    ?>

