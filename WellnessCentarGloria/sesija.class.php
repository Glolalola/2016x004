<?php

/*
 * The MIT License
 *
 * Copyright 2014 Matija Novak <matija.novak@foi.hr>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * 
 * Klasa za upravljanje sa sesijama
 *
 * @author Matija Novak <matija.novak@foi.hr>
 */

class Sesija {

    const KORISNIK = "korisnik";
    const KOSARICA = "kosarica";
    const SESSION_NAME = "ime_sesije";
    const TIP = "tip";
    const GRESKE = "greske";
    const IME_KORISNIKA = "ime_korisnika";
    const PREZIME_KORISNIKA = "prezime_korisnika";
    const MAIL_KORISNIKA = "mail_korisnika";
    const BROJ_INDEKSA = "broj_indeksa";
    const ID_KORISNIKA = "id_korisnika";
    const KUPON = "kuponi";
    const BODOVI = "bodovi";
    const KATEGORIJE = "kategorije";

        
        static function kreirajSesiju() {
        session_name(self::SESSION_NAME);

        if (session_id() == "") {
            session_start();
        }
    }

    static function kreirajKorisnika($korisnik, $tip, $greske, $ime_korisnika, $prezime_korisnika, $mail_korisnika, $broj_indeksa, $id_korisnika) {
        self::kreirajSesiju();
        $_SESSION[self::KORISNIK] = $korisnik;
        $_SESSION[self::TIP] = $tip;
        $_SESSION[self::GRESKE] = $greske;
        $_SESSION[self::IME_KORISNIKA] = $ime_korisnika;
        $_SESSION[self::PREZIME_KORISNIKA] = $prezime_korisnika;
        $_SESSION[self::MAIL_KORISNIKA] = $mail_korisnika;
        $_SESSION[self::BROJ_INDEKSA] = $broj_indeksa;
        $_SESSION[self::ID_KORISNIKA] = $id_korisnika;
    }

    static function kreirajKosaricu($kosarica, $kuponi) {
        self::kreirajSesiju();
        $_SESSION[self::KOSARICA] = $kosarica;
        $_SESSION[self::KUPON] = $kuponi;
        
    }
    static function kreirajModeratora($kategorije) {
        self::kreirajSesiju();
        $_SESSION[self::KATEGORIJE] = $kategorije;
        
    }

    static function dajKorisnika() {
        self::kreirajSesiju();
        if (isset($_SESSION[self::KORISNIK])) {
            $korisnik = $_SESSION[self::KORISNIK];
        } else {
            return null;
        }
        return $korisnik;
    }

    static function dajKosaricu() {
        self::kreirajSesiju();
        if (isset($_SESSION[self::KOSARICA])) {
            $kosarica = $_SESSION[self::KOSARICA];
        } else {
            return null;
        }
        return $kosarica;
    }

    /**
     * Odjavljuje korisnika tj. briše sesiju
     */
    static function obrisiSesiju() {
        session_name(self::SESSION_NAME);

        if (session_id() != "") {
            session_unset();
            session_destroy();
            

            }
            
            
    }

}
