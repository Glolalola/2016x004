
<!DOCTYPE html>
<html>
   <head>
        <title>{$naslov}</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="naslov" content="{$naslov}" />
        <meta name="kljucne_rijeci" content="projekt" />
        <meta name="datum_izrade" content="31.05.2017." />
        <meta name="autor" content="Gloria Babić" />
        
        <!--Datoteke za straničenje -->
        
        <!-- jQuery lib -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
		
		<!-- datatable lib -->
		<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/s/dt/jq-2.1.4,dt-1.10.10/datatables.min.css"/>
		<script type="text/javascript" src="https://cdn.datatables.net/s/dt/jq-2.1.4,dt-1.10.10/datatables.min.js"></script>
		
		<!-- jqueryUI -->
		<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
		
		
		<!-- korisnički JS sa jQuery -->
        
                
                <script type="text/javascript" src="http://barka.foi.hr/WebDiP/2016_projekti/WebDiP2016x004/js/globabic_jquery.js" ></script>


        <link rel="stylesheet" type="text/css" href="css/globabic.css" />
        <!--Kod za recaptcha preuzet sa: https://www.google.com/recaptcha -->
        <script src='https://www.google.com/recaptcha/api.js'></script>
        
        <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.css" />
      <!--<script src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.js"></script>-->
    </head>
   <body>
        <header>
            
            <p style="margin: 0px;">Wellness centar Gloria</p>
            
        </header
        
             