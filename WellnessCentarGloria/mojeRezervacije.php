<?php
include("baza.class.php");
include("sesija.class.php");

Sesija::kreirajSesiju();

if ($_SESSION["tip"] != 1 && $_SESSION["tip"] != 2 && $_SESSION["tip"] != 3) {
    echo "Preusmjeravanje...";
    header("Location: index.php");
}

$korisnik = $_SESSION["id_korisnika"];
$baza = new Baza();
$baza->spojiDB();

$datum = date("Y-m-d H:i:s");

$upit = "INSERT INTO dnevnik_rada VALUES(DEFAULT, '$korisnik', '$datum', 'Ulazak u mojeRezervacije.php' )";
$baza->selectDB($upit);

$baza->zatvoriDB();


require 'vanjske_biblioteke/Smarty/libs/Smarty.class.php';
$smarty = new Smarty;
$smarty->assign("naslov", "Moje rezervacije");
$smarty->display('predlosci/_header.tpl');

include 'navigacija.php';
$baza = new Baza();
$baza->spojiDB();

$korisnik = $_SESSION["id_korisnika"];
$datum = date("Y-m-d H:i:s");
$sql = "INSERT INTO bodovi VALUES(8, '$korisnik', '$datum',5)";
//echo "<br>" . $sql;
$uspjesno = $baza->selectDB($sql);
if ($baza->pogreskaDB()) {
    echo "Problem kod upita na bazu podataka!";
    exit;
}

function rezervacije() {
    $baza = new Baza();
    $baza->spojiDB();
    $korisnik = $_SESSION["id_korisnika"];

    $sql = "SELECT naziv, datum, vrijeme, potvrda, korisnik_idkorisnik, idusluga , idrezervacije, noviTermin FROM usluga, rezervacije WHERE korisnik_idkorisnik='$korisnik' AND usluga_idusluga=idusluga ";
    $rezultat = $baza->selectDB($sql);

    if ($baza->pogreskaDB()) {
        echo "Problem kod upita na bazu podataka!";
        exit;
    }

    while ($polje = mysqli_fetch_array($rezultat)) {
        if ($polje["potvrda"] == 1 || (($polje["potvrda"] == 0) && ($polje["noviTermin"] == 1))) {
            echo "<tr><td>" . $polje["naziv"] . "</td><td>" . $polje["datum"] . "</td><td>" . $polje["vrijeme"] . "</td><td>" . $polje["potvrda"] . "</td>";
        }
        /* if($polje["potvrda"] == 1  &&($polje["noviTermin"] == 0)){
          echo '<td></td>';
          } */
        if (($polje["potvrda"] == 0) && ($polje["noviTermin"] == 1)) {
            echo'<td><a href="mojeRezervacije.php?rezervacija=' . $polje["idrezervacije"] . '">Potvrdi</a></td>';
        } elseif ($polje["potvrda"] == 1) {
            echo '<td></td>';
        }
        echo"</tr>";
    }
    if (isset($_GET['rezervacija'])) {
        $rezervacija = $_GET['rezervacija'];
        $sql = "UPDATE rezervacije SET potvrda = 1 WHERE idrezervacije = '{$rezervacija}'";
        $baza->selectDB($sql);
        if ($baza->pogreskaDB()) {
            echo "Problem kod upita na bazu podataka!";
            exit;
        }
        echo "<meta http-equiv=\"refresh\" content=\"0;URL=mojeRezervacije.php\">"; 
    }
    $baza->zatvoriDB();
}
?>
    <div class="tablica" style="margin: 10px; margin-top: 20px;">
        <table id="tablica" border="none" class="display"style="margin: 10px; margin-top: 20px;">
            <thead>
                <tr>
                    <th>Usluga</th>
                    <th>Datum</th>
                    <th>Vrijeme</th>
                    <th>Potvrđeno</th>
                    <th>Predloženi termin</th>

            </thead>
            <tbody>
<?php rezervacije(); ?>
        </table>
        
    </div>
<?php
$smarty2 = new Smarty;
$smarty2->display('predlosci/_footer.tpl');
?>

