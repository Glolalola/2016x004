
<?php
include ("sesija.class.php");
include ("baza.class.php");
require 'vanjske_biblioteke/Smarty/libs/Smarty.class.php';


Sesija::kreirajSesiju();
if ($_SESSION["tip"] != 1 && $_SESSION["tip"] != 2 && $_SESSION["tip"] != 3) {
    echo "Preusmjeravanje...";
    header("Location: index.php");
}
$korisnik = $_SESSION["id_korisnika"];
$baza = new Baza();
$baza->spojiDB();

$datum = date("Y-m-d H:i:s");

$upit = "INSERT INTO dnevnik_rada VALUES(DEFAULT, '$korisnik', '$datum', 'Ulazak u rezervacija.php.' )";
$baza->selectDB($upit);

$baza->zatvoriDB();


$smarty = new Smarty;
$smarty->assign("naslov", "Rezervacija");
//$smarty->assign("tip", $tipP);
$smarty->display('predlosci/_header.tpl');

include 'navigacija.php';

$baza = new Baza();
$baza->spojiDB();

if (isset($_GET['usluga'])) {
    $usluga = $_GET['usluga'];

    $sql = "SELECT * FROM usluga WHERE idusluga='{$usluga}'";
    $rezultat = $baza->selectDB($sql);

    if ($baza->pogreskaDB()) {
        echo "Problem kod upita na bazu podataka!";
        exit;
    }
    $polje = mysqli_fetch_array($rezultat);
    
    echo "<h2>Opis usluge: <br>" . $polje["naziv"] . "<br></h2>";
    echo '<div style="display:inline-block;">';
    echo "<p>Opis usluge: <br>" . $polje["opis"] . "<br></p>";
    echo "<p>Vrijeme trajanja usluge: <br>" . $polje["vrijeme_trajanja"] . "sata<br></p>";
    echo "<p>Cijena usluge: <br>" . $polje["cijena"] . "kn<br></p>";
    
    $trajajeUsluge = $polje["vrijeme_trajanja"];
}

$baza->zatvoriDB();
?>
<form id="rezervacija" name="rezervacija" novalidate method="post">

    <label for="datumRezervacije"  id="datumRezervacijeLabel" >Datum rezervacije </label>
    <input id="datumRezervacije" type="date" name="datumRezervacije" ><br>

    <label for="vrijemeRezervacije" id="vrijemeRezervacijeLabel" >Vrijeme rezervacije: </label>
    <input id="vrijemeRezervacije" type="time" name="vrijemeRezervacije" ><br>

    <input id="rezerviraj" type="submit" style="margin: 10px;"name="rezerviraj" value="Rezerviraj"><br>

</form>


<?php
$greska = "";

if (!empty($_POST["rezerviraj"])) {
    $baza->spojiDB();
    //echo $_POST["datumRezervacije"];
    $uneseniDatum = $_POST["datumRezervacije"];

    $danas = date("Y-m-d H:i:s");
    //echo $danas;

    if ($uneseniDatum < $danas) {
        $greska .= "Datum ne može biti u prošlosti!";
        $ispravno = false;
    }

    //provjera ima li korisnik još neku uslugu tada rezerviranu
    $korisnik = $_SESSION["id_korisnika"];
    //echo  "KOrisnik: " .$_SESSION["id_korisnika"];
    $unesenoVrijeme = $_POST["vrijemeRezervacije"];
    //echo "Uneseno vrijeme".$unesenoVrijeme;


    $upit = "SELECT datum, vrijeme, vrijeme_trajanja, potvrda FROM rezervacije, usluga WHERE korisnik_idkorisnik = '$korisnik' AND usluga_idusluga = idusluga AND potvrda = 1";
    //echo $upit;

    if ($baza->pogreskaDB()) {
        $greska .= "Problem kod upita na bazu podataka!<br>";
        exit;
    }
    $rezultat = $baza->selectDB($upit);
    $zauzeto = false;
    while ($res = mysqli_fetch_array($rezultat)) {
        //echo $res["datum"];
        if ($res["datum"] == $uneseniDatum) {
            $prvaProvjera = $unesenoVrijeme > $res["vrijeme"];
            $drugaProvjera = $unesenoVrijeme < ($res["vrijeme"] + date('H', $res["vrijeme_trajanja"]));
            $trecaProvjera = ($unesenoVrijeme + $trajajeUsluge) > $res["vrijeme"];
            $cetvrtaProvjera = $unesenoVrijeme < $res["vrijeme"];
            /* echo '<br>SVe'. ($prvaProvjera && $drugaProvjera && $trecaProvjera);
              echo '<br>Prva'. $prvaProvjera;
              echo '<br>Druga'. $drugaProvjera;
              echo '<br>Treca'. $trecaProvjera; */
            //echo '<br>Uneseni datum:'.$uneseniDatum.'uneseno vrijeme'. $unesenoVrijeme.'<br>';
            if (($prvaProvjera && $drugaProvjera) || ($trecaProvjera && $cetvrtaProvjera)) {
                $greska .= "Već imate rezervaciju u to vrijeme!";
                $zauzeto = true;
                
            } 
            }
    }
    $upit = "SELECT datum, vrijeme, vrijeme_trajanja, potvrda FROM rezervacije, usluga WHERE usluga_idusluga = '$usluga' AND usluga_idusluga = idusluga AND potvrda = 1";
    //echo $upit;

    if ($baza->pogreskaDB()) {
        $greska .= "Problem kod upita na bazu podataka!<br>";
        exit;
    }
    $rezultat = $baza->selectDB($upit);
    while ($res = mysqli_fetch_array($rezultat)) {
        if ($res["datum"] == $uneseniDatum) {
            $prvaProvjera = $unesenoVrijeme > $res["vrijeme"];
            $drugaProvjera = $unesenoVrijeme < ($res["vrijeme"] + date('H', $res["vrijeme_trajanja"]));
            $trecaProvjera = ($unesenoVrijeme + $trajajeUsluge) > $res["vrijeme"];
            $cetvrtaProvjera = $unesenoVrijeme < $res["vrijeme"];
            
            if (($prvaProvjera && $drugaProvjera) || ($trecaProvjera && $cetvrtaProvjera)) {
                $greska .= "Taj termin je već rezerviran!";
                $zauzeto = true;
                
            } 
            }
    }
    
    
    
    if(!$zauzeto){
        $sql = "INSERT INTO rezervacije VALUES(DEFAULT, '$uneseniDatum', '$unesenoVrijeme', '$korisnik', '$usluga',  0, 0)";
                //echo "upit za rezervaciju<br>".$sql;
                $uspjesno = $baza->selectDB($sql);
                if ($baza->pogreskaDB()) {
                    echo "Problem kod upita na bazu podataka!";
                    exit;
                }
                
        $datum =date("Y-m-d H:i:s");
        $sql = "INSERT INTO bodovi VALUES(10, '$korisnik', '$datum',5)";
                //echo "bodovi za rezervaciju<br>".$sql;
                $uspjesno = $baza->selectDB($sql);
                if ($baza->pogreskaDB()) {
                    echo "Problem kod upita na bazu podataka!";
                    exit;
                }        
        
    }
    $baza->zatvoriDB();
}

echo "<br><span class='greska' >" . $greska . "</span></div>"
?>

<?php
$smarty2 = new Smarty;
$smarty2->display('predlosci/_footer.tpl');
?>



