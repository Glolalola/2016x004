<?php
include("sesija.class.php");
include("baza.class.php");

if (!isset($_COOKIE['aktivacijski_kod'])) {
    echo "Preusmjeravanje...";
    header("Location: index.php");
}

if ($_SERVER['REQUEST_METHOD'] == "POST") {
    if (isset($_POST['unos_koda'])) {
        $aktivacijski_kod = $_POST['unos_koda'];
        $spremljeniKod = $_COOKIE['aktivacijski_kod'];

        if ($aktivacijski_kod == $spremljeniKod) {
            $korisnik = $_COOKIE['korisnik'];
            $sql = "SELECT * FROM korisnik WHERE korisnickoIme = '$korisnik'";
            $baza = new Baza();
            $baza->spojiDB();
            $res = $baza->selectDB($sql);
            $polje = mysqli_fetch_array($res);

            echo 'Uspjesna prijava';
            Sesija::kreirajSesiju();
            Sesija::kreirajKorisnika($polje["korisnickoIme"], $polje["tip_korisnika_idtip_korisnika"], $broj_pokusaja, $polje["ime"], $polje["prezime"], $polje["email"], $polje["broj_indeksa"], $polje["idkorisnika"]);
            if ($_POST['zapamtiMe'] == "da") {
                setcookie("zapamti", $polje["korisnickoIme"]);
            }
            $korisnik = $_SESSION["id_korisnika"];
            $baza = new Baza();
            $baza->spojiDB();

            $datum = date("Y-m-d H:i:s");

            $upit ="INSERT INTO dnevnik_prijava VALUES(DEFAULT, '$datum', 1, '$korisnik' )";
            $baza->selectDB($upit);

            $baza->zatvoriDB();
            header("Location: index.php");
        }
    }
}

require 'vanjske_biblioteke/Smarty/libs/Smarty.class.php';


$smarty = new Smarty;
$smarty->assign("naslov", "Druga prijava");

$smarty->display('predlosci/_header.tpl');

include 'navigacija.php';
?>

<h2>Prijava - aktivacijski kod</h2>

<form id="unos_koda" method="post" novalidate>
    <label for="kod">Unesite aktivacijski kod</label><br>
    <input type="text" name="unos_koda" id="unos_koda" /><br/>
    <br>
    <input type="submit" name="druga_prijava" id="druga_prijava" value="Prijavi se"/><br>
    <br>


</form>
<?php
$smarty2 = new Smarty;
$smarty2->display('predlosci/_footer.tpl');
?>
   
