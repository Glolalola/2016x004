<?php

include("baza.class.php");
include("sesija.class.php");

Sesija::kreirajSesiju();

if ($_SESSION["tip"] != 1 && $_SESSION["tip"] != 2) {
    echo "Preusmjeravanje...";
    header("Location: index.php");
}
$korisnik = $_SESSION["id_korisnika"];
$baza = new Baza();
$baza->spojiDB();

$datum = date("Y-m-d H:i:s");

$upit = "INSERT INTO dnevnik_rada VALUES(DEFAULT, '$korisnik', '$datum', 'Ulazak u kalendar.php' )";
$baza->selectDB($upit);

$baza->zatvoriDB();

require 'vanjske_biblioteke/Smarty/libs/Smarty.class.php';
$smarty = new Smarty;
$smarty->assign("naslov", "Rezervacije");
$smarty->display('predlosci/_header.tpl');

include 'navigacija.php';
$baza = new Baza;
$baza->spojiDB();

echo '<form id="izaberi" name="izaberi" novalidate method="post">
            <label for="datum"  id="datumLabel" >Izaberi datum </label>
            <input id="datum" type="date" name="datum" ><br>
            
            <input id="izaberi" type="submit" style="margin: 10px;"name="izaberi" value="Izaberi"><br>
      </form>';

if (isset($_POST["izaberi"])) {

    $datum = $_POST["datum"];
    $sql = "SELECT datum, vrijeme, usluga_idusluga, naziv, idusluga FROM rezervacije,  usluga WHERE datum='$datum' AND potvrda=1 AND usluga_idusluga=idusluga";
    $rezultat = $baza->selectDB($sql);

    echo '<div class="tablica" style="margin: 10px; margin-top: 20px;">
        <table border="none" class="display"style="margin: 10px; margin-top: 20px;">
        <thead>
                <tr>
                    <th>Usluga</th>
                    <th>Vrijeme</th>
                    
            </thead>
        ';

    while ($polje = mysqli_fetch_array($rezultat)) {

        echo "<tr><td>" . $polje["naziv"] . "</td><td>" . $polje["vrijeme"] . "</td></tr>";
    }
    echo '</table></div>';
}


$baza->zatvoriDB();

$smarty2 = new Smarty;
$smarty2->display('predlosci/_footer.tpl');
?>

