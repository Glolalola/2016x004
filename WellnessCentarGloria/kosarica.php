<?php

include ("sesija.class.php");
include ("baza.class.php");
require 'vanjske_biblioteke/Smarty/libs/Smarty.class.php';

Sesija::kreirajSesiju();
if ($_SESSION["tip"] != 1 && $_SESSION["tip"] != 2 && $_SESSION["tip"] != 3) {
    echo "Preusmjeravanje...";
    header("Location: index.php");
}

$korisnik = $_SESSION["id_korisnika"];
$baza = new Baza();
$baza->spojiDB();

$datum = date("Y-m-d H:i:s");

$upit = "INSERT INTO dnevnik_rada VALUES(DEFAULT, '$korisnik', '$datum', 'Ulazak u kosarica.php' )";
$baza->selectDB($upit);

$baza->zatvoriDB();

$smarty = new Smarty;
$smarty->assign("naslov", "Košarica");
$smarty->display('predlosci/_header.tpl');

include 'navigacija.php';

$korisnik = $_SESSION["id_korisnika"];

$baza = new Baza();
$baza->spojiDB();

$sql = "
SELECT * 
FROM košarica, kupon, akcija_kupona
WHERE korisnik_idkorisnik =" . $korisnik . "
AND kod_kupona = '0'
AND idkupon = kupon_idkupon
AND akcija_kupona_idakcija_kupona = idakcija_kupona";

$rezultat = $baza->selectDB($sql);


if ($baza->pogreskaDB()) {
    echo "Problem kod upita na bazu podataka!";
    exit;
}
$sql2 = "SELECT naziv, datum, korisnik, idakcije_korisnika, ostvareni_bodovi, akcija_korisnika FROM bodovi, akcije_korisnika WHERE korisnik='$korisnik' AND idakcije_korisnika=akcija_korisnika ";
$rezultat2 = $baza->selectDB($sql2);

if ($baza->pogreskaDB()) {
    echo "Problem kod upita na bazu podataka!";
    exit;
}
$bodovi = 0;
while ($polje2 = mysqli_fetch_array($rezultat2)) {

    $bodovi += $polje2["ostvareni_bodovi"];
}


echo ' 
<div id="kategorije" class="kategorije">
    <h1>Vaša košarica</h1>
   
';
$suma = 0;

echo'<div class="tablica" style="margin: 10px; margin-top: 20px;">
        <table id="tablica" border="none" class="display"style="margin: 10px; margin-top: 20px;">
            <thead>
                <tr>
                    <th>Naziv</th>
                    <th>Bodovi</th>
                    <th>Izbaci</th>

            </thead>
            <tbody>';

while ($polje = mysqli_fetch_array($rezultat)) {
    $suma += $polje["bodovi"];
    echo "<tr>
                    <td>" . $polje["naziv"] . "</td>
                    <td>" . $polje["bodovi"] . "</td>
                    <td><a href='kosarica.php?kosarica=" . $polje["idkošarica"] . "'>Izbaci</a></td>
        </tr>";
}
echo' 
</table>
<form method="POST" action="kosarica.php">
    <button type="submit" name="kupi" value="kupi" '; if (intval($suma) > intval($bodovi)){     echo 'disabled';}echo '>Kupi</button></form>
    
</div>';


echo '<h3>Ukupno: ' . $suma . ' bodova</h3><h3';
if (intval($suma) > intval($bodovi)){
    echo 'style="color:red;"';
} echo '>  Vaši bodovi: ' . $bodovi . '</h3>
</div>';
if (isset($_POST["kupi"])) {
    $datum = date("Y-m-d H:i:s");
    $minusSuma = -$suma;
    //echo 'minus:'.$minusSuma;
    $sql = "INSERT INTO bodovi VALUES(2, $korisnik, '$datum', $minusSuma)";
    echo "<meta http-equiv=\"refresh\" content=\"0;URL=kosarica.php\">"; 

    $baza->selectDB($sql);

    if ($baza->pogreskaDB()) {
        echo "Problem kod upita na bazu podataka!";
        exit;
    }
    $sql3 = "
SELECT * 
FROM košarica, kupon, akcija_kupona
WHERE korisnik_idkorisnik =" . $korisnik . "
AND kod_kupona = '0'
AND idkupon = kupon_idkupon
AND akcija_kupona_idakcija_kupona = idakcija_kupona";

$rezultat3 = $baza->selectDB($sql3);
$kodoviKupona = array();
    while ($polje3 = mysqli_fetch_array($rezultat3)) {
        $kod = rand(1000, 100000);
        $kosarica =  $polje3["idkošarica"];
        $sql = "UPDATE košarica SET kod_kupona =  $kod WHERE idkošarica = $kosarica";
        array_push($kodoviKupona, $kod);
        $baza->selectDB($sql);
        if ($baza->pogreskaDB()) {
            echo "Problem kod upita na bazu podataka!";
            exit;
        }
}

Sesija::kreirajKosaricu($korisnik, $kodoviKupona);

    
}
if(isset($_SESSION["kosarica"])){
    echo "<a href='printKodova.php'>Ispiši</a>";
}

if (isset($_POST['kosarica'])) {
    $kosarica = intval($_POST['kosarica']);
        $sql = "UPDATE košarica SET kod_kupona =  'izbrisan' WHERE idkošarica = $kosarica";
        //echo $sql;
        $baza->selectDB($sql);
        if ($baza->pogreskaDB()) {
            echo "Problem kod upita na bazu podataka!";
            exit;
        }
        //echo "<meta http-equiv=\"refresh\" content=\"0;URL=kosarica.php\">";    
        
        
}


$baza->zatvoriDB();
$smarty2 = new Smarty;
$smarty2->display('predlosci/_footer.tpl');
?>
