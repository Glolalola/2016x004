<?php
include ("sesija.class.php");
include ("baza.class.php");
require 'vanjske_biblioteke/Smarty/libs/Smarty.class.php';
$kat = array();
$greska = "";

Sesija::kreirajSesiju();
if ($_SESSION["tip"] != 1 && $_SESSION["tip"] != 2) {
    echo "Preusmjeravanje...";
    header("Location: index.php");
}
$korisnik = $_SESSION["id_korisnika"];
$baza = new Baza();
$baza->spojiDB();

$datum = date("Y-m-d H:i:s");

$upit = "INSERT INTO dnevnik_rada VALUES(DEFAULT, '$korisnik', '$datum', 'Ulazak u definirajUsluge.php' )";
$baza->selectDB($upit);

$baza->zatvoriDB();

$smarty = new Smarty;
$smarty->assign("naslov", "Definiraj uslugu");
//$smarty->assign("tip", $tipP);
$smarty->display('predlosci/_header.tpl');

include 'navigacija.php';

function popisKategorija() {
    $baza = new Baza();
    $baza->spojiDB();
    if (isset($_SESSION["kategorije"])) {


        $kategorije = $_SESSION["kategorije"];

        $sql = "SELECT * FROM kategorija_usluga";

        $rezultat = $baza->selectDB($sql);

        if ($baza->pogreskaDB()) {
            echo "Problem kod upita na bazu podataka!";
            exit;
        }
        $idkategorije = array();
        $nazivkategorije = array();
        while ($polje = mysqli_fetch_array($rezultat)) {
            if (in_array($polje["idkategorija"], $kategorije)) {
                echo "<option>" . $polje["naziv"] . "</option>";
                array_push($idkategorije, $polje["idkategorija"]);
                array_push($nazivkategorije, $polje["naziv"]);
            }
        }
        global $kat;
        $kat = array_combine($nazivkategorije, $idkategorije);

        echo '</div>';
    }
    $baza->zatvoriDB();
}
?>
<form id="definiranjeUsluge" name="definiranjeUsluge"  method="post" class="def">

    <div style="display: inline-block">
        <label for="nazivUsluge"  id="nazivUslugeLabel" >Naziv usluge: </label>
        <input id="nazivUsluge" type="text" name="nazivUsluge" ><br>

        <label for="kategorijaUsluge"  id="kategorijaUslugeLabel" >Kategorija usluge: </label>
        <select style="float: right; margin: 5px;" id="kategorijaUsluge" name="kategorijaUsluge"><?php popisKategorija() ?></select><br>

        <label for="vrijemeUsluge" id="vrijemeUslugeLabel" >Vrijeme trajanja usluge: </label>
        <input id="vrijemeUsluge" type="number" name="vrijemeUsluge" min="1" step="any"><br>

        <label for="opisUsluge" id="opisUslugeLabel">Opis usluge: </label>
        <textarea rows="15" cols="30"  style="float: right; margin: 5px;"  id="opisUsluge" name="opisUsluge" placeholder="Ovdje unesite opis usluge"></textarea><br>

        <label for="cijenaUsluge" id="cijenaUslugeLabel">Cijena usluge: </label>
        <input id="cijenaUsluge" name="cijenaUsluge" type="number" min="1" step="any" /><br>
    </div>
    <div style="display: block">
        <button style="float: right; margin: 5px; display: block;"  type="submit" name="dodaj" value="Dodaj">Dodaj uslugu</button>
    </div>
</form>

<?php
if (!empty($_POST["dodaj"])) {
    $baza = new Baza;
    $baza->spojiDB();
    $ispravno = true;
    $nijePopunjeno = false;
    foreach ($_POST as $key => $value) {
        if (empty($_POST[$key])) {
            global $greska;
            $greska .= "Nisu popunjena sva polja! <br>";
            $popunjenaPolja = true;
            $ispravno = false;
            $nijePopunjeno = true;
        }
    }
    $uneseniNaziv = $_POST["nazivUsluge"];
    $unesenaKategorija = $kat[$_POST["kategorijaUsluge"]];
    $unesenoVrijeme = $_POST["vrijemeUsluge"];
    $uneseniOpis = mysqli_real_escape_string($baza->spojiDB(), $_POST["opisUsluge"]);
    $unesenaCijena = $_POST["cijenaUsluge"];
    
    

    $sql = "INSERT INTO usluga VALUES(DEFAULT, '$uneseniNaziv', '$unesenaKategorija', '$unesenoVrijeme', '$uneseniOpis', '$unesenaCijena' , 0)";
    //echo "upit za rezervaciju<br>".$sql;
    $uspjesno = $baza->selectDB($sql);
    if ($baza->pogreskaDB()) {
        echo "Problem kod upita na bazu podataka!";
        exit;
    }
}
 echo'<br><span class="greska" >' . $greska . '</span>" ';

$smarty2 = new Smarty;
$smarty2->display('predlosci/_footer.tpl');
?>



