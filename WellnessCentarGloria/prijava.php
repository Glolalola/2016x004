<?php
$request_uri = $_SERVER["REQUEST_URI"];
$uri = strrpos($request_uri, "/");
$dir = $_SERVER["SERVER_NAME"] . substr($request_uri, 0, $uri + 1);
if (!isset($_SERVER["HTTPS"]) || strtolower($_SERVER["HTTPS"]) != "on") {
    $adresa = 'https://' . $dir . 'prijava.php';
    header("Location: $adresa");
    exit();
}
$naziv = "Brojac";

require ("baza.class.php");
require ("sesija.class.php");


if (isset($_POST["submit"])) {
    $baza = new Baza();
    $baza->spojiDB();
    $korIme = mysqli_real_escape_string($baza->spojiDB(), $_POST["korime"]);
    $lozinka = mysqli_real_escape_string($baza->spojiDB(), $_POST["lozinka"]);

    $salt = 'SveSuZeneKurve';
    $kriptlozinka = sha1($salt) . $salt;

    $sql = "SELECT * FROM korisnik WHERE (korisnickoIme = '$korIme' AND lozinka = '$lozinka')";
    $res = $baza->updateDB($sql);
    if ($baza->pogreskaDB()) {
        $greska = "Problem kod upita na bazu podataka!";
        exit;
    }
    $polje = mysqli_fetch_array($res);
    if ($polje["korisnickoIme"] == $korIme && $polje["lozinka"] == $lozinka) {
        if (($polje["broj_pokusaja"] >= '3') || $polje["blokiran"] == 1) {
            header("Location: blokiran.php");
        } else {
            echo 'Uspjesna prijava';
            Sesija::kreirajSesiju();
            Sesija::kreirajKorisnika($polje["korisnickoIme"], $polje["tip_korisnika_idtip_korisnika"], $broj_pokusaja, $polje["ime"], $polje["prezime"], $polje["email"], $polje["idkorisnika"]);
            
            $korisnik = $_SESSION["id_korisnika"];
            $baza = new Baza();
            $baza->spojiDB();

            $datum = date("Y-m-d H:i:s");

            $upit = "INSERT INTO dnevnik_prijava VALUES(DEFAULT, '$datum', 1, '$korisnik' )";
            $baza->selectDB($upit);

            $baza->zatvoriDB();
            header("Location: index.php");
        }
    } else {
        $update = "UPDATE korisnik SET broj_pokusaja = broj_pokusaja + 1 WHERE korisnickoIme = '$korIme'";
        $baza->updateDB($update);

        if ($baza->pogreskaDB()) {
            echo "Problem kod upita na bazu podataka!";
            exit;
        }
        header("Location: krivaPrijava.php?korisnik=" . $korIme);
    }
}
if (isset($_SESSION["tip"])) {
    $tipP = $_SESSION["tip"];
} else {
    $tipP = '4';
}
include 'navigacija.php';
?>

<form id="prijava" name="prijava" method="POST"
      action="prijava.php">
    <div id="gornji_dio">

        <label id="lkorime" for="ikorime">Korisničko ime: </label>
        <input id="ikorime" type="text" name="korime" value="<?php print($korisnickoIme); ?>"><br>

        <label id="llozinka" for="ilozinka">Lozinka: </label>
        <input id="ilozinka" type="password" name="lozinka"><br>
    </div>
    <input id="iposalji" name="submit" type="submit" value="Pošalji">


</form>
<p>
    Ovdje se <a href="registracija.php">registriraj</a><br>
    <a href="zaboravljena.php">Zaboravljena lozinka</a>
</p>



