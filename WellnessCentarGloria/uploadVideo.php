<?php
include("baza.class.php");
include("sesija.class.php");

Sesija::kreirajSesiju();
if ($_SESSION["tip"] != 1) {
    echo "Preusmjeravanje...";
    header("Location: index.php");
}

$korisnik = $_SESSION["id_korisnika"];
$baza = new Baza();
$baza->spojiDB();

$datum = date("Y-m-d H:i:s");

$upit = "INSERT INTO dnevnik_rada VALUES(DEFAULT, '$korisnik', '$datum', 'Ulazak u uploadVideo.php.' )";
$baza->selectDB($upit);

$baza->zatvoriDB();
require 'vanjske_biblioteke/Smarty/libs/Smarty.class.php';
$smarty = new Smarty;
$smarty->assign("naslov", "Upload video");
$smarty->display('predlosci/_header.tpl');


include 'navigacija.php';
?>

<form id="dodajVideo" name="dodajVideo"  method="post" class="def">

    <div style="display: inline-block">
        <label for="nazivVidea"  id="nazivVideaLabel" >Naziv videa: </label>
        <input id="nazivVidea" type="text" name="nazivVidea" ><br>
        
        <label for="path"  id="pathLabel" >Putanja videa: </label>
        <input id="path" type="text" name="path" ><br>
    </div>
    <div style="display: block">
        <button style="float: right; margin: 5px; display: block;"  type="submit" name="dodajVideo" value="dodajVideo">Dodaj video</button>
    </div>
</form>



<?php
$greska = "";
if (!empty($_POST["dodajVideo"])) {
    $baza = new Baza;
    $baza->spojiDB();
    $ispravno = true;
    $nijePopunjeno = false;
    foreach ($_POST as $key => $value) {
        if (empty($_POST[$key])) {
            global $greska;
            $greska .= "Nisu popunjena sva polja! <br>";
            $popunjenaPolja = true;
            $ispravno = false;
            $nijePopunjeno = true;
        }
    }
    $uneseniNaziv = $_POST["nazivVidea"];
    $unesenaPutanja = $_POST["path"];
    $datum = date("Y-m-d H:i:s");
    
    if($ispravno){
        $sql = "INSERT INTO video VALUES(DEFAULT, '$uneseniNaziv', '$unesenaPutanja', '$datum')";
        

        echo 'SQL = ' . $sql;
        $baza->selectDB($sql);
    }
}
echo'<br><span class="greska" >' . $greska . '</span>" ';

$smarty2 = new Smarty;
$smarty2->display('predlosci/_footer.tpl');
?>
