<?php
include("../baza.class.php");

function izlistaj_korisnike() {
    $baza = new Baza();
    $baza->spojiDB();

    $sql = "SELECT korisnickoIme, lozinka, tip_korisnika_idtip_korisnika FROM korisnik ";
    $rezultat = $baza->selectDB($sql);

    if ($baza->pogreskaDB()) {
        echo "Problem kod upita na bazu podataka!";
        exit;
    }

    

    while ($polje = mysqli_fetch_array($rezultat)) {
        echo "<tr><td>" . $polje["korisnickoIme"] . "</td><td>" . $polje["lozinka"] . "</td><td>" . $polje["tip_korisnika_idtip_korisnika"] . "</td></tr>";
    }

    $baza->zatvoriDB();
}
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Korisnici</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="naslov" content="{$naslov}" />
        <meta name="kljucne_rijeci" content="projekt" />
        <meta name="datum_izrade" content="31.05.2017." />
        <meta name="autor" content="Gloria Babić" />

        <!--Datoteke za straničenje -->
        
        <!-- jQuery lib -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
		
		<!-- datatable lib -->
		<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/s/dt/jq-2.1.4,dt-1.10.10/datatables.min.css"/>
		<script type="text/javascript" src="https://cdn.datatables.net/s/dt/jq-2.1.4,dt-1.10.10/datatables.min.js"></script>
		
		<!-- jqueryUI -->
		<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
		
		
		<!-- korisnički JS sa jQuery -->
        
                
                <script type="text/javascript" src="../js/globabic_jquery.js" ></script>


        <link rel="stylesheet" type="text/css" href="../css/globabic.css" />
        <!--Kod za recaptcha preuzet sa: https://www.google.com/recaptcha -->
        <script src='https://www.google.com/recaptcha/api.js'></script>


    </head>
    <body>
        <header>

            <p style="margin: 0px;">Wellness centar Gloria</p>

        </header


        <section id="sadrzaj">
            <div class="tablica" style="margin: 10px; margin-top: 20px;">
                <table id="tablica" border="none" >
                    <thead>
                    <tr>
                        <th>Korisničko ime</th>
                        <th>Lozinka</th>
                        <th>Tip</th>
                        
                </thead>
                <tbody class="tablica1">
                    
<?php izlistaj_korisnike() ?>
                </table>        
            </div>

        </section>
    </section> 
    <footer class="podnozje">

        <figure>
            <a href="https://jigsaw.w3.org/css-validator/validator?uri=http://barka.foi.hr/WebDiP/2016_projekti/WebDiP2016x004/css/globabic.css">
                <img src="../slike/CSS3.png" width="100" alt="CSS3" > </a>
            <figcaption>CSS 3 validator</figcaption>
        </figure>
        <p>

        <address>
            Kontakt: <a href="mailto:globabic@foi.hr">Gloria Babić</a>
        </address>

        <p>
            <small>&copy;</small> 2017. G.B.
        </p>


    </footer>
</body>
</html>