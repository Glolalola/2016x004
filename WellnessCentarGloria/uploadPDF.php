<?php
include("baza.class.php");
include("sesija.class.php");

Sesija::kreirajSesiju();
if ($_SESSION["tip"] != 1) {
    echo "Preusmjeravanje...";
    header("Location: index.php");
}
$korisnik = $_SESSION["id_korisnika"];
$baza = new Baza();
$baza->spojiDB();

$datum = date("Y-m-d H:i:s");

$upit = "INSERT INTO dnevnik_rada VALUES(DEFAULT, '$korisnik', '$datum', 'Ulazak u uploadPDF.php.' )";
$baza->selectDB($upit);

$baza->zatvoriDB();
require 'vanjske_biblioteke/Smarty/libs/Smarty.class.php';
$smarty = new Smarty;
$smarty->assign("naslov", "Upload PDF");
$smarty->display('predlosci/_header.tpl');


include 'navigacija.php';
?>

<form enctype="multipart/form-data" action="uploaderPDF.php" method="post">
    <div style="display: inline-block;">
        <input type="hidden" name="MAX_FILE_SIZE" value="2097152" />
        <label for = "pdf">PDF: </label>
        <input type = "file" id = "pdf" name = "pdf" />
    </div>
    <div style="display: inline-block;">
        <input type="submit" value="Pošalji" />
    </div>
</form>




<?php
$smarty2 = new Smarty;
$smarty2->display('predlosci/_footer.tpl');
?>
