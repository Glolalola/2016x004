<?php

include("baza.class.php");
include("sesija.class.php");

Sesija::kreirajSesiju();
if ($_SESSION["tip"] != 1 && $_SESSION["tip"] != 2 && $_SESSION["tip"] != 3) {
    echo "Preusmjeravanje...";
    header("Location: index.php");
}
$korisnik = $_SESSION["id_korisnika"];
$baza = new Baza();
$baza->spojiDB();

$datum = date("Y-m-d H:i:s");

$upit = "INSERT INTO dnevnik_rada VALUES(DEFAULT, '$korisnik', '$datum', 'Ulazak u skupljeniBodovi.php.' )";
$baza->selectDB($upit);

$baza->zatvoriDB();

require 'vanjske_biblioteke/Smarty/libs/Smarty.class.php';
$smarty = new Smarty;
$smarty->assign("naslov", "Moji bodovi");
$smarty->display('predlosci/_header.tpl');

include 'navigacija.php';

$suma = 0;
function Bodovi(){
global $suma;
$baza = new Baza();

    $baza->spojiDB();
    $korisnik = $_SESSION["id_korisnika"];

    $sql = "SELECT naziv, datum, korisnik, idakcije_korisnika, ostvareni_bodovi, akcija_korisnika FROM bodovi, akcije_korisnika WHERE korisnik='$korisnik' AND idakcije_korisnika=akcija_korisnika ";
    $rezultat = $baza->selectDB($sql);

    if ($baza->pogreskaDB()) {
        echo "Problem kod upita na bazu podataka!";
        exit;
    }
    while ($polje = mysqli_fetch_array($rezultat)) {
        
        $suma += $polje["ostvareni_bodovi"];
        echo "<tr><td>" . $polje["naziv"] . "</td><td>" . $polje["datum"] . "</td><td>" . $polje["ostvareni_bodovi"] . "</td></tr>";
    }
    
    
}
    
    ?>
    <div class="tablica" style="margin: 10px; margin-top: 20px;">
        <table id="tablica" border="none" class="display"style="margin: 10px; margin-top: 20px;">
            <thead>
                <tr>
                    <th>Akcija</th>
                    <th>Datum</th>
                    <th>Ostvareni bodovi</th>

            </thead>
            <tbody>
                <?php Bodovi() ?>
        </table>
        <h2> <?php echo 'Vaš ukupni broj bodova je: '.$suma;?></h2>
    </div>
<?php
$smarty2 = new Smarty;
$smarty2->display('predlosci/_footer.tpl');
?>