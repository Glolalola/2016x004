-- phpMyAdmin SQL Dump
-- version 3.4.11.1deb2+deb7u8
-- http://www.phpmyadmin.net
--
-- Računalo: localhost
-- Vrijeme generiranja: Lip 08, 2017 u 11:04 AM
-- Verzija poslužitelja: 5.5.55
-- PHP verzija: 5.4.45-0+deb7u8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Baza podataka: `WebDiP2016x004`
--

-- --------------------------------------------------------

--
-- Tablična struktura za tablicu `akcija_kupona`
--

CREATE TABLE IF NOT EXISTS `akcija_kupona` (
  `idakcija_kupona` int(11) NOT NULL AUTO_INCREMENT,
  `naziv` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `slika` int(11) NOT NULL,
  `video` int(11) NOT NULL,
  `pdf_idpdf` int(11) NOT NULL,
  PRIMARY KEY (`idakcija_kupona`),
  KEY `fk_akcija_kupona_slika1_idx` (`slika`),
  KEY `fk_akcija_kupona_video1_idx` (`video`),
  KEY `fk_akcija_kupona_pdf1_idx` (`pdf_idpdf`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=13 ;

--
-- Izbacivanje podataka za tablicu `akcija_kupona`
--

INSERT INTO `akcija_kupona` (`idakcija_kupona`, `naziv`, `slika`, `video`, `pdf_idpdf`) VALUES
(1, 'Akcija na kupku', 3, 4, 4),
(2, 'Akcija na frizuru', 3, 5, 5),
(3, 'Akcija na manikuru', 4, 5, 5),
(4, 'Akcija na bazene', 6, 5, 5),
(5, 'Akcija na masaže', 5, 5, 8),
(6, 'Akcija na bazene', 7, 5, 6),
(7, 'Akcija na masaže', 6, 8, 7),
(8, 'Akcija na bazene', 6, 7, 7),
(9, 'Akcija na masaže', 5, 7, 8),
(10, 'Akcija na bazene', 5, 7, 6),
(11, 'Akcija 10%', 4, 4, 8),
(12, 'Akcija 15%', 5, 7, 8);

-- --------------------------------------------------------

--
-- Tablična struktura za tablicu `akcije_korisnika`
--

CREATE TABLE IF NOT EXISTS `akcije_korisnika` (
  `idakcije_korisnika` int(11) NOT NULL AUTO_INCREMENT,
  `naziv` varchar(45) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`idakcije_korisnika`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Izbacivanje podataka za tablicu `akcije_korisnika`
--

INSERT INTO `akcije_korisnika` (`idakcije_korisnika`, `naziv`) VALUES
(1, 'Klik na sliku kupona'),
(2, 'Kupnja kupona'),
(3, 'Klik na korisnički račun'),
(4, 'Rezervacija'),
(5, 'Klik na video'),
(6, 'Dodan kupon u košaricu'),
(7, 'Sadržaj podijeljen na tumblr'),
(8, 'Klik na popis rezervacija'),
(9, 'Mijenjanje korisničkih postavki'),
(10, 'Rezervacija usluge');

-- --------------------------------------------------------

--
-- Tablična struktura za tablicu `bodovi`
--

CREATE TABLE IF NOT EXISTS `bodovi` (
  `akcija_korisnika` int(11) NOT NULL,
  `korisnik` int(11) NOT NULL,
  `datum` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ostvareni_bodovi` int(11) DEFAULT NULL,
  PRIMARY KEY (`akcija_korisnika`,`korisnik`,`datum`),
  KEY `fk_akcije_korisnika_has_korisnik_korisnik1_idx` (`korisnik`),
  KEY `fk_akcije_korisnika_has_korisnik_akcije_korisnika1_idx` (`akcija_korisnika`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Izbacivanje podataka za tablicu `bodovi`
--

INSERT INTO `bodovi` (`akcija_korisnika`, `korisnik`, `datum`, `ostvareni_bodovi`) VALUES
(1, 5, '2017-06-07 12:00:20', 5),
(1, 10, '2017-03-07 23:00:00', 5),
(1, 46, '2017-06-04 18:25:49', 5),
(1, 46, '2017-06-04 18:26:09', 5),
(1, 46, '2017-06-04 18:26:13', 5),
(1, 46, '2017-06-04 18:26:31', 5),
(1, 46, '2017-06-04 18:26:34', 5),
(1, 46, '2017-06-04 18:26:53', 5),
(1, 46, '2017-06-04 18:27:03', 5),
(1, 46, '2017-06-04 18:27:18', 5),
(1, 46, '2017-06-04 18:27:20', 5),
(1, 46, '2017-06-04 18:27:37', 5),
(1, 46, '2017-06-04 18:27:39', 5),
(1, 46, '2017-06-04 18:27:49', 5),
(1, 46, '2017-06-04 18:27:53', 5),
(1, 46, '2017-06-04 18:28:07', 5),
(1, 46, '2017-06-04 18:28:09', 5),
(1, 52, '2017-06-04 11:28:47', 5),
(1, 52, '2017-06-04 11:43:12', 5),
(1, 52, '2017-06-04 11:44:47', 5),
(1, 52, '2017-06-04 11:45:59', 5),
(1, 52, '2017-06-04 11:50:01', 5),
(1, 52, '2017-06-04 11:50:29', 5),
(1, 52, '2017-06-04 11:50:33', 5),
(1, 52, '2017-06-04 11:51:52', 5),
(1, 52, '2017-06-04 11:51:55', 5),
(1, 52, '2017-06-04 11:53:47', 5),
(1, 52, '2017-06-04 11:53:51', 5),
(1, 52, '2017-06-04 11:55:33', 5),
(1, 52, '2017-06-04 11:55:36', 5),
(1, 52, '2017-06-04 12:05:23', 5),
(1, 52, '2017-06-04 12:05:25', 5),
(1, 52, '2017-06-04 12:20:05', 5),
(1, 52, '2017-06-04 12:20:09', 5),
(1, 52, '2017-06-04 12:33:58', 5),
(1, 52, '2017-06-04 12:34:00', 5),
(1, 52, '2017-06-04 12:34:29', 5),
(1, 52, '2017-06-04 12:34:32', 5),
(1, 52, '2017-06-04 12:37:25', 5),
(1, 52, '2017-06-04 12:37:27', 5),
(1, 52, '2017-06-04 12:51:59', 5),
(1, 52, '2017-06-04 12:52:01', 5),
(1, 52, '2017-06-04 12:53:28', 5),
(1, 52, '2017-06-04 12:53:30', 5),
(1, 52, '2017-06-04 12:54:35', 5),
(1, 52, '2017-06-04 12:54:37', 5),
(1, 52, '2017-06-04 12:54:42', 5),
(1, 52, '2017-06-04 12:54:44', 5),
(1, 52, '2017-06-04 12:56:10', 5),
(1, 52, '2017-06-04 12:56:13', 5),
(1, 52, '2017-06-04 13:03:37', 5),
(1, 52, '2017-06-04 13:03:38', 5),
(1, 52, '2017-06-05 08:29:04', 5),
(1, 52, '2017-06-06 09:33:28', 5),
(1, 52, '2017-06-07 14:20:49', 5),
(1, 52, '2017-06-07 14:29:04', 5),
(1, 52, '2017-06-07 14:29:06', 5),
(1, 52, '2017-06-07 14:29:08', 5),
(1, 52, '2017-06-07 14:29:10', 5),
(1, 52, '2017-06-07 14:29:12', 5),
(1, 52, '2017-06-07 14:29:14', 5),
(1, 52, '2017-06-07 14:29:22', 5),
(1, 52, '2017-06-07 14:29:24', 5),
(1, 52, '2017-06-07 14:29:37', 5),
(1, 52, '2017-06-07 14:29:39', 5),
(1, 52, '2017-06-07 14:29:40', 5),
(1, 52, '2017-06-07 14:29:42', 5),
(1, 52, '2017-06-07 14:29:43', 5),
(1, 52, '2017-06-07 14:29:45', 5),
(1, 52, '2017-06-07 14:29:46', 5),
(1, 53, '2017-06-04 20:05:43', 5),
(1, 53, '2017-06-04 20:06:03', 5),
(1, 53, '2017-06-04 20:06:12', 5),
(1, 53, '2017-06-06 20:16:43', 5),
(1, 53, '2017-06-06 20:17:05', 5),
(1, 53, '2017-06-07 10:05:44', 5),
(2, 46, '2017-06-04 18:28:32', -55),
(2, 52, '2017-06-04 10:17:21', -20),
(2, 52, '2017-06-04 11:55:24', -30),
(2, 52, '2017-06-04 11:56:56', -20),
(2, 52, '2017-06-04 12:05:43', -25),
(2, 52, '2017-06-04 12:20:18', -5),
(2, 52, '2017-06-04 12:28:46', -5),
(2, 52, '2017-06-04 12:29:24', -5),
(2, 52, '2017-06-04 12:32:04', -5),
(2, 52, '2017-06-04 12:33:53', -5),
(2, 52, '2017-06-04 12:34:06', -10),
(2, 52, '2017-06-04 12:34:38', -10),
(2, 52, '2017-06-04 12:37:30', -10),
(2, 52, '2017-06-04 12:52:05', -10),
(2, 52, '2017-06-04 12:53:40', -5),
(2, 52, '2017-06-04 12:54:50', -15),
(2, 52, '2017-06-04 12:56:17', -25),
(2, 52, '2017-06-04 13:03:43', -20),
(2, 52, '2017-06-07 14:29:19', -25),
(2, 52, '2017-06-07 14:29:55', -140),
(2, 53, '2017-06-03 23:34:25', -40),
(2, 53, '2017-06-03 23:39:18', 0),
(2, 53, '2017-06-03 23:56:02', -10),
(2, 53, '2017-06-03 23:57:44', -10),
(2, 53, '2017-06-03 23:58:24', -5),
(2, 53, '2017-06-06 20:17:22', -10),
(3, 3, '2017-03-08 23:00:00', 10),
(3, 5, '2017-03-13 23:00:00', 5),
(4, 4, '2017-03-12 23:00:00', 10),
(5, 5, '2017-03-07 23:00:00', 15),
(6, 3, '2017-03-11 23:00:00', 5),
(6, 7, '2017-03-12 23:00:00', 5),
(6, 9, '2017-03-14 23:00:00', 10),
(6, 46, '2017-06-04 18:26:14', 1),
(6, 46, '2017-06-04 18:26:34', 1),
(6, 46, '2017-06-04 18:27:04', 1),
(6, 46, '2017-06-04 18:27:20', 1),
(6, 46, '2017-06-04 18:27:39', 1),
(6, 46, '2017-06-04 18:27:53', 1),
(6, 46, '2017-06-04 18:28:09', 1),
(6, 52, '2017-06-04 12:05:26', 1),
(6, 52, '2017-06-04 12:20:09', 1),
(6, 52, '2017-06-04 12:34:00', 1),
(6, 52, '2017-06-04 12:34:32', 1),
(6, 52, '2017-06-04 12:37:27', 1),
(6, 52, '2017-06-04 12:52:01', 1),
(6, 52, '2017-06-04 12:53:31', 1),
(6, 52, '2017-06-04 12:54:37', 1),
(6, 52, '2017-06-04 12:54:44', 1),
(6, 52, '2017-06-04 12:56:13', 1),
(6, 52, '2017-06-04 13:03:38', 1),
(6, 52, '2017-06-07 14:29:06', 1),
(6, 52, '2017-06-07 14:29:08', 1),
(6, 52, '2017-06-07 14:29:10', 1),
(6, 52, '2017-06-07 14:29:12', 1),
(6, 52, '2017-06-07 14:29:14', 1),
(6, 52, '2017-06-07 14:29:24', 1),
(6, 52, '2017-06-07 14:29:39', 1),
(6, 52, '2017-06-07 14:29:40', 1),
(6, 52, '2017-06-07 14:29:42', 1),
(6, 52, '2017-06-07 14:29:43', 1),
(6, 52, '2017-06-07 14:29:45', 1),
(6, 52, '2017-06-07 14:29:46', 1),
(6, 53, '2017-06-06 20:17:05', 1),
(7, 3, '2017-03-11 23:00:00', 10),
(7, 10, '2017-03-07 23:00:00', 20),
(8, 2, '2017-06-07 12:09:02', 5),
(8, 2, '2017-06-07 12:09:35', 5),
(8, 2, '2017-06-07 12:10:22', 5),
(8, 2, '2017-06-07 12:10:24', 5),
(8, 2, '2017-06-07 12:10:29', 5),
(8, 2, '2017-06-07 12:10:36', 5),
(8, 2, '2017-06-07 12:11:45', 5),
(8, 2, '2017-06-07 12:11:47', 5),
(8, 2, '2017-06-07 12:11:48', 5),
(8, 2, '2017-06-07 12:11:51', 5),
(8, 5, '2017-06-03 12:37:21', 5),
(8, 5, '2017-06-04 00:00:06', 5),
(8, 5, '2017-06-04 00:00:37', 5),
(8, 5, '2017-06-04 10:18:31', 5),
(8, 5, '2017-06-04 10:19:05', 5),
(8, 5, '2017-06-04 10:19:10', 5),
(8, 5, '2017-06-04 10:22:05', 5),
(8, 5, '2017-06-04 10:22:06', 5),
(8, 52, '2017-06-03 18:38:43', 5),
(8, 52, '2017-06-03 18:38:46', 5),
(8, 52, '2017-06-03 18:38:48', 5),
(8, 52, '2017-06-03 18:39:07', 5),
(8, 52, '2017-06-03 18:39:10', 5),
(8, 52, '2017-06-04 10:17:37', 5),
(8, 52, '2017-06-04 10:18:06', 5),
(8, 52, '2017-06-04 11:01:04', 5),
(8, 52, '2017-06-04 22:17:34', 5),
(8, 52, '2017-06-04 22:26:40', 5),
(8, 52, '2017-06-04 22:27:30', 5),
(8, 52, '2017-06-04 22:27:53', 5),
(8, 52, '2017-06-04 22:29:02', 5),
(8, 52, '2017-06-04 22:54:06', 5),
(8, 52, '2017-06-04 22:54:08', 5),
(8, 52, '2017-06-04 22:59:25', 5),
(8, 52, '2017-06-04 23:06:22', 5),
(8, 52, '2017-06-04 23:09:35', 5),
(8, 52, '2017-06-04 23:10:08', 5),
(8, 52, '2017-06-04 23:10:42', 5),
(8, 52, '2017-06-04 23:23:23', 5),
(8, 52, '2017-06-04 23:23:28', 5),
(8, 52, '2017-06-04 23:23:35', 5),
(8, 52, '2017-06-04 23:23:39', 5),
(8, 52, '2017-06-04 23:23:43', 5),
(8, 52, '2017-06-04 23:24:10', 5),
(8, 52, '2017-06-04 23:24:11', 5),
(8, 52, '2017-06-04 23:24:46', 5),
(8, 52, '2017-06-04 23:24:48', 5),
(8, 52, '2017-06-04 23:24:53', 5),
(8, 52, '2017-06-04 23:24:54', 5),
(8, 52, '2017-06-04 23:24:58', 5),
(8, 52, '2017-06-04 23:25:16', 5),
(8, 52, '2017-06-04 23:25:18', 5),
(8, 52, '2017-06-04 23:25:19', 5),
(8, 52, '2017-06-04 23:25:32', 5),
(8, 52, '2017-06-04 23:25:33', 5),
(8, 52, '2017-06-04 23:25:43', 5),
(8, 52, '2017-06-04 23:25:44', 5),
(8, 52, '2017-06-04 23:26:01', 5),
(8, 52, '2017-06-04 23:26:39', 5),
(8, 52, '2017-06-04 23:26:41', 5),
(8, 52, '2017-06-04 23:26:43', 5),
(8, 52, '2017-06-04 23:26:44', 5),
(8, 52, '2017-06-04 23:26:45', 5),
(8, 52, '2017-06-04 23:26:46', 5),
(8, 52, '2017-06-05 08:14:46', 5),
(8, 52, '2017-06-05 08:15:52', 5),
(8, 52, '2017-06-06 08:32:38', 5),
(8, 52, '2017-06-06 08:33:07', 5),
(8, 52, '2017-06-06 11:20:35', 5),
(8, 52, '2017-06-06 11:25:29', 5),
(8, 52, '2017-06-06 11:25:31', 5),
(8, 52, '2017-06-06 11:25:45', 5),
(8, 52, '2017-06-06 11:27:07', 5),
(8, 52, '2017-06-06 11:28:35', 5),
(8, 52, '2017-06-06 11:28:50', 5),
(8, 52, '2017-06-06 12:01:18', 5),
(8, 52, '2017-06-06 12:04:52', 5),
(8, 52, '2017-06-06 12:06:18', 5),
(8, 52, '2017-06-06 12:07:33', 5),
(8, 52, '2017-06-06 12:07:56', 5),
(8, 52, '2017-06-06 12:07:59', 5),
(8, 52, '2017-06-06 12:11:07', 5),
(8, 52, '2017-06-06 12:14:07', 5),
(8, 52, '2017-06-06 12:14:36', 5),
(8, 52, '2017-06-06 12:14:37', 5),
(8, 52, '2017-06-06 12:14:41', 5),
(8, 52, '2017-06-06 12:14:44', 5),
(8, 52, '2017-06-06 12:14:54', 5),
(8, 52, '2017-06-06 12:14:55', 5),
(8, 52, '2017-06-06 12:16:05', 5),
(8, 52, '2017-06-06 12:17:07', 5),
(8, 52, '2017-06-06 12:17:16', 5),
(8, 52, '2017-06-06 12:17:17', 5),
(8, 52, '2017-06-06 12:17:22', 5),
(8, 52, '2017-06-06 12:18:51', 5),
(8, 52, '2017-06-06 12:19:02', 5),
(8, 52, '2017-06-06 12:19:14', 5),
(8, 52, '2017-06-06 12:19:21', 5),
(8, 52, '2017-06-06 12:19:22', 5),
(8, 52, '2017-06-06 12:19:26', 5),
(8, 52, '2017-06-06 12:21:10', 5),
(8, 52, '2017-06-06 12:21:12', 5),
(8, 52, '2017-06-06 12:21:20', 5),
(8, 52, '2017-06-06 12:22:05', 5),
(8, 52, '2017-06-06 12:22:07', 5),
(8, 52, '2017-06-06 12:22:15', 5),
(8, 52, '2017-06-06 12:22:19', 5),
(8, 52, '2017-06-06 12:22:26', 5),
(8, 52, '2017-06-06 12:22:35', 5),
(8, 52, '2017-06-06 12:22:45', 5),
(8, 52, '2017-06-06 12:22:46', 5),
(8, 52, '2017-06-06 12:22:57', 5),
(8, 52, '2017-06-06 12:23:05', 5),
(8, 52, '2017-06-06 12:23:15', 5),
(8, 52, '2017-06-06 12:23:22', 5),
(8, 52, '2017-06-06 12:23:23', 5),
(8, 52, '2017-06-06 12:23:41', 5),
(8, 52, '2017-06-06 12:24:00', 5),
(8, 52, '2017-06-06 12:24:01', 5),
(8, 52, '2017-06-06 12:24:17', 5),
(8, 52, '2017-06-06 12:24:29', 5),
(8, 52, '2017-06-06 12:24:30', 5),
(8, 52, '2017-06-06 12:25:00', 5),
(8, 52, '2017-06-06 12:25:09', 5),
(8, 52, '2017-06-06 12:25:10', 5),
(8, 52, '2017-06-06 12:25:27', 5),
(8, 52, '2017-06-06 12:25:40', 5),
(8, 52, '2017-06-06 12:25:41', 5),
(8, 52, '2017-06-06 12:26:50', 5),
(8, 52, '2017-06-06 12:26:52', 5),
(8, 52, '2017-06-06 12:26:59', 5),
(8, 52, '2017-06-06 12:27:05', 5),
(8, 52, '2017-06-06 12:30:33', 5),
(8, 52, '2017-06-06 12:30:35', 5),
(8, 52, '2017-06-06 12:30:43', 5),
(8, 52, '2017-06-06 12:30:45', 5),
(8, 52, '2017-06-06 12:30:47', 5),
(8, 52, '2017-06-06 12:30:49', 5),
(8, 52, '2017-06-06 12:31:53', 5),
(8, 52, '2017-06-06 12:32:34', 5),
(8, 52, '2017-06-06 12:32:37', 5),
(8, 52, '2017-06-06 12:32:43', 5),
(8, 52, '2017-06-06 12:32:44', 5),
(8, 52, '2017-06-06 12:33:27', 5),
(8, 52, '2017-06-06 12:33:28', 5),
(8, 52, '2017-06-06 12:33:35', 5),
(8, 52, '2017-06-06 12:34:32', 5),
(8, 52, '2017-06-06 12:34:33', 5),
(8, 52, '2017-06-06 12:34:40', 5),
(8, 52, '2017-06-06 12:35:34', 5),
(8, 52, '2017-06-06 12:35:37', 5),
(8, 52, '2017-06-06 12:35:43', 5),
(8, 52, '2017-06-06 12:35:44', 5),
(8, 52, '2017-06-06 12:36:00', 5),
(8, 52, '2017-06-06 12:40:10', 5),
(8, 52, '2017-06-06 12:40:13', 5),
(8, 52, '2017-06-06 12:40:19', 5),
(8, 52, '2017-06-06 12:40:20', 5),
(8, 52, '2017-06-06 12:42:51', 5),
(8, 52, '2017-06-06 12:42:54', 5),
(8, 52, '2017-06-06 12:43:04', 5),
(8, 52, '2017-06-06 12:43:10', 5),
(8, 52, '2017-06-06 12:44:45', 5),
(8, 52, '2017-06-06 12:44:47', 5),
(8, 52, '2017-06-06 12:44:53', 5),
(8, 52, '2017-06-06 12:44:54', 5),
(8, 52, '2017-06-06 12:50:36', 5),
(8, 52, '2017-06-06 12:50:38', 5),
(8, 52, '2017-06-06 12:50:40', 5),
(8, 52, '2017-06-06 12:50:47', 5),
(8, 52, '2017-06-06 12:52:09', 5),
(8, 52, '2017-06-06 12:53:05', 5),
(8, 52, '2017-06-06 12:53:17', 5),
(8, 52, '2017-06-06 12:53:31', 5),
(8, 52, '2017-06-06 12:53:37', 5),
(8, 52, '2017-06-06 12:54:17', 5),
(8, 52, '2017-06-06 12:54:24', 5),
(8, 52, '2017-06-06 12:54:38', 5),
(8, 52, '2017-06-06 12:55:21', 5),
(8, 52, '2017-06-06 12:55:37', 5),
(8, 52, '2017-06-06 12:55:41', 5),
(8, 52, '2017-06-06 13:03:52', 5),
(8, 52, '2017-06-06 13:04:01', 5),
(8, 52, '2017-06-06 18:56:00', 5),
(8, 52, '2017-06-06 18:56:05', 5),
(8, 52, '2017-06-06 18:57:04', 5),
(8, 52, '2017-06-06 18:57:26', 5),
(8, 52, '2017-06-06 18:57:43', 5),
(8, 52, '2017-06-06 18:57:45', 5),
(8, 52, '2017-06-06 18:57:48', 5),
(8, 52, '2017-06-06 18:58:02', 5),
(8, 52, '2017-06-06 18:58:24', 5),
(8, 52, '2017-06-06 19:16:39', 5),
(8, 52, '2017-06-08 02:18:33', 5),
(8, 52, '2017-06-08 02:18:41', 5),
(8, 52, '2017-06-08 02:18:42', 5),
(8, 52, '2017-06-08 02:19:46', 5),
(8, 53, '2017-06-03 12:41:41', 5),
(8, 53, '2017-06-03 12:41:44', 5),
(8, 53, '2017-06-03 13:56:26', 5),
(8, 53, '2017-06-03 13:56:32', 5),
(8, 53, '2017-06-03 13:56:34', 5),
(8, 53, '2017-06-03 13:56:35', 5),
(8, 53, '2017-06-03 21:00:02', 5),
(8, 53, '2017-06-03 23:31:33', 5),
(8, 53, '2017-06-03 23:43:57', 5),
(8, 53, '2017-06-03 23:44:07', 5),
(8, 53, '2017-06-03 23:57:33', 5),
(8, 53, '2017-06-03 23:58:05', 5),
(8, 53, '2017-06-03 23:58:09', 5),
(8, 53, '2017-06-03 23:58:43', 5),
(8, 53, '2017-06-03 23:59:02', 5),
(8, 53, '2017-06-04 11:00:55', 5),
(8, 53, '2017-06-06 09:00:30', 5),
(8, 53, '2017-06-06 09:00:44', 5),
(8, 53, '2017-06-06 09:43:53', 5),
(8, 53, '2017-06-06 09:44:05', 5),
(8, 53, '2017-06-06 09:48:36', 5),
(8, 53, '2017-06-06 09:48:40', 5),
(8, 53, '2017-06-06 10:10:50', 5),
(8, 53, '2017-06-06 10:25:13', 5),
(8, 53, '2017-06-06 10:28:49', 5),
(8, 53, '2017-06-06 10:28:50', 5),
(8, 53, '2017-06-06 10:30:00', 5),
(8, 53, '2017-06-06 10:30:01', 5),
(8, 53, '2017-06-06 10:30:14', 5),
(8, 53, '2017-06-06 10:31:26', 5),
(8, 53, '2017-06-06 10:31:27', 5),
(8, 53, '2017-06-06 10:31:31', 5),
(8, 53, '2017-06-06 10:31:36', 5),
(8, 53, '2017-06-06 10:31:38', 5),
(8, 53, '2017-06-06 10:31:39', 5),
(8, 53, '2017-06-06 10:31:41', 5),
(8, 53, '2017-06-06 10:31:43', 5),
(8, 53, '2017-06-06 10:31:46', 5),
(8, 53, '2017-06-06 10:33:55', 5),
(8, 53, '2017-06-06 10:33:59', 5),
(8, 53, '2017-06-06 10:34:17', 5),
(8, 53, '2017-06-06 10:34:18', 5),
(8, 53, '2017-06-06 10:38:08', 5),
(8, 53, '2017-06-06 10:40:30', 5),
(8, 53, '2017-06-06 10:41:04', 5),
(8, 53, '2017-06-06 10:41:34', 5),
(8, 53, '2017-06-06 10:42:45', 5),
(8, 53, '2017-06-06 10:43:38', 5),
(8, 53, '2017-06-06 10:43:42', 5),
(8, 53, '2017-06-07 08:01:04', 5),
(8, 53, '2017-06-07 08:01:09', 5),
(8, 53, '2017-06-08 02:17:39', 5),
(10, 2, '2017-06-07 12:10:20', 5),
(10, 3, '2017-06-02 22:00:00', 5),
(10, 5, '2017-06-02 22:00:00', 5),
(10, 5, '2017-06-03 11:48:15', 5),
(10, 5, '2017-06-03 11:52:50', 5),
(10, 5, '2017-06-03 11:53:07', 5),
(10, 5, '2017-06-03 11:53:09', 5),
(10, 5, '2017-06-03 12:36:36', 5),
(10, 5, '2017-06-04 00:00:29', 5),
(10, 5, '2017-06-04 00:01:58', 5),
(10, 46, '2017-06-03 12:03:14', 5),
(10, 46, '2017-06-04 18:24:46', 5),
(10, 46, '2017-06-04 18:25:19', 5),
(10, 52, '2017-06-03 18:39:02', 5),
(10, 52, '2017-06-03 18:39:23', 5),
(10, 52, '2017-06-04 10:17:55', 5),
(10, 52, '2017-06-06 18:57:23', 5),
(10, 53, '2017-06-03 20:59:50', 5),
(10, 53, '2017-06-03 23:58:58', 5);

-- --------------------------------------------------------

--
-- Tablična struktura za tablicu `dnevnik_baze`
--

CREATE TABLE IF NOT EXISTS `dnevnik_baze` (
  `iddnevnik_baze` int(11) NOT NULL AUTO_INCREMENT,
  `upit` text,
  `korisnik_idkorisnika` int(11) NOT NULL,
  PRIMARY KEY (`iddnevnik_baze`),
  KEY `fk_dnevnik_baze_korisnik1_idx` (`korisnik_idkorisnika`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Izbacivanje podataka za tablicu `dnevnik_baze`
--

INSERT INTO `dnevnik_baze` (`iddnevnik_baze`, `upit`, `korisnik_idkorisnika`) VALUES
(1, 'CREATE TABLE IF NOT EXISTS `WebDiP2016x004`.`tip_korisnika` (\r\n  `idtip_korisnika` INT NOT NULL AUTO_INCREMENT,\r\n  `naziv` VARCHAR(45) NULL,\r\n  PRIMARY KEY (`idtip_korisnika`))\r\nENGINE = InnoDB;', 1),
(2, 'CREATE TABLE IF NOT EXISTS `WebDiP2016x004`.`kategorija_usluga` (\r\n  `idkategorija` INT NOT NULL AUTO_INCREMENT,\r\n  `naziv` VARCHAR(45) NULL,\r\n  PRIMARY KEY (`idkategorija`))\r\nENGINE = InnoDB;', 1),
(3, 'CREATE TABLE IF NOT EXISTS `WebDiP2016x004`.`rezervacije` (\r\n  `idrezervacije` INT NOT NULL AUTO_INCREMENT,\r\n  `datum` DATE NULL,\r\n  `vrijeme` TIME NULL,\r\n  `korisnik_idkorisnik` INT NOT NULL,\r\n  `usluga_idusluga` INT NOT NULL,\r\n  `potvrda` TINYINT(1) NULL,\r\n  PRIMARY KEY (`idrezervacije`),\r\n  INDEX `fk_rezervacije_korisnik1_idx` (`korisnik_idkorisnik` ASC),\r\n  INDEX `fk_rezervacije_usluga1_idx` (`usluga_idusluga` ASC),\r\n  CONSTRAINT `fk_rezervacije_korisnik1`\r\n    FOREIGN KEY (`korisnik_idkorisnik`)\r\n    REFERENCES `WebDiP2016x004`.`korisnik` (`idkorisnika`)\r\n    ON DELETE NO ACTION\r\n    ON UPDATE NO ACTION,\r\n  CONSTRAINT `fk_rezervacije_usluga1`\r\n    FOREIGN KEY (`usluga_idusluga`)\r\n    REFERENCES `WebDiP2016x004`.`usluga` (`idusluga`)\r\n    ON DELETE NO ACTION\r\n    ON UPDATE NO ACTION)\r\nENGINE = InnoDB;', 1),
(4, 'CREATE TABLE IF NOT EXISTS `WebDiP2016x004`.`pdf` (\r\n  `idpdf` INT NOT NULL AUTO_INCREMENT,\r\n  `naziv` VARCHAR(45) NULL,\r\n  `path` TEXT NULL,\r\n  `vrijeme` TIMESTAMP NULL,\r\n  PRIMARY KEY (`idpdf`))\r\nENGINE = InnoDB;\r\n', 1),
(5, 'CREATE TABLE IF NOT EXISTS `WebDiP2016x004`.`video` (\r\n  `idvideo` INT NOT NULL AUTO_INCREMENT,\r\n  `naziv` VARCHAR(45) NULL,\r\n  `path` TEXT NULL,\r\n  `vrijeme` TIMESTAMP NULL,\r\n  PRIMARY KEY (`idvideo`))\r\nENGINE = InnoDB;', 1),
(6, 'CREATE TABLE IF NOT EXISTS `WebDiP2016x004`.`dnevnik_rada` (\r\n  `iddnevnik_rada` INT NOT NULL AUTO_INCREMENT,\r\n  `korisnik_idkorisnika` INT NOT NULL,\r\n  `vrijeme` TIMESTAMP NULL,\r\n  `radnja` TEXT NULL,\r\n  PRIMARY KEY (`iddnevnik_rada`),\r\n  INDEX `fk_dnevnik_rada_korisnik1_idx` (`korisnik_idkorisnika` ASC),\r\n  CONSTRAINT `fk_dnevnik_rada_korisnik1`\r\n    FOREIGN KEY (`korisnik_idkorisnika`)\r\n    REFERENCES `WebDiP2016x004`.`korisnik` (`idkorisnika`)\r\n    ON DELETE NO ACTION\r\n    ON UPDATE NO ACTION)\r\nENGINE = InnoDB;\r\n', 1),
(7, 'CREATE TABLE IF NOT EXISTS `WebDiP2016x004`.`dnevnik_rada` (\r\n  `iddnevnik_rada` INT NOT NULL AUTO_INCREMENT,\r\n  `korisnik_idkorisnika` INT NOT NULL,\r\n  `vrijeme` TIMESTAMP NULL,\r\n  `radnja` TEXT NULL,\r\n  PRIMARY KEY (`iddnevnik_rada`),\r\n  INDEX `fk_dnevnik_rada_korisnik1_idx` (`korisnik_idkorisnika` ASC),\r\n  CONSTRAINT `fk_dnevnik_rada_korisnik1`\r\n    FOREIGN KEY (`korisnik_idkorisnika`)\r\n    REFERENCES `WebDiP2016x004`.`korisnik` (`idkorisnika`)\r\n    ON DELETE NO ACTION\r\n    ON UPDATE NO ACTION)\r\nENGINE = InnoDB;\r\n', 1),
(8, 'CREATE TABLE IF NOT EXISTS `WebDiP2016x004`.`dnevnik_rada` (\r\n  `iddnevnik_rada` INT NOT NULL AUTO_INCREMENT,\r\n  `korisnik_idkorisnika` INT NOT NULL,\r\n  `vrijeme` TIMESTAMP NULL,\r\n  `radnja` TEXT NULL,\r\n  PRIMARY KEY (`iddnevnik_rada`),\r\n  INDEX `fk_dnevnik_rada_korisnik1_idx` (`korisnik_idkorisnika` ASC),\r\n  CONSTRAINT `fk_dnevnik_rada_korisnik1`\r\n    FOREIGN KEY (`korisnik_idkorisnika`)\r\n    REFERENCES `WebDiP2016x004`.`korisnik` (`idkorisnika`)\r\n    ON DELETE NO ACTION\r\n    ON UPDATE NO ACTION)\r\nENGINE = InnoDB;\r\n', 1),
(9, 'CREATE TABLE IF NOT EXISTS `WebDiP2016x004`.`dnevnik_prijava` (\r\n  `iddnevnik_prijava` INT NOT NULL AUTO_INCREMENT,\r\n  `vrijeme` TIMESTAMP NULL,\r\n  `prijava/odjava` TINYINT(1) NULL,\r\n  `korisnik_idkorisnika` INT NOT NULL,\r\n  PRIMARY KEY (`iddnevnik_prijava`),\r\n  INDEX `fk_dnevnik_prijava_korisnik1_idx` (`korisnik_idkorisnika` ASC),\r\n  CONSTRAINT `fk_dnevnik_prijava_korisnik1`\r\n    FOREIGN KEY (`korisnik_idkorisnika`)\r\n    REFERENCES `WebDiP2016x004`.`korisnik` (`idkorisnika`)\r\n    ON DELETE NO ACTION\r\n    ON UPDATE NO ACTION)\r\nENGINE = InnoDB;', 1),
(10, 'CREATE TABLE IF NOT EXISTS `WebDiP2016x004`.`pomak_vremena` (\r\n  `idpomak_vremena` INT NOT NULL AUTO_INCREMENT,\r\n  `pomak` INT NULL,\r\n  PRIMARY KEY (`idpomak_vremena`))\r\nENGINE = InnoDB;', 1);

-- --------------------------------------------------------

--
-- Tablična struktura za tablicu `dnevnik_prijava`
--

CREATE TABLE IF NOT EXISTS `dnevnik_prijava` (
  `iddnevnik_prijava` int(11) NOT NULL AUTO_INCREMENT,
  `vrijeme` timestamp NULL DEFAULT NULL,
  `prijava/odjava` tinyint(1) DEFAULT NULL,
  `korisnik_idkorisnika` int(11) NOT NULL,
  PRIMARY KEY (`iddnevnik_prijava`),
  KEY `fk_dnevnik_prijava_korisnik1_idx` (`korisnik_idkorisnika`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=23 ;

--
-- Izbacivanje podataka za tablicu `dnevnik_prijava`
--

INSERT INTO `dnevnik_prijava` (`iddnevnik_prijava`, `vrijeme`, `prijava/odjava`, `korisnik_idkorisnika`) VALUES
(1, '2017-03-28 20:21:04', 1, 1),
(2, '2017-03-28 20:21:04', 1, 6),
(3, '2017-03-28 20:21:04', 0, 6),
(4, '2017-03-28 20:21:04', 1, 4),
(5, '2017-03-28 20:21:04', 0, 4),
(6, '2017-03-28 20:21:04', 1, 5),
(7, '2017-03-28 20:21:04', 0, 5),
(8, '2017-03-28 20:21:04', 1, 7),
(9, '2017-03-28 20:21:04', 0, 7),
(10, '2017-03-28 20:21:04', 0, 5),
(11, '2017-06-07 14:20:09', 1, 52),
(12, '2017-06-07 14:26:26', 1, 52),
(13, '2017-06-07 14:39:43', 0, 52),
(14, '2017-06-07 14:39:48', 1, 53),
(15, '2017-06-07 16:12:53', 1, 52),
(16, '2017-06-08 02:11:19', 1, 53),
(17, '2017-06-08 02:16:56', 1, 53),
(18, '2017-06-08 02:18:19', 1, 52),
(19, '2017-06-08 02:20:51', 0, 52),
(20, '2017-06-08 08:13:41', 1, 52),
(21, '2017-06-08 08:18:32', 0, 52),
(22, '2017-06-08 08:18:36', 1, 53);

-- --------------------------------------------------------

--
-- Tablična struktura za tablicu `dnevnik_rada`
--

CREATE TABLE IF NOT EXISTS `dnevnik_rada` (
  `iddnevnik_rada` int(11) NOT NULL AUTO_INCREMENT,
  `korisnik_idkorisnika` int(11) NOT NULL,
  `vrijeme` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `radnja` text,
  PRIMARY KEY (`iddnevnik_rada`),
  KEY `fk_dnevnik_rada_korisnik1_idx` (`korisnik_idkorisnika`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=156 ;

--
-- Izbacivanje podataka za tablicu `dnevnik_rada`
--

INSERT INTO `dnevnik_rada` (`iddnevnik_rada`, `korisnik_idkorisnika`, `vrijeme`, `radnja`) VALUES
(1, 4, '2017-03-28 20:24:44', 'Klik na kupon'),
(2, 5, '2017-03-28 20:24:44', 'Rezervacija usluge'),
(3, 7, '2017-03-28 20:24:44', 'Rezervacija usluge'),
(4, 7, '2017-03-28 20:24:44', 'Klik na kupon'),
(5, 9, '2017-03-28 20:24:44', 'Rezervacija usluge'),
(6, 7, '2017-03-28 20:24:44', 'Klik na kupon'),
(7, 5, '2017-03-28 20:24:44', 'Rezervacija usluge'),
(8, 2, '2017-03-28 20:24:44', 'Klik na kupon'),
(9, 6, '2017-03-28 20:24:44', 'Rezervacija usluge'),
(10, 7, '2017-03-28 20:24:44', 'Rezervacija usluge'),
(11, 1, '2017-05-02 20:36:19', 'Unesen je novi proizvod.'),
(14, 53, '2017-06-07 12:21:07', 'Ulazak u aktivniKuponi.php'),
(17, 52, '2017-06-07 14:20:10', 'Ulazak u index.php'),
(22, 52, '2017-06-07 14:20:43', 'Ulazak u index.php'),
(23, 52, '2017-06-07 14:20:47', 'Ulazak u aktivniKuponi.php'),
(24, 52, '2017-06-07 14:20:49', 'Ulazak u kupon.php'),
(37, 52, '2017-06-07 14:26:26', 'Ulazak u index.php'),
(38, 52, '2017-06-07 14:26:31', 'Ulazak u index.php'),
(39, 52, '2017-06-07 14:26:31', 'Ulazak u index.php'),
(40, 52, '2017-06-07 14:26:42', 'Ulazak u index.php'),
(41, 52, '2017-06-07 14:26:44', 'Ulazak u index.php'),
(42, 52, '2017-06-07 14:26:45', 'Ulazak u index.php'),
(43, 52, '2017-06-07 14:26:46', 'Ulazak u index.php'),
(44, 52, '2017-06-07 14:26:47', 'Ulazak u index.php'),
(45, 52, '2017-06-07 14:26:48', 'Ulazak u index.php'),
(46, 52, '2017-06-07 14:26:49', 'Ulazak u index.php'),
(47, 52, '2017-06-07 14:26:50', 'Ulazak u index.php'),
(48, 52, '2017-06-07 14:26:52', 'Ulazak u index.php'),
(49, 52, '2017-06-07 14:26:53', 'Ulazak u index.php'),
(50, 52, '2017-06-07 14:27:36', 'Ulazak u kalendar.php'),
(51, 52, '2017-06-07 14:27:55', 'Ulazak u kalendar.php'),
(52, 52, '2017-06-07 14:28:00', 'Ulazak u kalendar.php'),
(53, 52, '2017-06-07 14:28:37', 'Ulazak u aktivniKuponi.php'),
(54, 52, '2017-06-07 14:28:53', 'Ulazak u skupljeniBodovi.php.'),
(55, 52, '2017-06-07 14:29:00', 'Ulazak u kosarica.php'),
(56, 52, '2017-06-07 14:29:02', 'Ulazak u aktivniKuponi.php'),
(57, 52, '2017-06-07 14:29:04', 'Ulazak u kupon.php'),
(58, 52, '2017-06-07 14:29:06', 'Ulazak u kupon.php'),
(59, 52, '2017-06-07 14:29:08', 'Ulazak u kupon.php'),
(60, 52, '2017-06-07 14:29:10', 'Ulazak u kupon.php'),
(61, 52, '2017-06-07 14:29:12', 'Ulazak u kupon.php'),
(62, 52, '2017-06-07 14:29:14', 'Ulazak u kupon.php'),
(63, 52, '2017-06-07 14:29:16', 'Ulazak u kosarica.php'),
(64, 52, '2017-06-07 14:29:19', 'Ulazak u kosarica.php'),
(65, 52, '2017-06-07 14:29:19', 'Ulazak u kosarica.php'),
(66, 52, '2017-06-07 14:29:20', 'Ulazak u aktivniKuponi.php'),
(67, 52, '2017-06-07 14:29:22', 'Ulazak u kupon.php'),
(68, 52, '2017-06-07 14:29:24', 'Ulazak u kupon.php'),
(69, 52, '2017-06-07 14:29:26', 'Ulazak u kosarica.php'),
(70, 52, '2017-06-07 14:29:35', 'Ulazak u aktivniKuponi.php'),
(71, 52, '2017-06-07 14:29:37', 'Ulazak u kupon.php'),
(72, 52, '2017-06-07 14:29:39', 'Ulazak u kupon.php'),
(73, 52, '2017-06-07 14:29:40', 'Ulazak u kupon.php'),
(74, 52, '2017-06-07 14:29:42', 'Ulazak u kupon.php'),
(75, 52, '2017-06-07 14:29:43', 'Ulazak u kupon.php'),
(76, 52, '2017-06-07 14:29:45', 'Ulazak u kupon.php'),
(77, 52, '2017-06-07 14:29:46', 'Ulazak u kupon.php'),
(78, 52, '2017-06-07 14:29:50', 'Ulazak u kosarica.php'),
(79, 52, '2017-06-07 14:29:54', 'Ulazak u kosarica.php'),
(80, 52, '2017-06-07 14:29:55', 'Ulazak u kosarica.php'),
(81, 52, '2017-06-07 14:32:24', 'Ulazak u kosarica.php'),
(82, 52, '2017-06-07 14:38:41', 'Ulazak u pomakVremena.php'),
(83, 52, '2017-06-07 14:38:41', 'Ulazak u index.php'),
(84, 53, '2017-06-07 14:39:48', 'Ulazak u index.php'),
(85, 53, '2017-06-07 14:39:54', 'Ulazak u pomakVremena.php'),
(86, 53, '2017-06-07 14:41:10', 'Ulazak u pomakVremena.php'),
(87, 53, '2017-06-07 14:43:15', 'Ulazak u pomakVremena.php'),
(88, 53, '2017-06-07 14:50:31', 'Ulazak u pomakVremena.php'),
(89, 53, '2017-06-07 15:26:30', 'Ulazak u pomakVremena.php'),
(90, 53, '2017-06-07 15:27:57', 'Ulazak u pomakVremena.php'),
(91, 53, '2017-06-07 15:29:03', 'Ulazak u index.php'),
(92, 53, '2017-06-07 15:30:10', 'Ulazak u index.php'),
(93, 53, '2017-06-07 15:30:35', 'Ulazak u index.php'),
(94, 53, '2017-06-07 15:30:36', 'Ulazak u pomakVremena.php'),
(95, 53, '2017-06-07 15:32:42', 'Ulazak u pomakVremena.php'),
(98, 52, '2017-06-07 16:12:53', 'Ulazak u index.php'),
(99, 52, '2017-06-07 16:12:56', 'Ulazak u aktivniKuponi.php'),
(107, 53, '2017-06-08 02:11:19', 'Ulazak u index.php'),
(119, 53, '2017-06-08 02:16:57', 'Ulazak u index.php'),
(120, 53, '2017-06-08 02:17:01', 'Ulazak u index.php'),
(121, 53, '2017-06-08 02:17:05', 'Ulazak u index.php'),
(122, 53, '2017-06-08 02:17:14', 'Ulazak u index.php'),
(123, 53, '2017-06-08 02:17:16', 'Ulazak u skupljeniBodovi.php.'),
(124, 53, '2017-06-08 02:17:22', 'Ulazak u index.php'),
(125, 53, '2017-06-08 02:17:25', 'Ulazak u index.php'),
(127, 53, '2017-06-08 02:17:35', 'Ulazak u index.php'),
(128, 53, '2017-06-08 02:17:39', 'Ulazak u mojeRezervacije.php'),
(133, 52, '2017-06-08 02:18:19', 'Ulazak u index.php'),
(134, 52, '2017-06-08 02:18:29', 'Ulazak u index.php'),
(135, 52, '2017-06-08 02:18:33', 'Ulazak u mojeRezervacije.php'),
(136, 52, '2017-06-08 02:18:41', 'Ulazak u mojeRezervacije.php'),
(137, 52, '2017-06-08 02:18:42', 'Ulazak u mojeRezervacije.php'),
(138, 52, '2017-06-08 02:18:47', 'Ulazak u skupljeniBodovi.php.'),
(139, 52, '2017-06-08 02:18:58', 'Ulazak u aktivniKuponi.php'),
(140, 52, '2017-06-08 02:19:07', 'Ulazak u kupon.php'),
(141, 52, '2017-06-08 02:19:27', 'Ulazak u kosarica.php'),
(142, 52, '2017-06-08 02:19:40', 'Ulazak u definirajUsluge.php'),
(143, 52, '2017-06-08 02:19:46', 'Ulazak u rezervacijeModerator.php.'),
(144, 52, '2017-06-08 02:20:00', 'Ulazak u promijeniIzgled.php.'),
(145, 52, '2017-06-08 02:20:06', 'Ulazak u kalendar.php'),
(146, 52, '2017-06-08 02:20:16', 'Ulazak u kalendar.php'),
(147, 52, '2017-06-08 02:20:26', 'Ulazak u provjeriKupon.php.'),
(148, 52, '2017-06-08 02:20:33', 'Ulazak u provjeriKupon.php.'),
(149, 52, '2017-06-08 02:20:38', 'Ulazak u noviKupon.php'),
(152, 52, '2017-06-08 08:13:41', 'Ulazak u index.php'),
(153, 53, '2017-06-08 08:18:36', 'Ulazak u index.php'),
(154, 53, '2017-06-08 08:19:15', 'Ulazak u index.php'),
(155, 53, '2017-06-08 08:26:40', 'Ulazak u index.php');

-- --------------------------------------------------------

--
-- Tablična struktura za tablicu `kategorija_usluga`
--

CREATE TABLE IF NOT EXISTS `kategorija_usluga` (
  `idkategorija` int(11) NOT NULL AUTO_INCREMENT,
  `naziv` varchar(45) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `boja` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `font` varchar(50) DEFAULT NULL,
  `pozadina` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`idkategorija`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=22 ;

--
-- Izbacivanje podataka za tablicu `kategorija_usluga`
--

INSERT INTO `kategorija_usluga` (`idkategorija`, `naziv`, `boja`, `font`, `pozadina`) VALUES
(1, 'Frizer', 'boja3', 'font1', 'pozadina1'),
(2, 'Masaža', 'boja2', 'font2', 'pozadina2'),
(3, 'Sauna', 'boja3', 'font3', 'pozadina3'),
(4, 'Bazeni', 'boja3', 'font3', 'pozadina3'),
(5, 'Njega lica', 'boja1', 'font3', 'pozadina3'),
(6, 'Njega tijela', 'boja2', 'font2', 'pozadina2'),
(7, 'Kupka', 'boja3', 'font3', 'pozadina3'),
(8, 'Manikura', 'boja1', 'font1', 'pozadina1'),
(9, 'Kat9', 'boja1', 'font1', 'pozadina1'),
(10, 'Kat10', 'boja1', 'font1', 'pozadina1'),
(11, 'Kat11', 'boja1', 'font1', 'pozadina1'),
(12, 'Kat12', 'boja1', 'font1', 'pozadina1'),
(13, 'Kat13', 'boja1', 'font1', 'pozadina1'),
(14, 'Kat14', 'boja1', 'font1', 'pozadina1'),
(15, 'Kat15', 'boja1', 'font1', 'pozadina1'),
(16, 'Kat16', 'boja1', 'font1', 'pozadina1'),
(17, 'Kat17', 'boja3', 'font2', 'pozadina3'),
(18, 'Kat18', 'boja3', 'font2', 'pozadina3'),
(19, 'Kat19', 'boja3', 'font2', 'pozadina3'),
(20, 'Kat20', 'boja1', 'font1', 'pozadina1'),
(21, 'Kat21', 'boja1', 'font1', 'pozadina1');

-- --------------------------------------------------------

--
-- Tablična struktura za tablicu `korisnik`
--

CREATE TABLE IF NOT EXISTS `korisnik` (
  `idkorisnika` int(11) NOT NULL AUTO_INCREMENT,
  `ime` varchar(45) DEFAULT NULL,
  `prezime` varchar(45) DEFAULT NULL,
  `korisnickoIme` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `lozinka` varchar(45) DEFAULT NULL,
  `hash_lozinka` varchar(45) DEFAULT NULL,
  `blokiran` tinyint(1) DEFAULT NULL,
  `tip_korisnika_idtip_korisnika` int(11) NOT NULL,
  `broj_indeksa` varchar(15) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `prijava_u_dva_koraka` tinyint(1) DEFAULT NULL,
  `broj_pokusaja` int(11) DEFAULT NULL,
  `aktivacijski_kod` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`idkorisnika`),
  KEY `fk_korisnik_tip_korisnika1_idx` (`tip_korisnika_idtip_korisnika`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=73 ;

--
-- Izbacivanje podataka za tablicu `korisnik`
--

INSERT INTO `korisnik` (`idkorisnika`, `ime`, `prezime`, `korisnickoIme`, `email`, `lozinka`, `hash_lozinka`, `blokiran`, `tip_korisnika_idtip_korisnika`, `broj_indeksa`, `prijava_u_dva_koraka`, `broj_pokusaja`, `aktivacijski_kod`) VALUES
(1, 'Gloria', 'Babi?', 'globabic', 'globabic@foi.hr', 'WebDiP0237', NULL, 0, 1, NULL, NULL, 0, NULL),
(2, 'Jelena', '?uka', 'jelcuka', 'jelcuka@foi.hr', 'jelenalozinka', NULL, 0, 2, NULL, NULL, 0, NULL),
(3, 'Petar', 'Jadek', 'pjadek', 'pjadek@foi.hr', 'petarlozinka', NULL, 0, 2, '43315/15-R', NULL, 1, NULL),
(4, 'Kristina', 'Ivatovi?', 'kivatovi', 'kivatovi@foi.hr', 'kristinalozinka', NULL, 0, 3, NULL, NULL, 0, NULL),
(5, 'Lara', 'Babi?', 'larabab', 'larabab@gmail.com', 'laralozinka', NULL, 0, 3, NULL, NULL, 1, NULL),
(6, 'Mario', 'Babi?', 'm4R10', 'mario.babic@hotmail.com', 'mariolozinka', NULL, 0, 3, NULL, NULL, 3, NULL),
(7, 'Karlo', 'Škobalj', 'kskobalj', 'kskobalj@gmail.com', 'karlolozinka', NULL, 0, 2, NULL, NULL, 0, NULL),
(8, 'Zdravka', 'Lozan?i?', 'zlozan', 'zlozan@gmail.com', 'zdravkalozinka', NULL, 0, 3, NULL, NULL, 0, NULL),
(9, 'Marko', '?ori?', 'macoric', 'macoric@gmail.com', 'markolozinka', NULL, 0, 3, NULL, NULL, 0, NULL),
(10, 'Ana', '?uli?', 'anaculic', 'anaculic@gmail.com', 'analozinka', NULL, 0, 3, NULL, NULL, 3, NULL),
(46, 'E', 'E', 'E', 'webdip@mail.com', 'E', '75f31f4fe9b03ecd5e242130b949af290aa90487jkwgv', 0, 3, NULL, 1, 2, NULL),
(50, 'Petar', 'Jadek', 'aaa', 'aaaa', 'r', '4dc7c9ec434ed06502767136789763ec11d2c4b7jkwgv', 0, 3, NULL, NULL, 0, NULL),
(52, 'm', 'm', 'm', 'webdip@mail.com', 'm', '1b6453892473a467d07372d45eb05abc2031647ajkwgv', 0, 2, NULL, NULL, 0, NULL),
(53, 'a', 'a', 'a', 'webdip@mail.com', 'a', '86f7e437faa5a7fce15d1ddcb9eaeaea377667b8jkwgv', 0, 1, NULL, NULL, 0, NULL),
(54, 'Ana', 'Vu?aj', 'anavucaj', 'anavucaj@gmail.com', 'AAAAAaaaaa55555', 'd2a7c114a50caf2f7a899072f3c8e414ca5cf2efjkwgv', 0, 3, '41257/14-R', 1, 0, NULL),
(55, 'Jovanka', 'Broz', 'jobroz', 'jobroz@gmail.com', 'LL22ll', '27495c727210cc81206af96d00723a1dabdbabafjkwgv', 0, 3, '41257/14-R', 1, 0, NULL),
(56, 'Mia', 'Šimunovi?', 'asimunov', 'asimunov@foi.hr', 'SS22ss', '8b5caab4f117d8a2197f88f7729ca0ff61590f03jkwgv', 0, 3, '41257/14-R', 1, 0, NULL),
(57, 'ko', 'ko', 'kokolo', 'ko@go.com', 'KKkk22', '50a93fb98963d126ca880b82fd1cef8ec9000ecbjkwgv', 0, 3, '41257/14-R', 1, 0, NULL),
(58, 'Larisa', 'Laric', 'larisalar', 'larisalar@gmail.com', 'LL22ll', '27495c727210cc81206af96d00723a1dabdbabafjkwgv', 0, 3, '41257/14-R', 1, 0, NULL),
(59, 'Karla', 'Vodanovi?', 'kvoda', 'kvoda@gmail.com', 'OO00oo', '05cdbeb6f8b20cb367593e16aeb2a4feeb38577ejkwgv', 0, 3, '41257/14-R', 1, 0, NULL),
(60, 'Marija', 'Mari?', 'mmaric', 'mmaric@gmail.com', 'MMmm22', '4223bb2b0aac1953d3c671822e0b59a88f515576jkwgv', 0, 3, '41257/14-R', 1, 0, NULL),
(61, 'Marija', 'Babi?', 'mbabic', 'mbabic@mail.com', 'MMmm22', '4223bb2b0aac1953d3c671822e0b59a88f515576jkwgv', 0, 3, '41257/14-R', 0, 0, NULL),
(62, 'Anananan', 'Annaasasa', 'ananannana', 'anannanana@gmail.com', 'AAaa22', '88ba6a5f9be29c4a087c9b73f8b3f764606b0827jkwgv', 0, 3, '41257/14-R', 0, 0, NULL),
(63, 'Mamamamm', 'Mamammam', 'ammamamam', 'webdip@mail.com', 'MMmm22', '4223bb2b0aac1953d3c671822e0b59a88f515576jkwgv', 0, 3, '41257/14-R', 0, 0, '1a122fe0a8c00ae3506c9b6b2ffb2fa9ba8caa3f'),
(64, 'Lolololo', 'Lollolo', 'Lola', 'webdip@mail.com', 'LLll22', '94ce25c73be19ab2c6644224a17fe52cec62f85ajkwgv', 0, 3, '41257/14-R', 1, 0, '002f118ede6507872701fb92752e9a1d2ba577d3'),
(65, 'Hehhehe', 'Hehehheh', 'Hehe', 'webdip@mail.com', '09551b1964e6477c6144885361d7dc3985313c9a', 'a694149e1bc4e0e58031e6fd0efc69b0df7cbccfjkwgv', 0, 3, '41257/14-R', 1, 0, '34e578076c83650903b2b48fb2bfd0779dff9c67'),
(66, 'Hihi', 'Hihi', 'Hihi', 'webdip@mail.com', 'JJjj22', '688f6096f8a168938e884345af973372455a36c3jkwgv', 1, 3, '41257/14-R', 1, 0, '58a8b82b31bdee05ed363cd901ae11ae71b53753'),
(67, 'Jelena', 'Cuka', 'jelenacuka', 'webdip@mail.com', 'JJjj22', '688f6096f8a168938e884345af973372455a36c3jkwgv', 0, 3, '41257/14-R', 1, 0, 'e3580d9a3dc8479245f2fca34891307851dc756f'),
(68, 'Matea', 'Bodulusic', 'mbodulu', 'webdip@mail.com', 'MMmm22', '4223bb2b0aac1953d3c671822e0b59a88f515576jkwgv', 1, 3, '41257/14-R', 1, 0, 'b585f3de0906935167b9f8d4044dfd08528d3a0e'),
(69, 'WebDiP', 'WebDiP', 'WebDiP2016x004', 'webdip@mail.com', 'admin_lmIl', NULL, 0, 1, NULL, 0, 0, '0'),
(70, 'Ananan', 'Ananan', 'a', 'anavufffcaj@gmail.com', 'AAaa22', '88ba6a5f9be29c4a087c9b73f8b3f764606b0827jkwgv', 1, 3, '41257/14-R', 1, 0, '625863d3ce82e53be1cdfebaa90c554d141364bb'),
(71, 'Janja', 'Peri?', 'larabab', 'larica@gmail.com', 'LLll22', '94ce25c73be19ab2c6644224a17fe52cec62f85ajkwgv', 1, 3, '41257/14-R', 0, 1, 'ae2f638607a6e1269c9097dabbd7aede89d70b31'),
(72, '', '', '', '', '', '', 1, 3, '41257/14-R', 0, 0, '');

-- --------------------------------------------------------

--
-- Tablična struktura za tablicu `košarica`
--

CREATE TABLE IF NOT EXISTS `košarica` (
  `idkošarica` int(11) NOT NULL AUTO_INCREMENT,
  `korisnik_idkorisnik` int(11) NOT NULL,
  `kupon_idkupon` int(11) NOT NULL,
  `kod_kupona` varchar(100) DEFAULT '0',
  PRIMARY KEY (`idkošarica`),
  KEY `fk_košarica_korisnik1_idx` (`korisnik_idkorisnik`),
  KEY `fk_košarica_kupon1_idx` (`kupon_idkupon`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=62 ;

--
-- Izbacivanje podataka za tablicu `košarica`
--

INSERT INTO `košarica` (`idkošarica`, `korisnik_idkorisnik`, `kupon_idkupon`, `kod_kupona`) VALUES
(1, 4, 1, '1456'),
(2, 4, 2, '45656'),
(3, 5, 3, '4523574'),
(4, 5, 10, '0'),
(5, 6, 6, '42'),
(6, 6, 6, '0'),
(7, 7, 1, '34534'),
(8, 7, 2, '45345'),
(9, 8, 9, '53453'),
(10, 8, 3, '453354'),
(11, 8, 6, '45345'),
(12, 53, 4, '44260'),
(13, 53, 4, '10669'),
(14, 53, 3, 'izbrisan'),
(15, 53, 3, 'izbrisan'),
(16, 53, 3, 'izbrisan'),
(17, 53, 9, 'izbrisan'),
(18, 53, 10, 'izbrisan'),
(19, 53, 1, 'izbrisan'),
(20, 53, 4, 'izbrisan'),
(21, 53, 10, '65568'),
(22, 53, 10, '71642'),
(23, 53, 9, '95268'),
(24, 52, 1, '11610'),
(25, 52, 8, 'izbrisan'),
(26, 52, 1, '3323'),
(27, 52, 1, '53029'),
(28, 52, 10, '99870'),
(29, 52, 1, '79953'),
(30, 52, 4, '29701'),
(31, 52, 8, '71604'),
(32, 52, 9, '79275'),
(33, 52, 10, '79619'),
(34, 52, 10, '13190'),
(35, 52, 1, '52679'),
(36, 52, 1, '57186'),
(37, 52, 9, '7854'),
(38, 52, 2, '79184'),
(39, 52, 9, '19687'),
(40, 52, 8, '8369'),
(41, 52, 4, '85326'),
(42, 46, 8, '70632'),
(43, 46, 2, 'izbrisan'),
(44, 46, 9, '9212'),
(45, 46, 9, 'izbrisan'),
(46, 46, 10, '92503'),
(47, 46, 1, '88575'),
(48, 46, 9, '22947'),
(49, 53, 10, '7981'),
(50, 52, 9, '50316'),
(51, 52, 9, '51100'),
(52, 52, 9, '11511'),
(53, 52, 9, '35237'),
(54, 52, 9, '44918'),
(55, 52, 4, '56224'),
(56, 52, 4, '70691'),
(57, 52, 4, '32228'),
(58, 52, 4, '38310'),
(59, 52, 4, '54428'),
(60, 52, 4, '91101'),
(61, 52, 4, '72679');

-- --------------------------------------------------------

--
-- Tablična struktura za tablicu `kupon`
--

CREATE TABLE IF NOT EXISTS `kupon` (
  `idkupon` int(11) NOT NULL AUTO_INCREMENT,
  `pocetak` date DEFAULT NULL,
  `kraj` date DEFAULT NULL,
  `bodovi` int(11) DEFAULT NULL,
  `akcija_kupona_idakcija_kupona` int(11) NOT NULL,
  `aktivan` tinyint(1) DEFAULT NULL,
  `kategorija_usluga_idkategorija` int(11) NOT NULL,
  PRIMARY KEY (`idkupon`),
  KEY `fk_kupon_akcija_kupona1_idx` (`akcija_kupona_idakcija_kupona`),
  KEY `fk_kupon_kategorija_usluga1_idx` (`kategorija_usluga_idkategorija`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=19 ;

--
-- Izbacivanje podataka za tablicu `kupon`
--

INSERT INTO `kupon` (`idkupon`, `pocetak`, `kraj`, `bodovi`, `akcija_kupona_idakcija_kupona`, `aktivan`, `kategorija_usluga_idkategorija`) VALUES
(1, '2017-03-05', '2017-09-19', 10, 4, 1, 5),
(2, '2017-03-08', '2017-11-13', 10, 4, 1, 5),
(3, '2017-03-19', '2017-10-24', 15, 4, 1, 6),
(4, '2017-03-29', '2017-09-11', 20, 5, 1, 5),
(5, '2017-03-15', '2017-08-15', 10, 6, 1, 5),
(6, '2017-03-20', '2017-04-03', 20, 1, 0, 8),
(7, '2017-03-19', '2017-11-21', 15, 5, 1, 7),
(8, '2017-03-17', '2017-10-04', 25, 3, 1, 6),
(9, '2017-05-16', '2017-09-14', 5, 1, 1, 7),
(10, '2017-03-12', '2017-08-23', 10, 6, 1, 2),
(11, '2017-06-09', '2017-06-16', 50, 11, 1, 7),
(12, '2017-06-15', '2017-06-22', 50, 3, 1, 5),
(13, '2017-06-09', '2017-06-23', 80, 10, 1, 7),
(14, '2017-06-23', '2017-06-30', 40, 12, 1, 12),
(15, '2017-06-08', '2017-06-29', 60, 12, 1, 12),
(16, '2017-06-08', '2017-06-15', 15, 12, 1, 6),
(17, '2017-06-15', '2017-06-16', 80, 12, 1, 2),
(18, '2017-06-05', '2017-06-18', 50, 9, 1, 2);

-- --------------------------------------------------------

--
-- Tablična struktura za tablicu `moderatori`
--

CREATE TABLE IF NOT EXISTS `moderatori` (
  `korisnik` int(11) NOT NULL,
  `kategorija` int(11) NOT NULL,
  PRIMARY KEY (`korisnik`,`kategorija`),
  KEY `fk_korisnik_has_kategorija_usluga_kategorija_usluga1_idx` (`kategorija`),
  KEY `fk_korisnik_has_kategorija_usluga_korisnik1_idx` (`korisnik`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Izbacivanje podataka za tablicu `moderatori`
--

INSERT INTO `moderatori` (`korisnik`, `kategorija`) VALUES
(2, 1),
(52, 1),
(2, 2),
(52, 3),
(3, 4),
(3, 5),
(52, 5),
(3, 6),
(52, 7),
(52, 8),
(2, 9),
(52, 12),
(2, 13),
(2, 21);

-- --------------------------------------------------------

--
-- Tablična struktura za tablicu `novi_proizvod`
--

CREATE TABLE IF NOT EXISTS `novi_proizvod` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `naziv` varchar(50) COLLATE utf8_bin NOT NULL,
  `opis` varchar(50) COLLATE utf8_bin NOT NULL,
  `datumProizvodnje` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `vrijemeProizvodnje` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `kolicinaProizvoda` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `tezinaProizvoda` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `kategorija1` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `kategorija2` varchar(50) COLLATE utf8_bin NOT NULL,
  `kategorija3` varchar(50) COLLATE utf8_bin NOT NULL,
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=8 ;

--
-- Izbacivanje podataka za tablicu `novi_proizvod`
--

INSERT INTO `novi_proizvod` (`id`, `naziv`, `opis`, `datumProizvodnje`, `vrijemeProizvodnje`, `kolicinaProizvoda`, `tezinaProizvoda`, `kategorija1`, `kategorija2`, `kategorija3`) VALUES
(1, 'Kukuruz', 'sfafa', '11.11.1111.', '02:02', '4', '50', '0', '0', '0'),
(2, 'Kukuruze', 'edeeee', '11.11.1111.', '03:57', '55', '39', '0', '0', '0'),
(3, 'Jabuka', 'Banana', '11.11.1111.', '01:59', '666', '30', '0', '0', '0'),
(4, 'Humus', 'bhbdcfkwebkc', '11.11.1111.', '02:02', '3', '50', '0', '0', '0'),
(5, 'Assasasasa', 'casdcasdcv', '11.11.1111.', '01:01', '3', '50', '0', '0', '0'),
(6, 'Banana', 'Jabuka', '11.11.1111.', '02:59', '555', '77', '0', '0', '0'),
(7, 'Naziv', 'aeskmfpoamj', '11.11.1111.', '05:03', '5', '55', '0', '0', '0');

-- --------------------------------------------------------

--
-- Tablična struktura za tablicu `pdf`
--

CREATE TABLE IF NOT EXISTS `pdf` (
  `idpdf` int(11) NOT NULL AUTO_INCREMENT,
  `naziv` varchar(45) DEFAULT NULL,
  `path` text,
  `vrijeme` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`idpdf`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Izbacivanje podataka za tablicu `pdf`
--

INSERT INTO `pdf` (`idpdf`, `naziv`, `path`, `vrijeme`) VALUES
(1, 'Sauna1', 'putanja', '2017-03-28 20:04:57'),
(2, 'Masaža1', 'putanja', '2017-03-28 20:04:57'),
(3, 'Sauna2', 'putanja', '2017-03-28 20:05:43'),
(4, 'Manikura1', 'putanja', '2017-03-28 20:05:43'),
(5, 'Manikura2', 'putanja', '2017-03-28 20:06:10'),
(6, 'Frizura1', 'putanja', '2017-03-28 20:06:10'),
(7, 'Frizura2', 'putanja', '2017-03-28 20:06:35'),
(8, 'Bazen4', 'putanja', '2017-03-28 20:06:35'),
(9, 'Lice1', 'putanja', '2017-03-28 20:06:57'),
(10, 'Tijelo1', 'putanja', '2017-03-28 20:06:57');

-- --------------------------------------------------------

--
-- Tablična struktura za tablicu `pomak_vremena`
--

CREATE TABLE IF NOT EXISTS `pomak_vremena` (
  `idpomak_vremena` int(11) NOT NULL AUTO_INCREMENT,
  `pomak` int(11) DEFAULT NULL,
  PRIMARY KEY (`idpomak_vremena`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Izbacivanje podataka za tablicu `pomak_vremena`
--

INSERT INTO `pomak_vremena` (`idpomak_vremena`, `pomak`) VALUES
(1, 1),
(2, 2),
(3, 3),
(4, 1),
(5, 4),
(6, 1),
(7, 2),
(8, 4),
(9, 2),
(10, 3);

-- --------------------------------------------------------

--
-- Tablična struktura za tablicu `rezervacije`
--

CREATE TABLE IF NOT EXISTS `rezervacije` (
  `idrezervacije` int(11) NOT NULL AUTO_INCREMENT,
  `datum` date DEFAULT NULL,
  `vrijeme` time DEFAULT NULL,
  `korisnik_idkorisnik` int(11) NOT NULL,
  `usluga_idusluga` int(11) NOT NULL,
  `potvrda` tinyint(1) DEFAULT NULL,
  `noviTermin` int(11) DEFAULT NULL,
  PRIMARY KEY (`idrezervacije`),
  KEY `fk_rezervacije_korisnik1_idx` (`korisnik_idkorisnik`),
  KEY `fk_rezervacije_usluga1_idx` (`usluga_idusluga`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=27 ;

--
-- Izbacivanje podataka za tablicu `rezervacije`
--

INSERT INTO `rezervacije` (`idrezervacije`, `datum`, `vrijeme`, `korisnik_idkorisnik`, `usluga_idusluga`, `potvrda`, `noviTermin`) VALUES
(1, '2028-03-20', '13:30:00', 4, 3, 0, 1),
(2, '2025-04-20', '09:00:00', 5, 6, 1, 1),
(3, '2014-05-20', '14:00:00', 6, 2, 0, 1),
(4, '2015-04-20', '15:15:00', 7, 5, 1, 0),
(5, '2017-03-15', '12:15:00', 7, 4, 0, 1),
(6, '2017-03-23', '15:45:00', 5, 9, 1, 1),
(7, '2017-03-15', '12:15:00', 7, 4, 1, 1),
(8, '2017-03-23', '15:45:00', 5, 9, 1, 1),
(9, '2017-06-06', '08:30:00', 3, 6, 1, 1),
(10, '2017-03-09', '09:05:00', 5, 9, 1, 1),
(11, '2017-06-15', '01:01:00', 3, 11, 0, 0),
(12, '2017-06-08', '13:40:00', 3, 3, 1, 1),
(13, '2017-06-23', '08:40:00', 3, 4, 0, 0),
(14, '2025-04-20', '09:00:00', 46, 10, 0, 0),
(15, '2017-06-09', '09:00:00', 52, 3, 1, 1),
(16, '2017-06-22', '08:50:00', 52, 10, 0, 0),
(17, '2017-06-22', '08:50:00', 53, 3, 1, 1),
(18, '2017-06-08', '10:00:00', 53, 11, 0, 1),
(19, '2017-06-22', '14:00:00', 5, 11, 0, 1),
(20, '2017-06-08', '08:50:00', 5, 11, 0, 0),
(21, '2017-06-08', '12:00:00', 5, 11, 0, 1),
(22, '2017-06-28', '09:00:00', 52, 3, 1, 1),
(23, '2017-06-01', '04:02:00', 46, 5, 0, 0),
(24, '2017-06-07', '03:03:00', 46, 5, 0, 0),
(25, '2017-06-15', '08:00:00', 52, 2, 0, 1),
(26, '2017-06-14', '08:00:00', 2, 26, 1, 1);

-- --------------------------------------------------------

--
-- Tablična struktura za tablicu `slika`
--

CREATE TABLE IF NOT EXISTS `slika` (
  `idslika` int(11) NOT NULL AUTO_INCREMENT,
  `naziv` varchar(45) DEFAULT NULL,
  `path` text,
  `vrijeme` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`idslika`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Izbacivanje podataka za tablicu `slika`
--

INSERT INTO `slika` (`idslika`, `naziv`, `path`, `vrijeme`) VALUES
(1, 'cetvrta', 'https://drive.google.com/open?id=0B5NVz1rA5zQMMlF2X1dCVERXR2s', '2017-03-28 20:02:07'),
(2, 'masaza', 'https://drive.google.com/open?id=0B5NVz1rA5zQMMHBON0VxNUJiMXc', '2017-03-28 20:02:07'),
(3, 'manikura', 'https://drive.google.com/open?id=0B5NVz1rA5zQMMlF2X1dCVERXR2s', '2017-03-28 20:02:45'),
(4, 'bazen', 'https://drive.google.com/open?id=0B5NVz1rA5zQMQmNuR3RTRHVwWE0', '2017-03-28 20:02:45'),
(5, 'druga', 'https://drive.google.com/open?id=0B5NVz1rA5zQMZ3cxM3ptRzdxQWs', '2017-03-28 20:03:20'),
(6, 'peta', 'https://drive.google.com/open?id=0B5NVz1rA5zQMNTJENmcxblFPaTg', '2017-03-28 20:03:20'),
(7, 'prva', 'https://drive.google.com/open?id=0B5NVz1rA5zQMY0NuUTVqVl9mTEU', '2017-03-28 20:03:49'),
(8, 'treca', 'https://drive.google.com/open?id=0B5NVz1rA5zQMUTJMRXZaZHF4amc', '2017-03-28 20:03:49'),
(9, 'bazen2', 'https://drive.google.com/open?id=0B5NVz1rA5zQMVHpXRXgxRnJvVHM', '2017-03-28 20:04:11'),
(10, 'bazen2', 'https://drive.google.com/open?id=0B5NVz1rA5zQMVHpXRXgxRnJvVHM', '2017-03-28 20:04:11');

-- --------------------------------------------------------

--
-- Tablična struktura za tablicu `tip_korisnika`
--

CREATE TABLE IF NOT EXISTS `tip_korisnika` (
  `idtip_korisnika` int(11) NOT NULL AUTO_INCREMENT,
  `naziv` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idtip_korisnika`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Izbacivanje podataka za tablicu `tip_korisnika`
--

INSERT INTO `tip_korisnika` (`idtip_korisnika`, `naziv`) VALUES
(1, 'Administrator'),
(2, 'Moderator'),
(3, 'Registrirani korisnik');

-- --------------------------------------------------------

--
-- Tablična struktura za tablicu `usluga`
--

CREATE TABLE IF NOT EXISTS `usluga` (
  `idusluga` int(11) NOT NULL AUTO_INCREMENT,
  `naziv` varchar(45) COLLATE utf8_bin DEFAULT NULL,
  `kategorija_usluga_idkategorija` int(11) NOT NULL,
  `vrijeme_trajanja` int(11) DEFAULT NULL,
  `opis` text COLLATE utf8_bin,
  `cijena` double DEFAULT NULL,
  `broj_rezervacija` int(11) DEFAULT NULL,
  PRIMARY KEY (`idusluga`),
  KEY `fk_usluga_kategorija_usluga1_idx` (`kategorija_usluga_idkategorija`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=27 ;

--
-- Izbacivanje podataka za tablicu `usluga`
--

INSERT INTO `usluga` (`idusluga`, `naziv`, `kategorija_usluga_idkategorija`, `vrijeme_trajanja`, `opis`, `cijena`, `broj_rezervacija`) VALUES
(1, 'Šišanje', 1, 1, 'Šišanje škarama ili aparatom.', 30, 3),
(2, 'Bojanje kose', 1, 2, 'Bojanje kose u trajnu ili polutrajnu boju.', 120, 4),
(3, 'Dodavanje pramenova', 1, 1, 'Bojanje samo pramenova kose.', 80, 6),
(4, 'Relaks masaža', 2, 1, 'Terapeut, uz pomo? ulja ili losiona za masažu, masira površinske spojeve miši?a dugim glatkim i kružnim pokretima.', 200, 2),
(5, 'Sportska masaža', 2, 1, 'Namijenjena je sportašima i osobama koje se aktivno bave sportom, jer joj osnovna funkcija nije opuštanje, ve? sprije?avanje povreda i pove?anje sportskih mogu?nosti. Koristi se kombinacija više tehnika, a pokreti su brži i intezivniji od relaks masaže. Najviše su orijentirani na istezanje miši?a što pove?ava njihovu elasti?nost.', 300, 8),
(6, 'Finska sauna', 3, 1, 'Klasi?na sauna, najbolja je na temperaturi od 75-90°C na vlazi od 20-35%, koju postižemo laganim posipanjem vode na vru?e kamenje pe?i, kada se osjete jedinstveni trnci u valovima. Tada toplina prodire duboko u kožu, otvorene pore i znoj osloba?aju mrtve stanice kože koje jednostavno nestaju ispod tuša.', 50, 4),
(7, 'Bazen', 4, 1, 'Sat vremena na unutarnjem bazenu. Temperatura : od 28 °C do 30 °C', 20, 5),
(8, 'Bazen dan', 4, 24, 'Dnevna ulaznica za unutarnji bazen. Temperatura : od 28 °C do 30 °C', 40, 7),
(9, 'Čišćenje lica', 5, 1, 'Konkretnim tretmanom u što je uključeno skidanje šminke, piling, mehaničko čišćenje, maska, krema, masaža daje licu svježiji i mlađi izgled. Koži lica odstranimo oštećene stanice i nahranimo zdrave.', 250, 2),
(10, 'Piling tijela', 6, 1, 'Kružnim i malo ja?im pokretima poti?emo kožu da se prokrvi i da bude spremna za nanošenje zaštitnog i hranjivog ulja ili losiona.', 300, 9),
(11, 'Oriental dream kupka', 7, 1, 'Kupka s orijentalnim esencijama s cimetom i ekstraktom šljive.', 100, 1),
(12, 'Spa manikura', 8, 1, 'Spa manikura je klasi?na manikura sa posebnim dodatcima u njezi ruku. Posebna maska koja se stavlja na kožu koja ju pomla?uje i hrani. Parafinska kupka tako?er poboljšava tonus i pruža veliko zadovoljstvo u izgledu i pomla?enoj koži.', 150, 12),
(13, 'Pedikura', 8, 1, 'Uređivanje stopala i niktiju na stopalima.', 50, 0),
(14, 'Hhhhh', 5, 4, 'dgsdgsdg', 4, 0),
(15, 'Duga sauna', 3, 4, 'Sauna koja traje 4 sata', 500, 0),
(16, 'Minival', 1, 2, 'Primjena minivala na kosu. Trajan učinak', 200, 0),
(17, 'Crna maska za lice', 5, 1, 'Crna maska izvlači sve mitesere i ostale nečistoće iz vaše kože.', 150, 0),
(18, 'Ružina kupka', 7, 1, 'Kupka sa ružinim laticama i ružinom vodicom.', 200, 0),
(19, 'Geliranje noktiju', 8, 2, 'Nanošenje gela na Vaše nokte. Dulje drži lak i izgleda profesionalno.', 200, 0),
(20, 'Peglanje', 1, 1, 'Izravnavanje kovrčave kose.', 150, 0),
(21, 'Feniranje', 1, 1, 'Feniranje kose.', 30, 0),
(23, 'Usluga1', 9, 1, 'Usluga1', 200, 0),
(24, 'Usluga2', 9, 1, 'Usluga2', 150, 0),
(25, '', 7, 0, '', 0, 0),
(26, 'uslugaa', 13, 1, 'aa', 40, 0);

-- --------------------------------------------------------

--
-- Tablična struktura za tablicu `video`
--

CREATE TABLE IF NOT EXISTS `video` (
  `idvideo` int(11) NOT NULL AUTO_INCREMENT,
  `naziv` varchar(45) DEFAULT NULL,
  `path` text,
  `vrijeme` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`idvideo`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Izbacivanje podataka za tablicu `video`
--

INSERT INTO `video` (`idvideo`, `naziv`, `path`, `vrijeme`) VALUES
(1, 'Bazen1', 'https://file.videopolis.com/D/09b06287-3a37-4ef4-af53-0254ba747026/8549.7447.prague.alchymist-grand-hotel-and-spa.amenity.health-club-spa-6967-480p.mp4', '2017-03-28 20:09:11'),
(2, 'Bazen2', 'https://www.thedoldergrand.com/app/uploads/2015/10/TDG_Wohlfuehlen.mp4', '2017-03-28 20:09:11'),
(3, 'Bazen3', 'https://file.videopolis.com/D/46b4f224-91cb-4573-813b-6b22ac65d594/8588.11612.acme.grand-traverse-resort-and-spa.amenity.spa-7639-480p.mp4', '2017-03-28 20:09:11'),
(4, 'Bazen4', 'https://file.videopolis.com/D/09b06287-3a37-4ef4-af53-0254ba747026/8549.7447.prague.alchymist-grand-hotel-and-spa.amenity.health-club-spa-6967-480p.mp4', '2017-03-28 20:09:11'),
(5, 'Bazen5', 'https://www.thedoldergrand.com/app/uploads/2015/10/TDG_Wohlfuehlen.mp4', '2017-03-28 20:09:11'),
(6, 'Masaža1', 'https://file.videopolis.com/D/46b4f224-91cb-4573-813b-6b22ac65d594/8588.11612.acme.grand-traverse-resort-and-spa.amenity.spa-7639-480p.mp4', '2017-03-28 20:09:11'),
(7, 'Masaža2', 'https://file.videopolis.com/D/09b06287-3a37-4ef4-af53-0254ba747026/8549.7447.prague.alchymist-grand-hotel-and-spa.amenity.health-club-spa-6967-480p.mp4', '2017-03-28 20:09:11'),
(8, 'Masaža3', 'https://www.thedoldergrand.com/app/uploads/2015/10/TDG_Wohlfuehlen.mp4', '2017-03-28 20:09:11'),
(9, 'Masaža4', 'https://file.videopolis.com/D/46b4f224-91cb-4573-813b-6b22ac65d594/8588.11612.acme.grand-traverse-resort-and-spa.amenity.spa-7639-480p.mp4', '2017-03-28 20:09:11'),
(10, 'Masaža5', 'https://file.videopolis.com/D/09b06287-3a37-4ef4-af53-0254ba747026/8549.7447.prague.alchymist-grand-hotel-and-spa.amenity.health-club-spa-6967-480p.mp4', '2017-03-28 20:09:11'),
(11, 'Spa6', 'http://www.ranchobernardoinn.com//videos/mp4/rbi-spa.mp4', '2017-06-07 11:01:03');

--
-- Ograničenja za izbačene tablice
--

--
-- Ograničenja za tablicu `akcija_kupona`
--
ALTER TABLE `akcija_kupona`
  ADD CONSTRAINT `fk_akcija_kupona_pdf1` FOREIGN KEY (`pdf_idpdf`) REFERENCES `pdf` (`idpdf`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_akcija_kupona_slika1` FOREIGN KEY (`slika`) REFERENCES `slika` (`idslika`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_akcija_kupona_video1` FOREIGN KEY (`video`) REFERENCES `video` (`idvideo`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ograničenja za tablicu `bodovi`
--
ALTER TABLE `bodovi`
  ADD CONSTRAINT `fk_akcije_korisnika_has_korisnik_akcije_korisnika1` FOREIGN KEY (`akcija_korisnika`) REFERENCES `akcije_korisnika` (`idakcije_korisnika`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_akcije_korisnika_has_korisnik_korisnik1` FOREIGN KEY (`korisnik`) REFERENCES `korisnik` (`idkorisnika`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ograničenja za tablicu `dnevnik_baze`
--
ALTER TABLE `dnevnik_baze`
  ADD CONSTRAINT `fk_dnevnik_baze_korisnik1` FOREIGN KEY (`korisnik_idkorisnika`) REFERENCES `korisnik` (`idkorisnika`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ograničenja za tablicu `dnevnik_prijava`
--
ALTER TABLE `dnevnik_prijava`
  ADD CONSTRAINT `fk_dnevnik_prijava_korisnik1` FOREIGN KEY (`korisnik_idkorisnika`) REFERENCES `korisnik` (`idkorisnika`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ograničenja za tablicu `dnevnik_rada`
--
ALTER TABLE `dnevnik_rada`
  ADD CONSTRAINT `fk_dnevnik_rada_korisnik1` FOREIGN KEY (`korisnik_idkorisnika`) REFERENCES `korisnik` (`idkorisnika`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ograničenja za tablicu `korisnik`
--
ALTER TABLE `korisnik`
  ADD CONSTRAINT `fk_korisnik_tip_korisnika1` FOREIGN KEY (`tip_korisnika_idtip_korisnika`) REFERENCES `tip_korisnika` (`idtip_korisnika`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ograničenja za tablicu `košarica`
--
ALTER TABLE `košarica`
  ADD CONSTRAINT `fk_košarica_kupon1` FOREIGN KEY (`kupon_idkupon`) REFERENCES `kupon` (`idkupon`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `košarica_ibfk_1` FOREIGN KEY (`korisnik_idkorisnik`) REFERENCES `korisnik` (`idkorisnika`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ograničenja za tablicu `kupon`
--
ALTER TABLE `kupon`
  ADD CONSTRAINT `fk_kupon_akcija_kupona1` FOREIGN KEY (`akcija_kupona_idakcija_kupona`) REFERENCES `akcija_kupona` (`idakcija_kupona`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_kupon_kategorija_usluga1` FOREIGN KEY (`kategorija_usluga_idkategorija`) REFERENCES `kategorija_usluga` (`idkategorija`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ograničenja za tablicu `moderatori`
--
ALTER TABLE `moderatori`
  ADD CONSTRAINT `fk_korisnik_has_kategorija_usluga_kategorija_usluga1` FOREIGN KEY (`kategorija`) REFERENCES `kategorija_usluga` (`idkategorija`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_korisnik_has_kategorija_usluga_korisnik1` FOREIGN KEY (`korisnik`) REFERENCES `korisnik` (`idkorisnika`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ograničenja za tablicu `rezervacije`
--
ALTER TABLE `rezervacije`
  ADD CONSTRAINT `fk_rezervacije_korisnik1` FOREIGN KEY (`korisnik_idkorisnik`) REFERENCES `korisnik` (`idkorisnika`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_rezervacije_usluga1` FOREIGN KEY (`usluga_idusluga`) REFERENCES `usluga` (`idusluga`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ograničenja za tablicu `usluga`
--
ALTER TABLE `usluga`
  ADD CONSTRAINT `fk_usluga_kategorija_usluga1` FOREIGN KEY (`kategorija_usluga_idkategorija`) REFERENCES `kategorija_usluga` (`idkategorija`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
